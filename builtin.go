// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "fmt"

type Operator struct {
	name   string
	prec   int
	unary  map[BaseType]*OpNamePair
	binary map[BaseType]*OpNamePair

	comparison bool
	reversible bool
}

type OpNamePair struct {
	normal   string
	unsigned string
}

var operators map[string]*Operator

func InitOperators() {
	operators = make(map[string]*Operator)

	/* binary ops (must be first) */

	RegisterOperator("or",  1, TYP_Bool, "bor",  "", false)
	RegisterOperator("and", 2, TYP_Bool, "band", "", false)

	RegisterOperator("==", 3, TYP_Int, "ieq?", "ueq?", true)
	RegisterOperator("!=", 3, TYP_Int, "ine?", "une?", true)
	RegisterOperator("<",  3, TYP_Int, "ilt?", "ult?", true)
	RegisterOperator("<=", 3, TYP_Int, "ile?", "ule?", true)
	RegisterOperator(">",  3, TYP_Int, "igt?", "ugt?", true)
	RegisterOperator(">=", 3, TYP_Int, "ige?", "uge?", true)

	RegisterOperator("==", 3, TYP_Pointer, "peq?", "", true)
	RegisterOperator("!=", 3, TYP_Pointer, "pne?", "", true)
	RegisterOperator("<",  3, TYP_Pointer, "plt?", "", true)
	RegisterOperator("<=", 3, TYP_Pointer, "ple?", "", true)
	RegisterOperator(">",  3, TYP_Pointer, "pgt?", "", true)
	RegisterOperator(">=", 3, TYP_Pointer, "pge?", "", true)

	RegisterOperator("==", 3, TYP_Float, "feq?", "", true)
	RegisterOperator("!=", 3, TYP_Float, "fne?", "", true)
	RegisterOperator("<",  3, TYP_Float, "flt?", "", true)
	RegisterOperator("<=", 3, TYP_Float, "fle?", "", true)
	RegisterOperator(">",  3, TYP_Float, "fgt?", "", true)
	RegisterOperator(">=", 3, TYP_Float, "fge?", "", true)

	RegisterOperator("==", 3, TYP_Bool, "beq?", "", true)
	RegisterOperator("!=", 3, TYP_Bool, "bne?", "", true)
	RegisterOperator("<",  3, TYP_Bool, "blt?", "", true)
	RegisterOperator("<=", 3, TYP_Bool, "ble?", "", true)
	RegisterOperator(">",  3, TYP_Bool, "bgt?", "", true)
	RegisterOperator(">=", 3, TYP_Bool, "bge?", "", true)

	RegisterOperator("+", 4, TYP_Int, "iadd",  "", false)
	RegisterOperator("-", 4, TYP_Int, "isub",  "", false)
	RegisterOperator("*", 5, TYP_Int, "imul",  "", false)
	RegisterOperator("/", 5, TYP_Int, "idivt", "udivt", false)
	RegisterOperator("%", 5, TYP_Int, "iremt", "uremt", false)

	RegisterOperator("|",  4, TYP_Int, "ior",  "", false)
	RegisterOperator("~",  4, TYP_Int, "ixor", "", false)
	RegisterOperator("&",  5, TYP_Int, "iand", "", false)
	RegisterOperator("<<", 6, TYP_Int, "ishl", "ushl", false)
	RegisterOperator(">>", 6, TYP_Int, "ishr", "ushr", false)

	RegisterOperator("+", 4, TYP_Float, "fadd",  "", false)
	RegisterOperator("-", 4, TYP_Float, "fsub",  "", false)
	RegisterOperator("*", 5, TYP_Float, "fmul",  "", false)
	RegisterOperator("/", 5, TYP_Float, "fdiv",  "", false)
	RegisterOperator("%", 5, TYP_Float, "fremt", "", false)

	RegisterOperator("+", 4, TYP_Pointer, "padd", "", false)
	RegisterOperator("-", 4, TYP_Pointer, "psub", "", false)

	/* unary ops */

	RegisterOperator("~",   0, TYP_Int,   "iflip", "", false)
	RegisterOperator("-",   0, TYP_Int,   "ineg",  "", false)
	RegisterOperator("-",   0, TYP_Float, "fneg",  "", false)
	RegisterOperator("not", 0, TYP_Bool,  "bnot",  "", false)

	RegisterOperator("abs",   0, TYP_Int,   "iabs",  "", false)
	RegisterOperator("abs",   0, TYP_Float, "fabs",  "", false)
	RegisterOperator("sqrt",  0, TYP_Float, "fsqrt", "", false)

	RegisterOperator("round", 0, TYP_Float, "fround", "", false)
	RegisterOperator("trunc", 0, TYP_Float, "ftrunc", "", false)
	RegisterOperator("floor", 0, TYP_Float, "ffloor", "", false)
	RegisterOperator("ceil",  0, TYP_Float, "fceil",  "", false)

	RegisterOperator("big-endian",    0, TYP_Int, "ibig",    "", false)
	RegisterOperator("little-endian", 0, TYP_Int, "ilittle", "", false)
	RegisterOperator("byte-swap",     0, TYP_Int, "iswap",   "", false)

	RegisterOperator("zero?", 0, TYP_Int, "izero?", "", true)
	RegisterOperator("some?", 0, TYP_Int, "isome?", "", true)
	RegisterOperator("neg?",  0, TYP_Int, "ineg?",  "", true)
	RegisterOperator("pos?",  0, TYP_Int, "ipos?",  "isome?", true)

	RegisterOperator("zero?", 0, TYP_Float, "fzero?", "", true)
	RegisterOperator("some?", 0, TYP_Float, "fsome?", "", true)
	RegisterOperator("neg?",  0, TYP_Float, "fneg?",  "", true)
	RegisterOperator("pos?",  0, TYP_Float, "fpos?",  "", true)

	RegisterOperator("inf?", 0, TYP_Float, "finf?", "", true)
	RegisterOperator("nan?", 0, TYP_Float, "fnan?", "", true)

	RegisterOperator("null?", 0, TYP_Pointer, "pnull?", "", true)
	RegisterOperator("ref?",  0, TYP_Pointer, "pref?",  "", true)

	/* mark the commutitive ops */

	operators["+"].reversible = true
	operators["*"].reversible = true
	operators["&"].reversible = true
	operators["|"].reversible = true
	operators["~"].reversible = true

	operators["=="].reversible = true
	operators["!="].reversible = true
}

func RegisterOperator(op string, prec int, kind BaseType, normal string, unsigned string, cmp bool) {
	info := operators[op]

	if info == nil {
		info = new(Operator)

		info.name   = op
		info.prec   = prec
		info.unary  = make(map[BaseType]*OpNamePair)
		info.binary = make(map[BaseType]*OpNamePair)
		info.comparison = cmp

		operators[op] = info
	} else {
		if prec != 0 && prec != info.prec {
			panic("different precedences for " + op)
		}
	}

	name_pair := new(OpNamePair)
	name_pair.normal = normal
	name_pair.unsigned = unsigned

	if prec == 0 {
		info.unary[kind] = name_pair
	} else {
		info.binary[kind] = name_pair
	}
}

func (name_pair *OpNamePair) Get(unsigned bool) string {
	if unsigned && name_pair.unsigned != "" {
		return name_pair.unsigned
	}
	return name_pair.normal
}

func IsBinaryOperator(t *Node) bool {
	if t.kind != NL_Name {
		return false
	}
	op := operators[t.str]
	if op == nil {
		return false
	}
	return len(op.binary) > 0
}

func IsOperator(t *Node) bool {
	if t.kind == NL_Name {
		return IsOperatorName(t.str)
	}
	return false
}

func IsOperatorName(op string) bool {
	_, ok := operators[op]
	return ok
}
