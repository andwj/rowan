// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

type Architecture interface {
	// this returns true for big endian CPUs, false for little endian.
	BigEndian() bool

	// RegisterBits gives the number of bits in a general purpose register.
	RegisterBits() int
}

var arch Architecture

//----------------------------------------------------------------------

type AMD64_Arch struct {
	unused int
}

func NewAMD64Arch() *AMD64_Arch {
	arch := new(AMD64_Arch)

	return arch
}

func (arch *AMD64_Arch) OutputExt() string {
	return ".s"
}

func (arch *AMD64_Arch) RegisterBits() int {
	return 64
}

func (arch *AMD64_Arch) BigEndian() bool {
	return false
}
