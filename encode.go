// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

func (L *LocalVar) Encode() string {
	return "%" + EncodeName(L.name, "")
}

func (def *Definition) Encode() string {
	switch def.kind {
	case DEF_Func:
		fdef := def.d_func
		if fdef.extern {
			return "@" + EncodeName(fdef.name, "")
		} else {
			return "@" + EncodeName(fdef.name, fdef.module)
		}

	case DEF_Method:
		obj_type := def.d_func.ty.param[0].ty.sub
		return EncodeMethodName(def.d_func.name, obj_type)

	case DEF_Var:
		vdef := def.d_var
		if vdef.extern {
			return "@" + EncodeName(vdef.name, "")
		} else {
			return "@" + EncodeName(vdef.name, vdef.module)
		}

	default:
		panic("weird def to encode")
	}
}

func (ref *ForeignRef) Encode() string {
	def := ref.def
	mod_name := ref.mod.name

	switch def.kind {
	case DEF_Func:
		fdef := def.d_func
		return "@" + EncodeName(fdef.name, mod_name)

	case DEF_Method:
		// obj_type := def.d_func.ty.param[0].ty.sub
		// return EncodeMethodName(def.d_func.name, obj_type)
		panic("DEF_Method in ForeignRef")

	case DEF_Var:
		vdef := def.d_var
		return "@" + EncodeName(vdef.name, mod_name)

	default:
		panic("weird def to encode")
	}
}

func EncodeMethodName(name string, ty *Type) string {
	def := ty.def
	if def == nil {
		panic("no TypeDef for method")
	}

	// remove the colon prefix
	name = name[1:]

	name = def.name + "." + name

	return "@" + EncodeName(name, def.module)
}

func EncodeLabel(name string) string {
	name = EncodeName(name, "")

	if name[0] == '$' {
		name = name[1:]
	}
	return name
}

func EncodeName(name string, module string) string {
	if module != "" && module != main_mod.name {
		// use a single colon here, which sageleaf will convert
		// to a `$` dollar sign (for AMD64 anyway).
		name = module + ":" + name
	}

/*
	var sb strings.Builder
	has_escape := false

	for _, ch := range []rune(name) {
		if ch == '_' || ('A' <= ch && ch <= 'Z') || ('a' <= ch && ch <= 'z') || ('0' <= ch && ch <= '9') {
			sb.WriteByte(byte(ch))
		} else if ch > 0xFFFF {
			fmt.Fprintf(&sb, "?v%06X", ch)
			has_escape = true
		} else if ch >= 0x7F {
			fmt.Fprintf(&sb, "?u%04X", ch)
			has_escape = true
		} else if ch == '/' {
			sb.WriteString("$")
			has_escape = true
		} else if ch == '-' {
			sb.WriteString("?_")
			has_escape = true
		} else {
			fmt.Fprintf(&sb, "?%02X", ch)
			has_escape = true
		}
	}
*/

	return name
}
