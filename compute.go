// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

func (fu *FuncDef) CompileTerm(t *Node, dest_type *Type) *Node {
	// result is always a NZ_Term or NZ_Local (a "term"), and `ty`
	// field has valid type.

	// for a computation, shift to a temporary
	switch t.kind {
		case
			NX_Call, NX_Method, NX_Matches, NX_Unary, NX_Binary,
			NX_Conv, NX_RawCast, NX_Min, NX_Max,
			NX_Field, NX_Index, NX_StackVar: {

			comp := fu.CompileComputation(t, dest_type)
			if comp == P_FAIL {
				return P_FAIL
			}

			return fu.ShiftComputation(comp)
		}
	}

	t = fu.Basic_Term(t, dest_type)
	if t == P_FAIL {
		return P_FAIL
	}

	if dest_type == nil {
		return t
	}
	return fu.CheckUpgradeType(t, dest_type)
}

func (fu *FuncDef) CompileTarget(t *Node) *Node {
	// result is always a NZ_Target or NZ_Local (a "target"), and `ty`
	// field has valid type.

	switch t.kind {
	case NX_Local:
		if t.local.immutable {
			PostError("cannot assign to immutable local %s", t.str)
			return P_FAIL
		}
		return fu.Basic_Local(t)

	case NX_Global:
		va := t.def.d_var
		if va.rom {
			PostError("cannot assign to immutable global %s", t.str)
			return P_FAIL
		}
		return fu.Basic_Deref(t, true)

	case NX_Deref:
		return fu.Basic_Deref(t.children[0], true)

	default:
		// parser has disallowed other things
		panic("weird node in CompileTarget")
	}
}

func (fu *FuncDef) CompileComputation(t *Node, dest_type *Type) *Node {
	// result is always a NZ_Comp, NZ_Term or NZ_Local (a "computation"),
	// and `ty` field has a valid type.

	switch t.kind {
		case NX_Call:
			t = fu.Comp_FunCall(t, true, false)

		case NX_Method:
			t = fu.Comp_MethodCall(t, true, false)

		case NX_Matches:
			t = fu.Comp_Matches(t, false)

		case NX_StackVar:
			t = fu.Comp_StackVar(t)

		case NX_Unary:
			// optimise boolean not + `matches?`
			child := t.children[0]
			if t.str == "not" && child.kind == NX_Matches {
				return fu.Comp_Matches(child, true)
			}

			t = fu.Math_Expr(t)

		case NX_Binary:
			t = fu.Math_Expr(t)

		case NX_Conv, NX_RawCast, NX_Min, NX_Max:
			t = fu.Math_Expr(t)

		case NX_Field, NX_Index:
			// these usually produce NC_OpSeq, hence are handled here
			t = fu.Math_Expr(t)

		default:
			// it is not a computation, so it must be a term
			t = fu.Basic_Term(t, dest_type)
	}

	// we may get an untyped literal from Math_Expr
	if t.IsLiteral() {
		t = fu.Basic_Literal(t, dest_type)
	}

	if t == P_FAIL {
		return P_FAIL
	}
	if t.ty == nil {
		panic("node without a type")
	}

	if dest_type != nil {
		if TypeIsSubtype(t.ty, dest_type) {
			t = fu.Math_DoConvert(t, dest_type)
			if t == P_FAIL {
				return P_FAIL
			}
		}
	}

	if t.kind == NC_OpSeq {
		t = fu.EncodeOpSeq(t)
		if t == P_FAIL {
			return P_FAIL
		}
	}

	if dest_type != nil {
		if ! TypeCompare(t.ty, dest_type) {
			PostError("type mismatch: wanted %s, got %s", dest_type.String(), t.ty.String())
			return P_FAIL
		}
	}
	return t
}

//----------------------------------------------------------------------

func (fu *FuncDef) Basic_Term(t *Node, dest_type *Type) *Node {
	if t.IsLiteral() {
		return fu.Basic_Literal(t, dest_type)
	}

	switch t.kind {
	case NX_Local:
		t = fu.Basic_Local(t)

	case NX_Global:
		t = fu.Basic_GlobalVar(t)

	case NX_Func:
		t = fu.Basic_GlobalFunc(t)

	case NX_ChkNull:
		t = fu.Basic_CheckNull(t)

	case NX_Deref:
		t = fu.Basic_Deref(t.children[0], false)

	case NX_AddrOf:
		t = fu.Basic_AddrOf(t)

	case NL_Name: panic("NL_Name in Basic_Term")
	case NG_Expr: panic("NG_Expr in Basic_Term")
	case NG_Data: panic("NG_Data in Basic_Term")

	default:
		// probably should never get here
		PostError("bad syntax")
		return P_FAIL
	}

	if t == P_FAIL {
		return P_FAIL
	}
	if t.ty == nil {
		panic("node without a type")
	}

	return t
}

func (fu *FuncDef) Basic_Literal(t *Node, dest_type *Type) *Node {
	if dest_type != nil {
		if CheckLiteral(t, dest_type) != OK {
			return P_FAIL
		}
	}

	if t.ty == nil {
		PostError("missing type (literals are untyped)")
		return P_FAIL
	}

	t2 := NewNode(NZ_Term, EncodeValue(t, false), t.pos)
	t2.ty = t.ty
	return t2
}

func (fu *FuncDef) Basic_Local(t *Node) *Node {
	// has an earlier error preventing it getting a type?
	if t.local.ty == nil {
		return P_FAIL
	}

	t2 := NewNode(NZ_Local, t.local.Encode(), t.pos)
	t2.ty = t.local.ty
	return t2
}

func (fu *FuncDef) Basic_GlobalVar(t *Node) *Node {
	// produces a mem-access NZ_Term to read/write the global,
	// or just the address for an array, struct or union.

	va := t.def.d_var

	t_glob := fu.Basic_GlobalVarAddr(t)

	if va.ty.IsStructural() {
		return t_glob
	}

	type_str := va.ty.Encode()
	full_str := "[" + type_str + " " + t_glob.str + "]"

	t2 := NewNode(NZ_Term, full_str, t.pos)
	t2.ty = va.ty
	return t2
}

func (fu *FuncDef) Basic_GlobalVarAddr(t *Node) *Node {
	t2 := NewNode(NZ_Term, t.def.Encode(), t.pos)
	t2.ty = NewPointerType(t.def.d_var.ty)
	return t2
}

func (fu *FuncDef) Basic_GlobalFunc(t *Node) *Node {
	t2 := NewNode(NZ_Term, t.def.Encode(), t.pos)
	t2.ty = NewPointerType(t.def.d_func.ty)
	return t2
}

func (fu *FuncDef) Basic_CheckNull(t *Node) *Node {
	child := t.children[0]
	check := true

	if child.kind == NL_Null {
		PostError("cannot dereference NULL")
		return P_FAIL
	}
	if child.IsLiteral() {
		PostError("cannot dereference a literal")
		return P_FAIL
	}

	if child.kind == NX_Local && child.local.self {
		// no check needed, self is checked at method call time
		check = false
	}
	if child.kind == NX_Func || child.kind == NX_StackVar {
		check = false
	}
	if child.kind == NX_Global {
		va := child.def.d_var
		if va.ty.IsStructural() {
			// no check needed, we just get the address
			check = false
		}
	}

	term := fu.CompileTerm(t.children[0], nil)
	if term == P_FAIL {
		return P_FAIL
	}
	if term.ty.kind != TYP_Pointer {
		PostError("cannot dereference a %s", term.ty.SimpleName())
		return P_FAIL
	}

	if check {
		OutCode("check-null %s", term.str)
	}

	return term
}

func (fu *FuncDef) Basic_Deref(t *Node, is_target bool) *Node {
	// t is the child of NX_Deref node

	if t.IsLiteral() {
		PostError("cannot access a literal value")
		return P_FAIL
	}

	switch t.kind {
	case NX_Local:
		t = fu.Basic_Local(t)

	case NX_Global:
		t = fu.Basic_GlobalVarAddr(t)
		if t == P_FAIL {
			return P_FAIL
		}

	default:
		t = fu.CompileTerm(t, nil)
		if t == P_FAIL {
			return P_FAIL
		}
		if t.kind != NZ_Local {
			t = fu.ShiftToTemporary(t, "__ptr")
		}
	}

	if t.ty.kind != TYP_Pointer {
		PostError("cannot dereference a %s", t.ty.SimpleName())
		return P_FAIL
	}

	target_type := t.ty.sub

	if target_type.IsStructural() {
		PostError("cannot read/write a whole %s", target_type.SimpleName())
		return P_FAIL
	}
	if target_type == rawmem_type {
		PostError("cannot access a raw-mem pointer")
		return P_FAIL
	}
	if target_type.kind == TYP_Extern {
		PostError("cannot access an external type in []")
		return P_FAIL
	}

	type_str := target_type.Encode()
	full_str := "[" + type_str + " " + t.str + "]"

	if is_target {
		term := NewNode(NZ_Target, full_str, t.pos)
		term.ty = target_type
		return term
	}

	term := NewNode(NZ_Term, full_str, t.pos)
	term.ty = target_type
	return term
}

func (fu *FuncDef) Basic_AddrOf(t *Node) *Node {
	// this node is primarily descriptive, the NX_Field and NX_Index
	// children do the actual work of computing the address.

	child := t.children[0]

	if child.kind == NX_Global {
		return fu.Basic_GlobalVarAddr(child)
	}

	return fu.CompileTerm(child, nil)
}

//----------------------------------------------------------------------

func (fu *FuncDef) Comp_FunCall(t *Node, need_type bool, is_tail bool) *Node {
	children := t.children

	t_func := children[0]
	params := children[1:]

	if t_func.IsLiteral() {
		PostError("cannot call a literal/constant")
		return P_FAIL
	}

/* FIXME
	if called_func != nil && called_func.inline {
		PostError("cannot tail call an inline function")
		return FAILED
	}
*/

	t_func = fu.CompileTerm(t_func, nil)
	if t_func == P_FAIL {
		return P_FAIL
	}

	return fu.Comp_FinishCall(t_func, params, nil, need_type, is_tail)
}

func (fu *FuncDef) Comp_MethodCall(t *Node, need_type bool, is_tail bool) *Node {
	// grab the receiver
	if t.children[1].IsLiteral() {
		PostError("method receiver cannot be a literal/constant")
		return P_FAIL
	}

	recv := fu.CompileTerm(t.children[1], nil)
	if recv == P_FAIL {
		return P_FAIL
	}

	self_type := recv.ty
	if self_type == nil {
		panic("receiver without a type")
	}
	if self_type.kind != TYP_Pointer {
		PostError("method receiver must be a pointer, got a %s", self_type.SimpleName())
		return P_FAIL
	}

	obj_type := self_type.sub
	if obj_type.def == nil {
		PostError("method receiver must be a custom type")
		return P_FAIL
	}

	method_name := t.children[0].str
	method := obj_type.FindMethod(method_name)
	if method == nil {
		PostError("unknown method '%s' in type %s", method_name, obj_type.def.name)
		return P_FAIL
	}

	fun_str := EncodeMethodName(method_name, obj_type)

	t_func := NewNode(NZ_Term, fun_str, t.pos)
	t_func.ty = method.ty

	// this includes the receiver, but it is only there tp simplify
	// Comp_Parameters(), the receiver will not be processed again.
	params := t.children[1:]

	return fu.Comp_FinishCall(t_func, params, recv, need_type, is_tail)
}

func (fu *FuncDef) Comp_FinishCall(t_func *Node, params []*Node, recv *Node, need_type bool, is_tail bool) *Node {
	fun_type := t_func.ty
	if fun_type == nil {
		panic("no type in function node")
	}
	if fun_type.kind == TYP_Pointer {
		fun_type = fun_type.sub
	}
	if fun_type.kind != TYP_Function {
		PostError("call requires a function argument")
		return P_FAIL
	}

	if is_tail {
		// if this function (the caller) returns something, then the called
		// function must return the SAME type.  That includes `no-return`.
		if fu.ty.sub.kind != TYP_Void {
			if ! TypeCompare(fun_type.sub, fu.ty.sub) {
				PostError("type mismatch on tail-call result: wanted %s, got %s",
					fu.ty.sub.String(), fun_type.sub.String())
				return P_FAIL
			}
		}
	}

	type_str := ""
	if need_type {
		type_str = fun_type.sub.Encode() + " "
	}

	flag_str := ""
	if is_tail {
		flag_str = "tail "
	} else if fun_type.sub.kind == TYP_NoReturn {
		flag_str = "no-return "
	}

	t_params := fu.Comp_Parameters(params, fun_type, recv)
	if t_params == P_FAIL {
		return P_FAIL
	}

	call_str := "call " + type_str + flag_str + t_func.str + t_params.str

	t2 := NewNode(NZ_Comp, call_str, t_func.pos)
	t2.ty = fun_type.sub
	return t2
}

func (fu *FuncDef) Comp_Parameters(params []*Node, fun_type *Type, recv *Node) *Node {
	if len(params) != len(fun_type.param) {
		PostError("wrong number of parameters: wanted %d, got %d",
			len(fun_type.param), len(params))
		return P_FAIL
	}

	line := ""

	for i, par_info := range fun_type.param {
		line = line + " "

		// the receiver of methods is already compiled
		if i == 0 && recv != nil {
			line = line + recv.str
			continue
		}

		term := fu.CompileTerm(params[i], par_info.ty)
		if term == P_FAIL {
			return P_FAIL
		}

		line = line + term.str
	}

	t_params := NewNode(NZ_Comp, line, Position{})
	return t_params
}

//----------------------------------------------------------------------

func (fu *FuncDef) Comp_Matches(t *Node, negate bool) *Node {
	// collect terms, determine a type
	var final_type *Type

	terms := make([]*Node, 0)

	for _, child := range t.children {
		term := child

		// we must handle literals later (once we have a type)
		if term.IsLiteral() && term.ty == nil {
			terms = append(terms, term)
			continue
		}

		term = fu.CompileTerm(term, nil)
		if term == P_FAIL {
			return P_FAIL
		}

		terms = append(terms, term)

		if final_type == nil {
			final_type = term.ty
		} else if TypeIsSubtype(final_type, term.ty) {
			final_type = term.ty
		}
	}

	// check the types
	if final_type == nil {
		PostError("unknown type for matches? (untyped literals)")
		return P_FAIL
	}

	line := "matches?"

	if negate {
		line = line + " not"
	}

	for _, child := range terms {
		term := child

		if term.IsLiteral() && term.ty == nil {
			term = fu.CompileTerm(term, final_type)
		} else {
			term = fu.CheckUpgradeType(term, final_type)
		}
		if term == P_FAIL {
			return P_FAIL
		}

		line = line + " " + term.str
	}

	t2 := NewNode(NZ_Comp, line, t.pos)
	t2.ty = bool_type
	return t2
}

func (fu *FuncDef) Comp_StackVar(t *Node) *Node {
	size_str := strconv.Itoa(int(t.ty.size))
	comp_str := "stack-var " + size_str

	t2 := NewNode(NZ_Comp, comp_str, t.pos)
	t2.ty = NewPointerType(t.ty)
	return t2
}

//----------------------------------------------------------------------

func (fu *FuncDef) Math_Expr(t *Node) *Node {
	// this function is allowed to return NC_OpSeq (as well as NZ_XXX),
	// which is needed to allow building up chains of operations.
	// parent (CompileComputation) takes care to encode the NC_OpSeq.
	// it may also return literals, even untyped ones.

	if t.IsLiteral() {
		return t
	}

	switch t.kind {
		case NX_Unary:
			return fu.Math_Unary(t)

		case NX_Binary:
			return fu.Math_Binary(t)

		case NX_Conv:
			return fu.Math_Conv(t)

		case NX_RawCast:
			return fu.Math_RawCast(t)

		case NX_Min, NX_Max:
			return fu.Math_MinMax(t)

		case NX_Field:
			return fu.Math_Field(t)

		case NX_Index:
			return fu.Math_Index(t)
	}

	return fu.CompileTerm(t, nil)
}

func (fu *FuncDef) Math_Conv(t *Node) *Node {
	// source and target types must either be integer, float or bool.
	// conversions between int and float require signed integers.
	// literals allow a few other types (e.g. strings and NULL).

	t_value := fu.Math_Expr(t.children[0])
	if t_value == P_FAIL {
		return P_FAIL
	}
	if t_value.IsLiteral() {
		panic("literal missed by EvalConstExprs")
	}

	src_type  := t_value.ty
	dest_type := t.ty

	// check target type makes sense
	switch dest_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool:
		// ok
	default:
		PostError("conversion target cannot be a %s", dest_type.SimpleName())
		return P_FAIL
	}

	if src_type == nil {
		panic("term in NS_Conv has no type")
	}

	// check source type makes sense
	switch src_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool:
		// ok
	default:
		PostError("cannot convert from a %s", src_type.SimpleName())
		return P_FAIL
	}

	// become a no-op when source and destination are the same.
	if src_type.kind == dest_type.kind && src_type.bits == dest_type.bits &&
		src_type.unsigned == dest_type.unsigned && src_type.archy == dest_type.archy {
		return t_value
	}

	// check combination of source + dest types
	if (dest_type.kind == TYP_Float && src_type.kind == TYP_Int && src_type.unsigned) ||
		(src_type.kind == TYP_Float && dest_type.kind == TYP_Int && dest_type.unsigned) {
		PostError("cannot convert between float and unsigned int")
		return P_FAIL
	}

	return fu.Math_DoConvert(t_value, dest_type)
}

func (fu *FuncDef) Math_DoConvert(L *Node, dest_type *Type) *Node {
	src_type := L.ty

	if L.IsLiteral() {
		L = fu.Basic_Literal(L, nil)
		if L == P_FAIL { return P_FAIL }
	}

	// no conversion necessary if same types
	if TypeCompare(src_type, dest_type) {
		return L
	}

	L = fu.MakeOpSeq(L)
	L.ty = dest_type

	op_name := "???"

	switch src_type.kind {
	case TYP_Bool:
		if dest_type.kind == TYP_Float {
			fu.OpSeqApply(L, "btoi", s32_type, nil)
			op_name = "itof"
		} else {
			op_name = "btoi"
		}

	case TYP_Int:
		if dest_type.kind == TYP_Bool {
			if src_type.unsigned {
				op_name = "isome?"
			} else {
				op_name = "ipos?"
			}
		} else if dest_type.kind == TYP_Float {
			if src_type.unsigned {
				op_name = "utof"
			} else {
				op_name = "itof"
			}
		} else {
			// note that the SOURCE determines the operation.
			// e.g. `s8 -1` should become `u16 0xFFFF`.
			if src_type.unsigned {
				op_name = "uext"
			} else {
				op_name = "iext"
			}
		}

	case TYP_Float:
		if dest_type.kind == TYP_Bool {
			op_name = "fpos?"
		} else if dest_type.kind == TYP_Float {
			op_name = "fext"
		} else if dest_type.unsigned {
			op_name = "ftou"
		} else {
			op_name = "ftoi"
		}

	default:
		panic("weird conversion")
	}

	fu.OpSeqApply(L, op_name, dest_type, nil)
	return L
}

func (fu *FuncDef) Math_RawCast(t *Node) *Node {
	// source and target type must be one of: int, float, bool, ptr.
	// source and target must be different types.
	// source and target must be same size, no exceptions.

	t_value := fu.Math_Expr(t.children[0])

	if t_value == P_FAIL {
		return P_FAIL
	}
	if t_value.IsLiteral() {
		PostError("cast cannot be used with a literal value")
		return P_FAIL
	}

	src_type  := t_value.ty
	dest_type := t.ty

	if src_type == nil {
		panic("cast value has no type")
	}

	// check source type makes sense
	switch src_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok
	default:
		PostError("source type of cast cannot be a %s", src_type.SimpleName())
		return P_FAIL
	}

	// check combination of source + dest types.
	if dest_type.archy != src_type.archy {
		PostError("cannot cast a fixed size type and arch-dependent one")
		return P_FAIL
	}
	if dest_type.bits != src_type.bits {
		PostError("cast requires types with same size")
		return P_FAIL
	}
	if dest_type.kind != TYP_Pointer && dest_type.kind == src_type.kind {
		PostError("cannot cast between two %s types", dest_type.kind.String())
		return P_FAIL
	}

	// casting is done like other math operations (in a NC_OpSeq)
	op_seq := fu.MakeOpSeq(t_value)

	fu.OpSeqApply(op_seq, "raw-cast", dest_type, nil)
	return op_seq
}

func (fu *FuncDef) Math_MinMax(t *Node) *Node {
	// collect terms, determine a type
	var final_type *Type

	terms := make([]*Node, 0)

	for _, child := range t.children {
		term := child

		// we must handle literals later (once we have a type)
		if term.IsLiteral() && term.ty == nil {
			terms = append(terms, term)
			continue
		}

		term = fu.CompileTerm(term, nil)
		if term == P_FAIL {
			return P_FAIL
		}

		terms = append(terms, term)

		if final_type == nil {
			final_type = term.ty
		} else if TypeIsSubtype(final_type, term.ty) {
			final_type = term.ty
		}
	}

	// check the type
	if final_type == nil {
		PostError("unknown type for min/max (untyped literals)")
		return P_FAIL
	}

	// nothing to do with only a single term
	if len(terms) == 1 {
		return terms[0]
	}

	// determine name of operation
	op_name := "min"
	if t.kind == NX_Max {
		op_name = "max"
	}

	switch final_type.kind {
	case TYP_Int:
		if final_type.unsigned {
			op_name = "u" + op_name
		} else {
			op_name = "i" + op_name
		}
	case TYP_Float:   op_name = "f" + op_name
	case TYP_Bool:    op_name = "b" + op_name
	case TYP_Pointer: op_name = "p" + op_name
	default:
		panic("weird type in Min/Max")
	}

	// WISH: optimise away unneeded terms, e.g. `umin 1 x 3`

	// build operation sequence
	t_minmax := NewNode(NC_OpSeq, "", t.pos)
	t_minmax.ty = final_type

	for idx, child := range terms {
		term := child

		if term.IsLiteral() && term.ty == nil {
			term = fu.CompileTerm(term, final_type)
		} else {
			term = fu.CheckUpgradeType(term, final_type)
		}
		if term == P_FAIL {
			return P_FAIL
		}

		// optimise boolean
		if t.kind == NX_Min && LiteralIsFalse(term) {
			return EV_MakeBool(false, t.pos)
		}
		if t.kind == NX_Max && LiteralIsTrue(term) {
			return EV_MakeBool(true, t.pos)
		}

		if idx == 0 {
			t_minmax.Add(term)
		} else {
			fu.OpSeqApply(t_minmax, op_name, final_type, term)
		}
	}

	return t_minmax
}

//----------------------------------------------------------------------

func (fu *FuncDef) Math_Unary(t *Node) *Node {
	op    := operators[t.str]
	child := t.children[0]

	L := fu.Math_Expr(child)
	if L == P_FAIL {
		return P_FAIL
	}

	L_type := L.ty
	if L_type == nil {
		PostError("unknown type for %s operator (untyped literal)", op.name)
		return P_FAIL
	}

	name_pair, exist := op.unary[L_type.kind]

	if ! exist {
		PostError("type not supported by %s unary op: %s", op.name, L_type.String())
		return P_FAIL
	}

	final_type := L_type
	if op.comparison {
		final_type = bool_type
	}

	// unsigned values are never negative, so `abs` does nothing,
	// and `neg?` is always FALSE.
	if L_type.unsigned {
		if op.name == "abs" {
			return L
		}
		if op.name == "neg?" {
			// since Math_Expr calls CompileTerm for a function call,
			// which shifts it to a temporary, the call is not lost.
			return EV_MakeBool(false, t.pos)
		}
	}

	if L.IsLiteral() {
		L = fu.Basic_Literal(L, nil)
		if L == P_FAIL { return P_FAIL }
	}

	L = fu.MakeOpSeq(L)

	full_name := name_pair.Get(L_type.unsigned)

	fu.OpSeqApply(L, full_name, final_type, nil)
	return L
}

func (fu *FuncDef) Math_Binary(t *Node) *Node {
	// these two operators are short-circuiting, so they require
	// special logic...
	if t.str == "and" || t.str == "or" {
		return fu.Math_ShortCircuit(t)
	}

	L := fu.Math_Expr(t.children[0])
	R := fu.Math_Expr(t.children[1])

	if L == P_FAIL { return P_FAIL }
	if R == P_FAIL { return P_FAIL }

	op := operators[t.str]

	/* determine types, check L and R are compatible */

	L_type := L.ty
	R_type := R.ty

	if L_type == nil && (R_type == nil || op.name == "<<" || op.name == ">>") {
		PostError("unknown type for %s operator (untyped literals)", op.name)
		return P_FAIL
	}

	if L_type == nil {
		L_type = R_type

		if CheckLiteral(L, L_type) != OK {
			return P_FAIL
		}
	}

	if R_type == nil {
		R_type = L_type

		if op.name == "<<" || op.name == ">>" {
			R_type = u8_type
		} else if L_type.kind == TYP_Pointer && (op.name == "+" || op.name == "-") {
			R_type = ssize_type
		}

		if CheckLiteral(R, R_type) != OK {
			return P_FAIL
		}
	}

	final_type := L_type
	if op.comparison {
		final_type = bool_type
	}

	name_pair, exist := op.binary[L_type.kind]

	if ! exist {
		PostError("type not supported by %s operator: %s", op.name, L_type.String())
		return P_FAIL
	}

	full_name := name_pair.Get(L_type.unsigned)

	if op.name == "-" && L_type.kind == TYP_Pointer && R_type.kind == TYP_Pointer {
		// ok -- pointer difference is lax with pointer types
		final_type = ssize_type
		full_name  = "pdiff"

	} else if (op.name == "+" || op.name == "-") && L_type.kind == TYP_Pointer {
		// these just need an integer, size does not matter
		if R_type.kind != TYP_Int {
			PostError("type not supported by pointer addition: %s", R_type.String())
			return P_FAIL
		}

		// need to convert to an arch-dependent type, if not already.
		// using one of same signed-ness may be more efficient.
		if ! R_type.archy {
			if R_type.unsigned {
				R_type = usize_type
			} else {
				R_type = ssize_type
			}
			R = fu.Math_DoConvert(R, R_type)
		}

	} else if op.name == "<<" || op.name == ">>" {
		if R_type.kind != TYP_Int {
			PostError("type not supported for shift count: %s", R_type.String())
			return P_FAIL
		}

		// back-end require an 8-bit shift count
		if R_type.bits != 8 {
			R_type = u8_type
			R = fu.Math_DoConvert(R, R_type)
		}

		// check that a constant shift count is valid
		if R.kind == NL_Integer {
			val, err := strconv.ParseInt(R.str, 0, 64)

			if err != nil || val < 0 {
				PostError("invalid shift count: %s", R.str)
				return P_FAIL
			}
			if val >= int64(L_type.bits) {
				PostError("shift count too high (>= %d)", L_type.bits)
				return P_FAIL
			}
		}

	} else {
		// check type compatibility, enable auto conversions

		if TypeIsSubtype(R_type, L_type) {
			R = fu.Math_DoConvert(R, L_type)

		} else if TypeIsSubtype(L_type, R_type) {
			L = fu.Math_DoConvert(L, R_type)
			if final_type == L_type {
				final_type = R_type
			}

		} else if ! TypeCompare(L_type, R_type) {
			PostError("type mismatch between %s args: %s and %s",
				op.name, L_type.String(), R_type.String())
			return P_FAIL
		}
	}

	// check for division by zero
	if op.name == "/" || op.name == "%" {
		if LiteralIsZero(R) {
			PostError("division by zero in expression")
			return P_FAIL
		}
	}

	// check for operations which have no effect.
	// since Math_Expr calls CompileTerm for a function call,
	// which shifts it to a temporary, calls here are not lost.

	switch full_name {
	case "iadd":
		if LiteralIsZero(L) { return R }
		if LiteralIsZero(R) { return L }

	case "isub", "padd", "psub":
		if LiteralIsZero(R) { return L }

	case "imul":
		if LiteralIsOne(L) { return R }
		if LiteralIsOne(R) { return L }

		if LiteralIsZero(L) { return L }
		if LiteralIsZero(R) { return R }

	case "idivt", "udivt":
		if LiteralIsOne(R)  { return L }
		if LiteralIsZero(L) { return L }

	case "iand":
		if LiteralIsZero(L) { return L }
		if LiteralIsZero(R) { return R }

	case "ior", "ixor":
		if LiteralIsZero(L) { return R }
		if LiteralIsZero(R) { return L }
	}

	// we prefer to build in a NC_OpSeq, for LHS this is natural,
	// but for RHS the current operator must allow swapping.

	if L.kind == NC_OpSeq {
		// ok
	} else if R.kind == NC_OpSeq && op.reversible && L_type.kind != TYP_Pointer {
		// swap them
		tmp := L ; L = R ; R = tmp
	} else {
		if L.IsLiteral() {
			L = fu.Basic_Literal(L, nil)
			if L == P_FAIL { return P_FAIL }
		}

		L = fu.MakeOpSeq(L)
	}

	// ensure RHS is a term
	if R.IsLiteral() {
		R = fu.Basic_Literal(R, nil)
	} else if R.kind == NC_OpSeq {
		R = fu.EncodeOpSeq(R)
	}
	if R == P_FAIL {
		return P_FAIL
	}
	R = fu.ShiftComputation(R)

	fu.OpSeqApply(L, full_name, final_type, R)
	return L
}

func (fu *FuncDef) Math_ShortCircuit(t *Node) *Node {
	L := t.children[0]
	R := t.children[1]

	// optimise for literal values
	if t.str == "and" {
		if LiteralIsTrue (L) { return fu.Math_Shorted(R, t.str, 0) }
		if LiteralIsTrue (R) { return fu.Math_Shorted(L, t.str, 0) }

		if LiteralIsFalse(L) { return fu.Math_Shorted(R, t.str, -1) }
		if LiteralIsFalse(R) { return fu.Math_Shorted(L, t.str, -1) }
	}
	if t.str == "or" {
		if LiteralIsFalse(L) { return fu.Math_Shorted(R, t.str, 0) }
		if LiteralIsFalse(R) { return fu.Math_Shorted(L, t.str, 0) }

		if LiteralIsTrue (L) { return fu.Math_Shorted(R, t.str, +1) }
		if LiteralIsTrue (R) { return fu.Math_Shorted(L, t.str, +1) }
	}

	L = fu.Math_Expr(L)
	if L == P_FAIL {
		return P_FAIL
	}

	if L.IsLiteral() {
		PostError("type mismatch with %s operator", t.str)
		return P_FAIL
	}
	if L.ty.kind != TYP_Bool {
		PostError("type not supported by %s operator: %s", t.str, L.ty.String())
		return P_FAIL
	}

	// we need to generate code like this:
	//    __temp = LHS
	//    jump __over if [not] __temp
	//    __temp = RHS
	//    label __over

	if L.kind == NC_OpSeq {
		L = fu.EncodeOpSeq(L)
	}
	L = fu.ShiftToTemporary(L, "__" + t.str)

	label_name := fu.GenLabel("__" + t.str)

	cond_str := ""

	if t.str == "and" {
		cond_str = cond_str + "not "
	}
	cond_str = cond_str + L.str

	OutCode("jump %s if %s", EncodeLabel(label_name), cond_str)

	// handle the RHS expression
	R = fu.CompileComputation(R, bool_type)
	if R == P_FAIL {
		return P_FAIL
	}

	OutCode("%s = %s", L.str, R.str)

	fu.EmitLabel(label_name, t.pos)

	// return the temporary var
	return L
}

func (fu *FuncDef) Math_Shorted(t *Node, what string, value int) *Node {
	// this ensures any function call is not lost
	L := fu.Math_Expr(t)
	if L == P_FAIL {
		return P_FAIL
	}
	if L.ty != bool_type {
		PostError("type mismatch with %s operator", what)
		return P_FAIL
	}
	if value == 0 {
		return L
	} else {
		return EV_MakeBool(value > 0, t.pos)
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) Math_Field(t *Node) *Node {
	field_name := t.str

	base := fu.Math_Expr(t.children[0])

	if base == P_FAIL {
		return P_FAIL
	}
	if base.IsLiteral() {
		PostError("cannot access a literal value")
		return P_FAIL
	}
	if base.ty.kind != TYP_Pointer {
		PostError("cannot access field in a %s", base.ty.SimpleName())
		return P_FAIL
	}

	target_type := base.ty.sub

	if ! (target_type.kind == TYP_Struct || target_type.kind == TYP_Union) {
		if target_type.kind == TYP_Pointer {
			PostError("cannot deref a new pointer in []")
		} else {
			PostError("cannot access field in a %s", target_type.SimpleName())
		}
		return P_FAIL
	}

	field_idx := target_type.FindParam(field_name)

	if field_idx < 0 {
		PostError("no such field %s in type %s", field_name, target_type.String())
		return P_FAIL
	}

	field_offset := target_type.param[field_idx].offset
	field_type   := target_type.param[field_idx].ty
	ptr_type     := NewPointerType(field_type)

	if field_offset != 0 {
		// need to compute pointer addition, done via a NC_OpSeq
		base = fu.MakeOpSeq(base)

		ofs_str  := strconv.FormatInt(field_offset, 10)
		t_offset := NewNode(NZ_Term, "iptr " + ofs_str, base.pos)
		t_offset.ty = ssize_type

		fu.OpSeqApply(base, "padd", ptr_type, t_offset)
	}

	base.ty = ptr_type
	return base
}

func (fu *FuncDef) Math_Index(t *Node) *Node {
	base := fu.Math_Expr(t.children[0])

	if base == P_FAIL {
		return P_FAIL
	}
	if base.IsLiteral() {
		PostError("cannot access a literal value")
		return P_FAIL
	}
	if base.ty.kind != TYP_Pointer {
		PostError("cannot access array in a %s", base.ty.SimpleName())
		return P_FAIL
	}

	array_type := base.ty.sub

	if array_type.kind != TYP_Array {
		if array_type.kind == TYP_Pointer {
			PostError("cannot deref a new pointer in []")
		} else {
			PostError("cannot index a %s", array_type.SimpleName())
		}
		return P_FAIL
	}

	elem_type := array_type.sub
	ptr_type  := NewPointerType(elem_type)

	t_index := fu.Math_Expr(t.children[1])
	if t_index == P_FAIL {
		return P_FAIL
	}

	// handle a literal value...
	if t_index.IsLiteral() {
		return fu.Math_IndexByLiteral(base, array_type, ptr_type, t_index)
	}

	if t_index.ty.kind != TYP_Int {
		PostError("array index must be integer, got a %s", t_index.ty.SimpleName())
		return P_FAIL
	}

	// ensure index is in a local var (for bounds check)
	if t_index.kind == NC_OpSeq {
		t_index = fu.EncodeOpSeq(t_index)
	}
	if t_index.kind != NZ_Local {
		t_index = fu.ShiftToTemporary(t_index, "__index")
	}

	// do bounds check (unless array is open)
	if array_type.elems > 0 {
		// we don't need to check cases where index has an unsigned
		// type and highest value of type is < number of elements.
		check := true

		if t_index.ty.unsigned && t_index.ty.bits < 64 {
			highest := (uint64(1) << t_index.ty.bits) - 1
			if highest < uint64(array_type.elems) {
				check = false
			}
		}

		if check {
			num_str := strconv.Itoa(array_type.elems)
			OutCode("check-range %s %s", num_str, t_index.str)
		}
	}

	// extend index to `ssize` and multiply by element size
	t_index = fu.MakeOpSeq(t_index)

	if ! t_index.ty.archy {
		t_index = fu.Math_DoConvert(t_index, ssize_type)
	}

	if elem_type.size != 1 {
		mul_str := strconv.FormatInt(int64(elem_type.size), 10)
		t_mul   := NewNode(NZ_Term, "iptr " + mul_str, t.pos)
		t_mul.ty = ssize_type

		fu.OpSeqApply(t_index, "imul", ssize_type, t_mul)
	}

	// ensure index is a term
	if t_index.kind == NC_OpSeq {
		t_index = fu.EncodeOpSeq(t_index)
	}
	t_index = fu.ShiftComputation(t_index)

	// compute the new pointer, done via a NC_OpSeq
	base = fu.MakeOpSeq(base)

	fu.OpSeqApply(base, "padd", ptr_type, t_index)
	return base
}

func (fu *FuncDef) Math_IndexByLiteral(base *Node, array_type *Type, ptr_type *Type, lit *Node) *Node {
	// base is never a literal, but may be an NC_OpSeq or NZ_XXX

	if ! (lit.kind == NL_Integer || lit.kind == NL_Char) {
		PostError("array index must be integer, got: %s", lit.String())
		return P_FAIL
	}

	// parse the index value
	index, err := strconv.ParseInt(lit.str, 0, 64)

	if err != nil || index < 0 || index > 0x7FFFFFFF {
		PostError("illegal index value: %s", lit.str)
		return P_FAIL
	}

	if index == 0 {
		base.ty = ptr_type
		return base
	}

	// check index is within bounds
	if array_type.elems > 0 {
		if index >= int64(array_type.elems) {
			PostError("index is out of bounds (%d > %d)", index, array_type.elems - 1);
			return P_FAIL
		}
	}

	// compute the new pointer, done via a NC_OpSeq
	base = fu.MakeOpSeq(base)
	base.ty = ptr_type

	offset   := index * int64(ptr_type.sub.size)
	ofs_str  := strconv.FormatInt(offset, 10)

	t_offset := NewNode(NZ_Term, "iptr " + ofs_str, base.pos)
	t_offset.ty = ssize_type

	fu.OpSeqApply(base, "padd", base.ty, t_offset)
	return base
}
