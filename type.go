// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

type BaseType int
const (
	TYP_Void BaseType = iota
	TYP_NoReturn
	TYP_Extern

	TYP_Int
	TYP_Float
	TYP_Pointer
	TYP_Bool

	TYP_Array
	TYP_Struct
	TYP_Union
	TYP_Function
)

type Type struct {
	kind BaseType

	// for TYP_Int and TYP_Float, this is number of bits (8,16,32,64).
	// for TYP_Pointer this depends on architecture.
	bits int

	// for TYP_Array, number of elements given in `[]`.  can be zero.
	elems int

	// the total size of this type (in bytes).
	// -1 means it has not (or could not) be determined yet.
	// for unions, it is the size of the largest field.
	// for pointers, this is size of a register.
	// for arrays, this is elems * sub.size
	size int64

	// for TYP_Int, distinguishes u8..u64 from s8..s64.
	// for TYP_Pointer it is always set.
	unsigned bool

	// for types whose size is architecture-dependent, like `usize`.
	// always set for TYP_Pointer.
	archy bool

	// for `struct` and `union`, this means that fields should have
	// padding in-between (and possibly at very end) to ensure proper
	// alignment.
	padded bool

	// this is pointed-to type for pointers and references, the
	// element type for arrays, and return type for functions.
	sub *Type

	// parameters of a function, fields of a struct or union.
	param []ParamInfo

	// for a user-defined type, this is the TypeDef, otherwise nil.
	def *TypeDef

	// all methods defined for this user-defined type.
	methods []*FuncDef

	// private field for TypeCompare()
	_sequence int

	// private field for NewPointerType()
	_ptr *Type
}

type ParamInfo struct {
	name   string
	ty     *Type
	offset int64
}

//----------------------------------------------------------------------

var u8_type  *Type
var u16_type *Type
var u32_type *Type
var u64_type *Type
var usize_type *Type

var s8_type  *Type
var s16_type *Type
var s32_type *Type
var s64_type *Type
var ssize_type *Type

var f32_type *Type
var f64_type *Type

var bool_type   *Type
var void_type   *Type
var noret_type  *Type
var rawmem_type *Type

var all_types []*Type

func InitTypes() {
	all_types = make([]*Type, 0)

	reg_bits := arch.RegisterBits()

	u8_type  = NewType(TYP_Int,  8, true)
	u16_type = NewType(TYP_Int, 16, true)
	u32_type = NewType(TYP_Int, 32, true)
	u64_type = NewType(TYP_Int, 64, true)

	s8_type  = NewType(TYP_Int,  8, false)
	s16_type = NewType(TYP_Int, 16, false)
	s32_type = NewType(TYP_Int, 32, false)
	s64_type = NewType(TYP_Int, 64, false)

	f32_type = NewType(TYP_Float, 32, false)
	f64_type = NewType(TYP_Float, 64, false)

	usize_type = NewType(TYP_Int, reg_bits, true)
	ssize_type = NewType(TYP_Int, reg_bits, false)

	usize_type.archy = true
	ssize_type.archy = true

	bool_type = NewType(TYP_Bool, 8, false)

	void_type = NewType(TYP_Void, 0, false)
	void_type.size = 0

	noret_type = NewType(TYP_NoReturn, 0, false)
	noret_type.size = 0

	rawmem_type = NewType(TYP_Extern, 0, false)
	rawmem_type.size = 0
}

func NewType(kind BaseType, bits int, unsigned bool) *Type {
	ty := new(Type)
	ty.kind = kind
	ty.bits = bits
	ty.unsigned = unsigned
	if bits > 0 {
		ty.size = int64(bits) / 8
	} else {
		ty.size = -1
	}
	ty.methods = make([]*FuncDef, 0)

	// add it to global list
	ty._sequence = 1 + len(all_types)
	all_types = append(all_types, ty)

	return ty
}

func NewPointerType(sub *Type) *Type {
	if sub._ptr == nil {
		ty := NewType(TYP_Pointer, 0, false)
		ty.bits = arch.RegisterBits()
		ty.size = int64(ty.bits) / 8
		ty.unsigned = true
		ty.archy = true
		ty.sub  = sub

		sub._ptr = ty
	}
	return sub._ptr
}

func GetIntegerType(bits int, unsigned bool) *Type {
	if unsigned {
		switch bits {
		case 8:  return u8_type
		case 16: return u16_type
		case 32: return u32_type
		case 64: return u64_type
		}
	} else {
		switch bits {
		case 8:  return s8_type
		case 16: return s16_type
		case 32: return s32_type
		case 64: return s64_type
		}
	}

	panic("weird bits for GetIntegerType")
}

func CompileAllTypes() {
	// an "alias" is a definition like this: type A B.
	// for these, the alias shares the underlying Type with the original.
	// naturally we must check for infinitely cyclic aliases...

	for _, mod := range ordered_mods {
		for _, def := range mod.ord_defs {
			if def.kind == DEF_Type {
				tdef := def.d_type

				if tdef.CheckAlias() {
					tdef.alias = true
				} else {
					// create a dummy Type now, it is filled out later
					tdef.CreateBareType()
				}
			}
		}
	}

	if ResolveTypeAliases() != OK {
		return
	}

	for _, mod := range ordered_mods {
		cur_mod = mod

		for _, def := range mod.ord_defs {
			if def.kind == DEF_Type && def.d_type.body != nil {
				def.d_type.Compile()
			}
		}
	}
}

func (tdef *TypeDef) CreateBareType() {
	tdef.ty = NewType(TYP_Void, 0, false)

	if tdef.extern {
		tdef.ty.kind = TYP_Extern
		tdef.ty.size = 0

	} else if tdef.body != nil && tdef.body.Len() > 0 && tdef.body.children[0].Match("fun") {
		tdef.ty.kind = TYP_Function
		tdef.ty.size = 0
	}

	// link the Type back to its definition
	tdef.ty.def = tdef
}

func (tdef *TypeDef) CheckAlias() bool {
	body := tdef.body

	if body == nil {
		return false
	}
	if body.kind != NG_Line {
		return false
	}
	if body.Len() != 1 {
		return false
	}

	child := body.children[0]

	if child.kind != NL_Name {
		return false
	}

	// handle module aliases (the `alias` directive)
	ReplaceAlias(child)

	return true
}

func ResolveTypeAliases() error {
	// need to loop until either the number of unresolved aliases
	// drops to zero or no changes were possible.  the latter case
	// means we have cyclic aliases.

	last_count := -1  // first time

	for {
		count     := 0
		example   := ""
		ex_pos    := Position{}
		got_error := false

		for _, mod := range ordered_mods {
			cur_mod = mod

			for _, def := range mod.ord_defs {
				if def.kind == DEF_Type {
					tdef := def.d_type

					if tdef.alias {
						count += 1

						if tdef.TryResolveAlias() != OK {
							got_error = true
						}
						if tdef.alias {
							example = tdef.name
							ex_pos  = tdef.body.pos
						}
					}
				}
			}
		}

		if got_error {
			return FAILED
		}
		if count == 0 {
			return OK
		}
		if count == last_count {
			Error_SetPos(ex_pos)
			PostError("cyclic dependencies with types (such as %s)", example)
			return FAILED
		}

		last_count = count
	}

	return OK
}

func (tdef *TypeDef) TryResolveAlias() error {
	Error_SetPos(tdef.body.pos)

	other   := tdef.body.children[0].str
	oth_mod := tdef.body.children[0].module

	if !ValidateModUsage(oth_mod) {
		return FAILED
	}
	def, mod := LookupDef(other, oth_mod)

	var new_type *Type

	if def != nil {
		if def.kind != DEF_Type {
			PostError("no such type: %s", other)
			return FAILED
		}
		if mod != cur_mod && !def.IsPublic() {
			PostError("%s is private to module %s", other, mod.name)
			return FAILED
		}

		new_type = def.d_type.ty

		if new_type == nil {
			// try again later...
			return OK
		}

	} else {
		new_type = LookupBuiltinType(other)

		if new_type == nil {
			PostError("no such type: %s", other)
			return FAILED
		}
	}

	if new_type == void_type || new_type == noret_type || new_type == rawmem_type {
		PostError("custom types cannot be %s", other)
		return FAILED
	}

	// okay, so remove from the list
	tdef.ty    = new_type
	tdef.alias = false
	tdef.body  = nil

	return OK
}

func (tdef *TypeDef) Compile() error {
	if tdef.kind != "" {
		return tdef.CompileStruct()
	}

	line := tdef.body

	Error_SetPos(line.pos)

	if line.children[0].Match("fun") {
		return tdef.CompileFuncType()
	}

	new_type, count := ParseTypeSpec(line.children, "custom types")
	if new_type == nil {
		return FAILED
	}

	if count < len(line.children) {
		PostError("extra rubbish after type definition")
		return FAILED
	}

	tdef.ty.kind  = new_type.kind
	tdef.ty.bits  = new_type.bits
	tdef.ty.elems = new_type.elems
	tdef.ty.unsigned = new_type.unsigned

	if new_type.size >= 0 {
		tdef.ty.size = new_type.size
	}

	for _, par := range new_type.param {
		tdef.ty.AddParam(par.name, par.ty)
	}
	tdef.ty.sub = new_type.sub

	return OK
}

func (tdef *TypeDef) CompileFuncType() error {
	tdef.ty.kind = TYP_Function
	tdef.ty.size = 0

	children := tdef.body.children

	if len(children) < 2 {
		PostError("missing parameters after 'fun'")
		return FAILED
	}
	if len(children) > 2 {
		PostError("extra rubbish after type definition")
		return FAILED
	}

	param := children[1]

	if param.kind != NG_Expr {
		PostError("expected parameters in (), got: %s", param.String())
		return FAILED
	}

	fun_type := ParseParameters(param)
	if fun_type == nil {
		return FAILED
	}

	// copy parameters and ret-type into existing Type
	for _, par := range fun_type.param {
		tdef.ty.AddParam(par.name, par.ty)
	}
	tdef.ty.sub = fun_type.sub

	return OK
}

func (tdef *TypeDef) CompileStruct() error {
	if tdef.kind == "union" || tdef.kind == "raw-union" {
		tdef.ty.kind = TYP_Union
	} else {
		tdef.ty.kind = TYP_Struct
	}

	if tdef.kind == "struct" || tdef.kind == "union" {
		tdef.ty.padded = true
	}

	for _, line := range tdef.body.children {
		Error_SetPos(line.pos)

		if line.Len() < 2 {
			PostError("missing type spec for struct/union field")
			return FAILED
		}

		t_field := line.children[0]
		if !t_field.IsField() {
			PostError("field names must begin with a dot, got: %s", t_field.str)
			return FAILED
		}

		ctx := "field " + t_field.str
		field_type, count := ParseTypeSpec(line.children[1:], ctx)
		if field_type == nil {
			return FAILED
		}

		if count < len(line.children[1:]) {
			PostError("extra rubbish after field definition")
			return FAILED
		}

		tdef.ty.AddParam(t_field.str, field_type)
	}

	if len(tdef.ty.param) == 0 {
		Error_SetPos(tdef.body.pos)
		PostError("struct/union type cannot be empty")
		return FAILED
	}

	return OK
}

//----------------------------------------------------------------------

func FinalizeTypes() {
	// this also determines field offsets for structs
	DetermineTypeSizes()

	if !have_errors {
		CreateTypeConstants()
	}
}

func DetermineTypeSizes() {
	// collect all the type definitions that are lacking a size.
	// we will process the list multiple times until it shrinks to
	// zero.  if it fails to shrink, it means some types are cyclic
	// in a way which is impossible to actually use them.

	group := make(map[*Type]bool)
	for _, ty := range all_types {
		group[ty] = true
	}

	last_count := 999999999

	for len(group) > 0 {
		// note that we modify the group as we iterate over it.
		// in Go this is legal, may not be legal in other languages.

		for ty, _ := range group {
			if ty.DetermineSize() {
				delete(group, ty)
			}
		}

		if have_errors {
			break
		}

		count := len(group)

		if count == last_count {
			Error_SetFile(0)
			PostError("unable to determine size of some types %s",
				UnsizableExample(group))
			return
		}

		last_count = count
	}
}

func UnsizableExample(group map[*Type]bool) string {
	for ty, _ := range group {
		if ty.def != nil {
			return "(such as " + ty.def.name + ")"
		}
	}
	return ""
}

func (ty *Type) DetermineSize() bool {
	if ty.size >= 0 {
		return true
	}

	if ty.def != nil {
		Error_SetPos(ty.def.body.pos)
	} else {
		Error_SetFile(0)
	}

	// we only need to handle three things here: arrays, structs and
	// unions.  everything else *should* have a size already.

	// WISH: check for overflow / excessively large types

	switch ty.kind {
	case TYP_Array:
		if ty.sub.size == 0 {
			PostError("array type has zero-size elements: %s", ty.String())
			return false
		}
		if ty.sub.size > 0 {
			ty.size = int64(ty.elems) * ty.sub.size
			return true
		}

	case TYP_Struct:
		for _, par := range ty.param {
			if par.ty.size < 0 {
				return false
			}
		}
		ty.size = ty.DetermineOffsets()
		return true

	case TYP_Union:
		ty.size = 0
		for i := range ty.param {
			size := ty.param[i].ty.size
			if size < 0 {
				return false
			}
			if ! ty.padded {
				// a raw-union : trust thy programmer
			} else {
				size = PaddedSize(size)
			}
			if size > ty.size {
				ty.size = size
			}
			ty.param[i].offset = 0
		}
		return true
	}

	return false
}

func (ty *Type) DetermineOffsets() int64 {
	var offset int64 = 0

	for i := range ty.param {
		field_type := ty.param[i].ty

		var elems int64 = 1
		var size  int64 = field_type.size

		// for arrays, the alignment requirements come from element size
		if field_type.kind == TYP_Array {
			arr_type := field_type

			for arr_type.kind == TYP_Array {
				elems = elems * int64(arr_type.elems)
				arr_type = arr_type.sub
			}
			size = arr_type.size
		}

		if ! ty.padded {
			// a raw-struct : trust thy programmer
		} else if elems <= 0 || size <= 0 {
			// do nothing for zero-size things
		} else if size == 1 {
			// can pack single bytes as tightly as possible
		} else {
			size = PaddedSize(size)
			gap := offset % size
			if gap > 0 {
				offset += (size - gap)
			}
		}

		ty.param[i].offset = offset

		offset += elems * size
	}

	if ty.padded {
		offset = PaddedSize(offset)
	}
	return offset
}

func PaddedSize(size int64) int64 {
	if size > 8 { return ((size + 15) / 16) * 16 }
	if size > 4 { return 8 }
	if size > 2 { return 4 }
	return size
}

func CreateTypeConstants() {
	for _, mod := range ordered_mods {
		for name, def := range mod.defs {
			if def.kind == DEF_Type {
				ty := def.d_type.ty
				if ty.size >= 0 {
					cdef := mod.AddConst_Int(name + ".size", int64(ty.size))
					cdef.d_const.public = def.d_type.public
				}
				if ty.kind == TYP_Struct {
					for _, par := range ty.param {
						cdef := mod.AddConst_Int(name + par.name + ".offset", par.offset)
						cdef.d_const.public = def.d_type.public
					}
				}
			}
		}
	}
}

//----------------------------------------------------------------------

func ParseParameters(param *Node) *Type {
	ty := NewType(TYP_Function, 0, false)
	ty.size = 0

	children := param.children

	for len(children) > 0 {
		if len(children) < 2 {
			PostError("malformed parameters")
			return nil
		}
		if ty.sub != nil {
			PostError("extra rubbish after return type")
			return nil
		}

		t_name := children[0]

		if t_name.Match("->") {
			ret, count := ParseTypeSpec2(children[1:], "a function return", true, false)
			if ret == nil {
				return nil
			}
			ty.sub = ret
			children = children[1+count:]
			continue
		}

		if t_name.kind != NL_Name {
			PostError("expected parameter name, got: %s", t_name.String())
			return nil
		}
		if ValidateName(t_name, "parameter", ALLOW_SHADOW) != OK {
			return nil
		}
		if ty.FindParam(t_name.str) >= 0 {
			PostError("duplicate parameter name: %s", t_name.str)
			return nil
		}

		ctx := "parameter '" + t_name.str + "'"
		par_type, count := ParseTypeSpec(children[1:], ctx)
		if par_type == nil {
			return nil
		}

		ty.AddParam(t_name.str, par_type)

		children = children[1+count:]
	}

	if ty.sub == nil {
		ty.sub = void_type
	}
	return ty
}

func ValidateType(ty *Type) error {
	if ty.kind == TYP_Function {
		switch ty.sub.kind {
		case TYP_Array, TYP_Struct, TYP_Union, TYP_Function:
			PostError("bad return type, cannot be %s", ty.sub.SimpleName())
				return FAILED
		}
		for _, par := range ty.param {
			switch par.ty.kind {
			case TYP_Array, TYP_Struct, TYP_Union, TYP_Function:
				PostError("bad type for parameter '%s', cannot be a %s", par.name, par.ty.SimpleName())
				return FAILED
			}
		}
	}

	return OK
}

func ParseTypeSpec(input []*Node, ctx string) (*Type, int) {
	return ParseTypeSpec2(input, ctx, false, false)
}

func ParseTypeSpec2(input []*Node, ctx string, allow_void bool, allow_extern bool) (*Type, int) {
	// NOTE WELL: this code cannot validate anything inside a Type received
	// from ParseTypeSpec or a defined type, since that may be a definition
	// which has not been compiled yet.  But we *can* compare a pointer with
	// a singleton like `void_type` etc...

	if len(input) == 0 {
		panic("no input to ParseTypeSpec")
	}

	head := input[0]

	if head.Match("^") {
		if len(input) < 2 {
			PostError("missing type for pointer")
			return nil, -1
		}

		sub, count := ParseTypeSpec2(input[1:], "a pointer target", false, true)
		if sub == nil {
			return nil, -1
		}

		ty := NewPointerType(sub)
		return ty, 1 + count
	}

	if head.kind == NG_Data {
		if len(input) < 2 {
			PostError("missing type for array")
			return nil, -1
		}
		sub, count := ParseTypeSpec(input[1:], "array elements")
		if sub == nil {
			return nil, -1
		}

		arr_len := ParseArrayLength(head.children)
		if arr_len < 0 {
			return nil, -1
		}

		ty := NewType(TYP_Array, 0, false)
		ty.elems = arr_len
		ty.sub   = sub

		// compute size if possible
		if ty.sub.size == 0 {
			PostError("array type has zero-size elements: %s", ty.String())
			return nil, -1
		}
		if ty.sub.size > 0 {
			ty.size = int64(ty.elems) * ty.sub.size
		}

		return ty, 1 + count
	}

	if head.kind != NL_Name {
		PostError("expected type spec, got: %s", head.String())
		return nil, -1
	}

	if !ValidateModUsage(head.module) {
		return nil, -1
	}
	ReplaceAlias(head)

	def, mod := LookupDef(head.str, head.module)

	var ty *Type

	if def != nil && def.kind == DEF_Type {
		ty = def.d_type.ty

		if mod != cur_mod && !def.IsPublic() {
			PostError("%s is private to module %s", head.str, mod.name)
			return nil, -1
		}

	} else {
		ty = LookupBuiltinType(head.str)
	}

	if ty == nil {
		PostError("no such type: %s", head.str)
		return nil, -1
	}

	if !allow_void {
		if ty == void_type {
			PostError("%s cannot be void type", ctx)
			return nil, -1
		}
		if ty == noret_type {
			PostError("%s cannot be no-return type", ctx)
			return nil, -1
		}
	}

	if !allow_extern {
		if ty == rawmem_type {
			PostError("%s cannot be raw-mem type", ctx)
			return nil, -1
		}

		// despite the scary warning above, we can check for TYP_Extern and
		// TYP_Function here due to the logic in CreateBareType().

		if ty.kind == TYP_Extern {
			PostError("%s cannot be an external type", ctx)
			return nil, -1
		}
		if ty.kind == TYP_Function {
			PostError("%s cannot be a function type", ctx)
			return nil, -1
		}
	}

	// okay
	return ty, 1
}

func LookupBuiltinType(name string) *Type {
	switch name {
	case "u8":  return u8_type
	case "u16": return u16_type
	case "u32": return u32_type
	case "u64": return u64_type

	case "s8":  return s8_type
	case "s16": return s16_type
	case "s32": return s32_type
	case "s64": return s64_type

	case "f32": return f32_type
	case "f64": return f64_type

	case "usize": return usize_type
	case "ssize": return ssize_type

	case "bool":      return bool_type
	case "void":      return void_type
	case "no-return": return noret_type
	case "raw-mem":   return rawmem_type
	}

	return nil
}

func ParseArrayLength(children []*Node) int {
	if len(children) == 0 {
		PostError("missing size in [] for array")
		return -1
	}

	var pctx ParsingCtx
	var ectx EvalContext

	t_len := pctx.ParseExpression(children)
	if t_len == P_FAIL {
		return -1
	}
	t_len = ectx.EvalConstExprs(t_len)
	if t_len == P_FAIL {
		return -1
	}

	if t_len.kind != NL_Integer {
		PostError("bad length for array, wanted int, got: %s", t_len.String())
		return -1
	}

	arr_len, err := strconv.ParseInt(t_len.str, 0, 64)
	if err != nil || arr_len < 0 {
		PostError("bad length for array: %s", t_len.str)
		return -1
	}
	return int(arr_len)
}

//----------------------------------------------------------------------

func (ty *Type) AddParam(name string, par_type *Type) {
	ty.param = append(ty.param, ParamInfo{name, par_type, 0})
}

func (ty *Type) FindParam(name string) int {
	for i, par := range ty.param {
		if par.name == name {
			return i
		}
	}
	return -1  // not found
}

func (ty *Type) AddMethod(fu *FuncDef) {
	ty.methods = append(ty.methods, fu)
}

func (ty *Type) FindMethod(name string) *FuncDef {
	for _, fu := range ty.methods {
		if fu.name == name {
			return fu
		}
	}
	return nil  // not found
}

//----------------------------------------------------------------------

func TypeCompare(src, dest *Type) bool {
	// this works in a structural way.
	// to prevent infinite looping with cyclical types, we keep track
	// of which pairs of types have been compared so far.

	seen_pairs := make(map[uint64]bool)

	return TypeCompare2(src, dest, seen_pairs)
}

func TypeCompare2(src, dest *Type, seen_pairs map[uint64]bool) bool {
	if src  == nil { panic("TypeCompare with nil src")  }
	if dest == nil { panic("TypeCompare with nil dest") }

	if src == dest {
		return true
	}

	if src.kind != dest.kind {
		return false
	}

	switch src.kind {
	case TYP_Void, TYP_NoReturn, TYP_Extern, TYP_Bool:
		return true

	case TYP_Int:
		// arch-dependent types are not compatible with fixed size types.
		// this is disallowed because otherwise it means there can be code
		// which compiles fine on 32-bit but fails to compile on 64-bit,
		// or vice versa.
		if src.archy != dest.archy {
			return false
		}

		if src.unsigned != dest.unsigned {
			return false
		}

		return src.bits == dest.bits

	case TYP_Float:
		return src.bits == dest.bits

	case TYP_Union:
		// perhaps this is too lax...
		return src.size == dest.size
	}

	// produce a value representing this pair of types, and check
	// whether we have processed this pair before.
	pair := (uint64(src._sequence) << 32) | uint64(dest._sequence)

	if seen_pairs[pair] {
		return true
	}

	// need to mark it as seen before recursing.
	seen_pairs[pair] = true

	// pointers have a special rule.
	// we consider ^X and ^[]X and ^[][]X (etc) to be compatible.
	// also allow ^raw-mem to match any other pointer.

	if src.kind == TYP_Pointer {
		if src.sub == rawmem_type || dest.sub == rawmem_type {
			return true
		}

		src  =  src.sub.GetNonArray()
		dest = dest.sub.GetNonArray()

		return TypeCompare2(src, dest, seen_pairs)
	}

	// from here we handle TYP_Struct, TYP_Union, TYP_Array and TYP_Function.

	if len(src.param) != len(dest.param) {
		return false
	}

	if src.sub != nil {
		if !TypeCompare2(src.sub, dest.sub, seen_pairs) {
			return false
		}
	}

	for i := range src.param {
		src_par  := src.param[i]
		dest_par := dest.param[i]

		if !TypeCompare2(src_par.ty, dest_par.ty, seen_pairs) {
			return false
		}
	}

	return true
}

func TypeIsSubtype(src, dest *Type) bool {
	if src.kind != dest.kind {
		return false
	}

	switch src.kind {
	case TYP_Int:
		// for `usize` and `ssize`, extra restrictions are in place.
		// the basis of this logic is that these are at least 32-bits,
		// and hence should allow bits <= 32 to auto-convert.

		if src.archy {
			return false

		} else if dest.archy {
			if src.bits > 32 {
				return false
			}
			if src.unsigned == dest.unsigned {
				return true
			}
			if src.bits == 32 {
				return false
			}
			return src.unsigned

		} else {
			if src.bits < dest.bits {
				if src.unsigned == dest.unsigned {
					return true
				}
				// u8 --> s16 is fine, s8 --> u16 is not.
				return src.unsigned
			}
		}

	case TYP_Float:
		return src.bits < dest.bits
	}

	return false
}

func (ty *Type) IsStructural() bool {
	switch ty.kind {
	case TYP_Array, TYP_Struct, TYP_Union:
		return true
	default:
		return false
	}
}

func (ty *Type) GetNonArray() *Type {
	// infinitely cyclic array types should not be possible, but
	// for safety we don't loop forever here...
	for d := 0; d < 100; d++ {
		if ty.kind != TYP_Array {
			return ty
		}
		ty = ty.sub
	}
	return ty
}

//----------------------------------------------------------------------

func (ty *Type) SimpleName() string {
	return ty.kind.String()
}

func (kind BaseType) String() string {
	switch kind {
	case TYP_Void:     return "void"
	case TYP_NoReturn: return "no-return"
	case TYP_Extern:   return "extern"
	case TYP_Int:      return "integer"
	case TYP_Float:    return "float"
	case TYP_Bool:     return "boolean"
	case TYP_Pointer:  return "pointer"
	case TYP_Array:    return "array"
	case TYP_Struct:   return "struct"
	case TYP_Union:    return "union"
	case TYP_Function: return "function"
	default:
		panic("unknown type")
	}
}

func (ty *Type) String() string {
	// check whether this is a custom type.
	// this is needed to prevent an infinite loop with types
	// which form cyclic references (to itself or other types).
	if ty.def != nil {
		return ty.def.name
	}

	if ty == usize_type {
		return "usize"
	}
	if ty == ssize_type {
		return "ssize"
	}

	switch ty.kind {
	case TYP_Void:
		return "void"

	case TYP_NoReturn:
		return "no-return"

	case TYP_Extern:
		return "extern"

	case TYP_Int:
		s := "s"
		if ty.unsigned {
			s = "u"
		}
		return s + strconv.Itoa(ty.bits)

	case TYP_Float:
		return "f" + strconv.Itoa(ty.bits)

	case TYP_Bool:
		return "bool"

	case TYP_Pointer:
		return "^" + ty.sub.String()

	case TYP_Array:
		return "[]" + ty.sub.String()

	case TYP_Struct:
		return "struct"

	case TYP_Union:
		return "union"

	case TYP_Function:
		s := ""
		for _, par := range ty.param {
			if s != "" { s = s + " " }
			s = s + par.ty.String()
		}
		if ty.sub.kind != TYP_Void {
			if s != "" { s = s + " " }
			s = s + "-> " + ty.sub.String()
		}
		return "fun(" + s + ")"

	default:
		panic("unknown type")
	}
}

func (ty *Type) Encode() string {
	switch ty.kind {
	case TYP_Void, TYP_NoReturn:
		return "void"

	case TYP_Int:
		if ty.archy {
			return "iptr"
		}
		return "i" + strconv.Itoa(ty.bits)

	case TYP_Float:
		return "f" + strconv.Itoa(ty.bits)

	case TYP_Bool:
		return "bool"

	case TYP_Pointer:
		return "ptr"

	default:
		panic("cannot encode complex type")
	}
}
