
Rowan Programming Language
==========================

by Andrew Apted, 2022.


About
-----

TODO


Documentation
-------------

TODO


Binary Packages
---------------

TODO


Status
------

TODO


Legalese
--------

Rowan is under a permissive MIT license.

Rowan comes with NO WARRANTY of any kind, express or implied.

See the [LICENSE.md](LICENSE.md) doc for the full terms.


Sample Code
-----------

```
;;
;; Mandelbrot example
;;
;; by Andrew Apted, 2021.
;;
;; this code is licensed as CC0 (i.e. public domain)
;;

const W = 78
const H = 62

const XOffset = -1.33
const YOffset = -1.20
const XScale  = 45.0
const YScale  = 30.0

const MaxIterations = 64000

#public

fun main () {
    let y = s32 0
    loop while y < H {
        DrawLine y
        y = y + 1
    }
}

fun DrawLine (y s32) {
    let x = s32 0
    loop while x < W {
        let ch = ComputePoint x y
        tinyrt_out_byte ch
        x = x + 1
    }
    tinyrt_out_byte '\r'
    tinyrt_out_byte '\n'
}

fun ComputePoint (sx s32, sy s32 -> u8) {
    let x = f64 sx
    let y = f64 sy

    x = x / XScale + XOffset
    y = y / YScale + YOffset

    let re = x      ; real component of Z
    let im = y      ; imaginary component of Z
    let n  = s32 0  ; number of iterations

    loop {
        n = n + 1
        if n >= MaxIterations {
            return '#'
        }

        let re2 = re * re
        let im2 = im * im

        if re2 + im2 > 4.0 {
            return '.'
        }

        im = y + re * im * 2.0
        re = x + re2 - im2
    }
}

extern-fun tinyrt_out_byte (c u8)
```
