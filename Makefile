#
#  --- ROWAN ---
#
#  Requires GNU make.
#

PROGRAM=rowan

ROWAN=./rowan
SAGELEAF=sageleaf
NASM=nasm
LD=ld

all: $(PROGRAM)

clean: tinyrt-clean samples-clean
	rm -f $(PROGRAM)

$(PROGRAM): *.go
	go build

# --------------------------------------

TINYRT_LIB=tinyrt/unix.o

tinyrt: $(TINYRT_LIB)

tinyrt-clean:
	rm -f tinyrt/*.o

$(TINYRT_LIB): tinyrt/*.s
	$(NASM) -f elf64 tinyrt/unix.s -o $@

# --------------------------------------

samples: $(PROGRAM)  \
	samples/adventure.exe samples/fibonacci.exe samples/hello.exe  \
	samples/input.exe samples/life.exe samples/mandelbrot.exe  \
	samples/sum.exe samples/tailcall.exe

samples/%.exe: samples/%.o $(TINYRT_LIB)
	$(LD) $< $(TINYRT_LIB) -o $@

samples/%.o: samples/%.s
	$(NASM) -f elf64 $< -o $@

samples/%.s: samples/%.ll
	$(SAGELEAF) $< -o $@

samples/%.ll: samples/%.rx
	$(ROWAN) $< -o $@

samples-clean:
	rm -f samples/*.exe
	rm -f samples/*.ll
	rm -f samples/*.o
	rm -f samples/*.s

.PHONY: all clean tinyrt samples samples-clean

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
