// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"
import "strings"
import "unicode/utf8"

func DeduceAllVars() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Var {
			def.d_var.Deduce()
		}
	}
	Error_SetWhat("")
}

func ParseAllVars() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Var && !(def.d_var.extern || def.d_var.zeroed) {
			def.d_var.Parse()
		}
	}
	Error_SetWhat("")
}

func CompileAllVars() {
	cur_mod.public = 0

	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Var {
			def.d_var.Compile()
		}
	}
	Error_SetWhat("")
}

//----------------------------------------------------------------------

// Deduce merely determines the overall type of the variable.
func (va *VarDef) Deduce() error {
	Error_SetWhat(va.name)
	Error_SetPos(va.raw_type.pos)

	ty, count := ParseTypeSpec(va.raw_type.children, "a variable")
	if ty == nil {
		return FAILED
	}
	if count < va.raw_type.Len() {
		PostError("extra rubbish after type in var def")
		return FAILED
	}

	if ty.kind == TYP_Void || ty.kind == TYP_NoReturn {
		PostError("global vars cannot be void or no-return")
		return FAILED
	}
	if ty.kind == TYP_Function {
		PostError("global vars cannot be a raw function (use a pointer)")
		return FAILED
	}

	va.ty = ty
	return OK
}

//----------------------------------------------------------------------

// Parse looks at the expression/body of the variable and produces
// higher-level nodes for the back-end code to use.
func (va *VarDef) Parse() error {
	Error_SetPos(va.raw_type.pos)
	Error_SetWhat(va.name)

	new_expr := va.ParseValue(va.expr, va.ty)
	if new_expr == P_FAIL {
		return FAILED
	}

	va.expr = new_expr
	return OK
}

func (va *VarDef) ParseValue(t *Node, ty *Type) *Node {
	Error_SetPos(t.pos)

	switch ty.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		t = va.ParseSimpleValue(t, ty)

		if t != P_FAIL {
			// check if value fits into type
			if CheckNumericValue(t, ty) != OK {
				return P_FAIL
			}
		}
		return t

	case TYP_Array:
		return va.ParseArray(t, ty)

	case TYP_Struct:
		return va.ParseStruct(t, ty)

	case TYP_Union:
		return va.ParseUnion(t, ty)

	default:
		PostError("strange type in data: %s", ty.SimpleName())
		return P_FAIL
	}
}

func (va *VarDef) ParseSimpleValue(t *Node, ty *Type) *Node {
	if t.kind == NG_Data || t.kind == NG_Block {
		PostError("expected value, got: %s", t.String())
		return P_FAIL
	}

	if t.kind == NG_Expr {
		var pctx ParsingCtx
		var ectx EvalContext

		t = pctx.ParseBasicTerm(t)
		if t == P_FAIL {
			return P_FAIL
		}

		t = ectx.EvalConstExprs(t)
		if t == P_FAIL {
			return P_FAIL
		}
	}

	if t.kind == NL_Name {
		if !ValidateModUsage(t.module) {
			return P_FAIL
		}
		ReplaceAlias(t)

		def, mod := LookupDef(t.str, t.module)

		if def == nil {
			PostError("unknown identifier: %s", t.str)
			return P_FAIL
		}
		if mod != cur_mod && !def.IsPublic() {
			PostError("%s is private to module %s", t.str, mod.name)
			return P_FAIL
		}

		if def.kind == DEF_Const {
			t = def.d_const.MakeLiteral(t.pos)

		} else if ty.kind == TYP_Pointer {
			return va.ParsePointerValue(t, ty, def)

		} else {
			// leave `t` unchanged, trigger an error below
		}
	}

	switch ty.kind {
	case TYP_Int:
		switch t.kind {
		case NL_Integer, NL_Char:
			// ok
		default:
			PostError("expected integer value, got: %s", t.String())
			return P_FAIL
		}

	case TYP_Float:
		switch t.kind {
		case NL_Float, NL_FltSpec:
			// ok
		case NL_Integer, NL_Char:
			// ok, convert integer to a float
			if IntegerToFloat(t) != nil {
				return P_FAIL
			}
		default:
			PostError("expected float value, got: %s", t.String())
			return P_FAIL
		}

	case TYP_Bool:
		switch t.kind {
		case NL_Bool:
			// ok
		default:
			PostError("expected boolean value, got: %s", t.String())
			return P_FAIL
		}

	case TYP_Pointer:
		switch t.kind {
		case NL_String:
			if ty.sub.kind == TYP_Int && ty.sub.unsigned && ty.sub.bits < 64 {
				// ok
			} else {
				PostError("type mismatch with string literal")
				return P_FAIL
			}

		case NL_Null:
			// ok

		default:
			PostError("expected a pointer value, got: %s", t.String())
			return P_FAIL
		}
	}

	t.ty = ty
	return t
}

func (va *VarDef) ParsePointerValue(t *Node, ty *Type, def *Definition) *Node {
	// this should be a global var or function name
	if !(def.kind == DEF_Var || def.kind == DEF_Func) {
		PostError("expected a pointer value, got: %s", t.str)
		return P_FAIL
	}

	// NOTE: we only perform limited type checking here, as there
	// is no way to explicitly cast to a different pointer type.
	// so we just check that functions are used appropriately.

	ty_funky  := (ty.sub.kind == TYP_Function)
	def_funky := (def.kind == DEF_Func)

	if ty_funky && !def_funky {
		PostError("expected a function pointer, got: %s", t.str)
		return P_FAIL
	}
	if !ty_funky && def_funky {
		PostError("type mismatch with function reference")
		return P_FAIL
	}
	if def_funky && def.d_func.inline {
		PostError("cannot take the address of an inline function")
		return P_FAIL
	}

	name := def.Name()

	t_glob := NewNode(NX_Global, name, t.pos)
	t_glob.ty  = ty
	t_glob.def = def

	return t_glob
}

func (va *VarDef) ParseArray(t *Node, ty *Type) *Node {
	if t.kind != NG_Block {
		PostError("expected array data in {}, got: %s", t.String())
		return P_FAIL
	}

	if ty.size == 0 {
		if t.Len() > 0 {
			PostError("expected empty {}")
			return P_FAIL
		}
		return t
	}

	new_array := NewNode(NG_Block, "", t.pos)

	// visit all elements to check type
	t = t.CompressBlock()
	children := t.children

	for len(children) > 0 {
		elem := children[0]
		children = children[1:]

		Error_SetPos(elem.pos)

		// handle `...` which fills remaining values with a default
		if elem.Match("...") {
			if va.FillArray(new_array, ty) != OK {
				return P_FAIL
			}
			break
		}

		// handle strings where int values are expected
		if ty.sub.kind != TYP_Pointer {
			switch elem.kind {
			case NL_String:
				if va.StringToArray(new_array, ty, elem.str) != OK {
					return P_FAIL
				}
				continue

			case NL_Name:
				// no need to replace an alias here
				def, mod := LookupDef(elem.str, elem.module)

				if def != nil && def.kind == DEF_Const && def.d_const.value.kind == NL_String {
					if mod != cur_mod && !def.IsPublic() {
						PostError("%s is private to module %s", elem.str, mod.name)
						return P_FAIL
					}
					if va.StringToArray(new_array, ty, def.d_const.value.str) != OK {
						return P_FAIL
					}
					continue
				}
			}
		}

		elem2 := va.ParseValue(elem, ty.sub)
		if elem2 == P_FAIL {
			return P_FAIL
		}
		new_array.Add(elem2)
	}

	if len(children) > 0 {
		PostError("extra rubbish after '...' in array")
		return P_FAIL
	}
	if new_array.Len() != ty.elems {
		PostError("wrong number of array elements: wanted %d, got %d",
			ty.elems, new_array.Len())
		return P_FAIL
	}

	return new_array
}

func (va *VarDef) StringToArray(new_array *Node, ty *Type, s string) error {
	char_type := ty.sub

	// Note: we disallow the signed types (s8, s16, s32) here.
	// I don't think it makes any sense to allow them.

	if char_type.kind != TYP_Int || !char_type.unsigned {
		PostError("cannot use strings in an array of %s", char_type.String())
		return FAILED
	}

	switch char_type.bits {
	case 8:
		return va.StringToArray_UTF8(new_array, s)

	case 16:
		return va.StringToArray_UTF16(new_array, s)

	case 32:
		return va.StringToArray_UTF32(new_array, s)

	default:
		PostError("cannot use strings in an array of %s", char_type.String())
		return FAILED
	}
}

func (va *VarDef) StringToArray_UTF8(new_array *Node, s string) error {
	// this is easy since in Go strings are already UTF-8
	for i := 0; i < len(s); i++ {
		var ch uint8 = s[i]
		char_str := strconv.Itoa(int(ch))
		t_lit := NewNode(NL_Char, char_str, new_array.pos)
		t_lit.ty = u8_type
		new_array.Add(t_lit)
	}
	return OK
}

func (va *VarDef) StringToArray_UTF16(new_array *Node, s string) error {
	// produce error if string is not valid UTF-8
	// [ which can occur when using octal/hex escapes ]
	if !utf8.ValidString(s) {
		PostError("string is not valid UTF-8")
		return FAILED
	}

	r := []rune(s)

	// the only tricky part here is to convert a large character
	// value to the surrogate pairs.

	for _, ch := range r {
		var word int64 = int64(ch)

		if word > 0xFFFF {
			word -= 0x10000

			high := 0xD800 + ((word >> 10) & 0x03FF)
			word  = 0xDC00 + ( word        & 0x03FF)

			char_str := strconv.FormatInt(high, 10)
			t_lit := NewNode(NL_Char, char_str, new_array.pos)
			t_lit.ty = u16_type
			new_array.Add(t_lit)
		}

		char_str := strconv.FormatInt(word, 10)
		t_lit := NewNode(NL_Char, char_str, new_array.pos)
		t_lit.ty = u16_type
		new_array.Add(t_lit)
	}
	return OK
}

func (va *VarDef) StringToArray_UTF32(new_array *Node, s string) error {
	// produce error if string is not valid UTF-8
	// [ which can occur when using octal/hex escapes ]
	if !utf8.ValidString(s) {
		PostError("string is not valid UTF-8")
		return FAILED
	}

	r := []rune(s)

	for _, ch := range r {
		char_str := strconv.FormatInt(int64(ch), 10)
		t_lit := NewNode(NL_Char, char_str, new_array.pos)
		t_lit.ty = u32_type
		new_array.Add(t_lit)
	}
	return OK
}

func (va *VarDef) FillArray(new_array *Node, ty *Type) error {
	want_len := ty.elems

	for new_array.Len() < want_len {
		t := va.DefaultValue(ty.sub, new_array.pos)
		if t == P_FAIL {
			PostError("cannot use '...' in array of %s", ty.sub.SimpleName())
			return FAILED
		}
		new_array.Add(t)
	}

	return OK
}

func (va *VarDef) ParseStruct(t *Node, ty *Type) *Node {
	if t.kind != NG_Block {
		PostError("expected struct data in {}, got: %s", t.String())
		return P_FAIL
	}

	new_struct := NewNode(NG_Block, "", t.pos)

	t = t.CompressBlock()
	children := t.children

	var offset int64 = 0

	for _, field := range ty.param {
		// when a field name is present, it must match the struct
		if len(children) > 0 && children[0].IsField() {
			t_name := children[0]
			if t_name.str != field.name {
				PostError("expected struct field %s, got %s", field.name, t_name.str)
				return P_FAIL
			}
			children = children[1:]
		}

		if len(children) == 0 {
			PostError("missing element for %s in struct", field.name)
			return P_FAIL
		}

		elem := children[0]
		children = children[1:]

		// handle `...` which fills remaining values with a default
		if elem.Match("...") {
			if len(children) > 0 {
				PostError("extra rubbish after '...' in struct")
				return nil
			}

			// outer logic will add a ND_Zeroes node
			break
		}

		elem2 := va.ParseValue(elem, field.ty)
		if elem2 == P_FAIL {
			return P_FAIL
		}

		// handle gaps (padding) between fields
		gap := field.offset - offset
		new_struct = va.WithPadding(new_struct, gap)

		new_struct.Add(elem2)

		offset = field.offset + field.ty.size
	}

	if len(children) > 0 {
		PostError("too many elements for struct, got: %s", children[0].String())
		return P_FAIL
	}

	// handle padding at end of struct
	new_struct = va.WithPadding(new_struct, ty.size - offset)

	return new_struct
}

/*
func (va *VarDef) FillStruct(new_struct *Node, ty *Type, offset int64) error {
	want_len := len(ty.param)

	for new_struct.Len() < want_len {
		field_idx  := new_struct.Len()
		field_name := ty.param[field_idx].name
		field_type := ty.param[field_idx].ty

		t := va.DefaultValue(field_type, new_struct.pos)
		if t == P_FAIL {
			PostError("cannot use '...' for struct field %s", field_name)
			return FAILED
		}
		new_struct.Add(t)
	}

	return OK
}
*/

func (va *VarDef) ParseUnion(t *Node, ty *Type) *Node {
	if t.kind != NG_Block {
		PostError("expected union data in {}, got: %s", t.String())
		return P_FAIL
	}

	t = t.CompressBlock()

	// handle `...` which fills the union with a default
	if t.Len() >= 1 {
		if t.children[0].Match("...") {
			if t.Len() > 1 {
				PostError("extra rubbish after '...' in union")
				return P_FAIL
			}
			return va.DefaultValue(ty, t.pos)
		}
	}

	if t.Len() < 2 || ! t.children[0].IsField() {
		PostError("union data is missing field name or value")
		return P_FAIL
	}
	if t.Len() > 2 {
		PostError("extra rubbish after union value")
		return P_FAIL
	}

	t_name := t.children[0]

	new_union := NewNode(ND_Union, "", t.pos)

	for _, field := range ty.param {
		if field.name == t_name.str {
			t_value := va.ParseValue(t.children[1], field.ty)
			if t_value == P_FAIL {
				return P_FAIL
			}
			padding := ty.size - field.ty.size

			new_union.str = field.name
			new_union.Add(t_value)

			return va.WithPadding(new_union, padding)
		}
	}

	PostError("no such field %s in union type", t_name.str)
	return P_FAIL
}

func (va *VarDef) DefaultValue(ty *Type, pos Position) *Node {
	switch ty.kind {
	case TYP_Int:
		t_lit := NewNode(NL_Integer, "0", pos)
		t_lit.ty = ty
		return t_lit

	case TYP_Float:
		t_lit := NewNode(NL_Float, "0.0", pos)
		t_lit.ty = ty
		return t_lit

	case TYP_Pointer:
		t_lit := NewNode(NL_Null, "", pos)
		t_lit.ty = NewPointerType(rawmem_type)
		return t_lit

	case TYP_Bool:
		t_lit := NewNode(NL_Bool, "0", pos)
		t_lit.ty = bool_type
		return t_lit

	case TYP_Array, TYP_Struct, TYP_Union:
		t_zero := NewNode(ND_Zeroes, "", pos)
		t_zero.size = ty.size
		return t_zero

	default:
		// parent must show an appropriate error!
		return P_FAIL
	}
}

func (va *VarDef) WithPadding(t *Node, padding int64) *Node {
	if padding == 0 {
		return t
	}
	if padding < 0 {
		panic("negative padding?")
	}
	if t.kind != NG_Block {
		t_old := t
		t = NewNode(NG_Block, "", t_old.pos)
		t.Add(t_old)
	}
	t_zero := NewNode(ND_Zeroes, "", t.pos)
	t_zero.size = padding

	t.Add(t_zero)
	return t
}

func (t *Node) CompressBlock() *Node {
	res := NewNode(NG_Block, "", t.pos)
	for _, line := range t.children {
		for _, child := range line.children {
			res.Add(child)
		}
	}
	return res
}

//----------------------------------------------------------------------

func (va *VarDef) Compile() error {
	out_name := "@" + EncodeName(va.name, va.module)

	if va.extern {
		OutLine("extern %s", out_name)
		OutLine("")
		return OK
	}

	OutVisibility(va.public)

	if va.zeroed {
		size := va.ty.size

		OutLine("var %s = zero %d", out_name, size)
		OutLine("")
		return OK
	}

	keyword := "var"
	if va.rom {
		keyword = keyword + " rom"
	}

	switch va.ty.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// simple types put value on same line
		val_str := EncodeValue(va.expr, true)
		OutLine("%s %s = %s", keyword, out_name, val_str)
		OutLine("")

	default:
		// complex values occur over multiple lines
		OutLine("%s %s =", keyword, out_name)

		va.CompileData(va.expr)

		OutLine("end")
		OutLine("")
	}

	return OK
}

func (va *VarDef) CompileData(t *Node) {
	if t.kind == ND_Union {
		va.CompileData(t.children[0])
		return
	}

	if t.kind != NG_Block {
		OutCode("%s", EncodeValue(t, true))
		return
	}

	// put all values on a single line, when feasible
	if va.EstimateInline(t) < 200 {
		var last_type *Type = nil
		var line string = ""

		for i, child := range t.children {
			if i > 0 {
				line = line + "  "
			}
			if last_type != nil && child.ty == last_type {
				line = line + EncodeValueNoType(child)
			} else {
				line = line + EncodeValue(child, true)
			}
			last_type = child.ty
		}
		OutCode("%s", line)
		return
	}

	for _, child := range t.children {
		va.CompileData(child)
	}
}

func (va *VarDef) EstimateInline(block *Node) int {
	for _, child := range block.children {
		switch child.kind {
		case NG_Block, ND_Union:
			// not a simple value
			return 9999
		}
	}

	// WISH: better estimate
	return block.Len() * 8
}

//----------------------------------------------------------------------

func EncodeValue(elem *Node, is_data bool) string {
	switch elem.kind {
	case ND_Zeroes:
		num_str := strconv.FormatInt(elem.size, 10)
		return "zero " + num_str

	case NL_Null:
		return "ptr 0"

	case NL_Integer, NL_Float, NL_FltSpec:
		return elem.ty.Encode() + " " + elem.str

	case NL_Char:
		ch, err := strconv.ParseInt(elem.str, 0, 64)
		if err != nil || ch < 0 {
			panic("bad NL_Char")
		}
		char_str := EncodeCharLiteral(rune(ch))
		return elem.ty.Encode() + " " + char_str

	case NL_Bool:
		if elem.str == "1" {
			return "bool 1"
		} else {
			return "bool 0"
		}

	case NL_String:
		glob_name := ""

		switch elem.ty.sub.bits {
			case 8:  glob_name = cur_mod.AddStringUTF8 (elem.str)
			case 16: glob_name = cur_mod.AddStringUTF16(elem.str)
			case 32: glob_name = cur_mod.AddStringUTF32(elem.str)
			default: panic("string with odd type")
		}
		if is_data { return "ptr " + glob_name }
		return glob_name

	case NX_Global:
		out_name := elem.def.Encode()
		if is_data { return "ptr " + out_name }
		return out_name

	default:
		panic("odd node to EncodeValue: " + elem.String())
	}
}

func EncodeValueNoType(elem *Node) string {
	switch elem.kind {
	case NL_Null:
		return "0"

	case NL_Integer, NL_Float, NL_FltSpec:
		return elem.str

	case NL_Char:
		ch, err := strconv.ParseInt(elem.str, 0, 64)
		if err != nil || ch < 0 {
			panic("bad NL_Char")
		}
		return EncodeCharLiteral(rune(ch))

	case NL_Bool:
		if elem.str == "1" {
			return "1"
		} else {
			return "0"
		}

	case NL_String, NX_Global:
		return EncodeValue(elem, false)

	default:
		return EncodeValue(elem, true)
	}
}

//----------------------------------------------------------------------

func EncodeCharLiteral(ch rune) string {
	switch ch {
	case '\'': return "'\\''"
	case '\\': return "'\\\\'"
	}

	if LEX_Printable(ch) || ch == ' ' {
		return "'" + string(ch) + "'"
	}

	return "'" + EncodeEscapedChar(ch) + "'"
}

func EncodeStringLiteral(s string) string {
	var sb strings.Builder

	sb.WriteRune('"')

	for _, ch := range s {
		if	ch == '"' || ch == '\\' {
			sb.WriteRune('\\')
			sb.WriteRune(ch)

		} else if LEX_Printable(ch) || ch == ' ' {
			sb.WriteRune(ch)

		} else {
			sb.WriteString(EncodeEscapedChar(ch))
		}
	}

	sb.WriteRune('"')
	return sb.String()
}

func EncodeEscapedChar(ch rune) string {
	switch ch {
	case 7:  return "\\a"  // bell
	case 8:  return "\\b"  // backspace
	case 9:  return "\\t"  // tab
	case 10: return "\\n"  // linefeed
	case 11: return "\\v"  // vertical tab
	case 12: return "\\f"  // formfeed
	case 13: return "\\r"  // carriage return
	case 27: return "\\e"  // escape
	}

	s := strconv.FormatInt(int64(ch), 16)

	if ch < 128 {
		if len(s) < 2 { s = "0" + s }
		return "\\x" + strings.ToUpper(s)
	} else if ch < 0x10000 {
		for len(s) < 4 { s = "0" + s }
		return "\\u" + strings.ToUpper(s)
	} else {
		for len(s) < 8 { s = "0" + s }
		return "\\U" + strings.ToUpper(s)
	}
}

//----------------------------------------------------------------------

func CompileStrings() {
	// collect the strings to ensure the right order
	values8  := make([]string, len(cur_mod.utf8_strings))
	values16 := make([]string, len(cur_mod.utf16_strings))
	values32 := make([]string, len(cur_mod.utf32_strings))

	for v, idx := range cur_mod.utf8_strings {
		values8[idx] = v
	}
	for v, idx := range cur_mod.utf16_strings {
		values16[idx] = v
	}
	for v, idx := range cur_mod.utf32_strings {
		values32[idx] = v
	}

	total := len(values8) + len(values16) + len(values32)
	if total == 0 {
		return
	}

	OutLine("private")
	OutLine("")

	for idx, s := range values8 {
		WriteString(8, idx, s)
	}
	for idx, s := range values16 {
		WriteString(16, idx, s)
	}
	for idx, s := range values32 {
		WriteString(32, idx, s)
	}
}

func WriteString(bits int, idx int, s string) {
	// FIXME handle invalid UTF-8 when bits == 8
	// TODO  break long strings up (over multiple lines)

	OutLine("var rom @utf%d_string%d =", bits, idx)

	s = EncodeStringLiteral(s)

	type_name := "i8"
	if bits == 16 { type_name = "i16" }
	if bits == 32 { type_name = "i32" }

	// have a trailing zero to ensure string is NUL terminated

	OutCode("%s %s 0", type_name, s)
	OutLine("end")
	OutLine("")
}
