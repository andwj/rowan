// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "bufio"

type InputFile struct {
	name    string
	public  bool  // current visibility (default is private)
	lexer   *Lexer
}

//----------------------------------------------------------------------

func NewInputFile(name string, filenum int, r io.Reader) *InputFile {
	reader := bufio.NewReader(r)

	f := new(InputFile)

	f.name   = name
	f.public = false
	f.lexer  = NewLexer(reader, filenum)

	return f
}

// Load reads the input file, decodes each line into low-level tokens
// and does some awesome shit.  Returns OK if everything was fine, or
// FAILED if an error occurred.
func (f *InputFile) Load() error {
	for {
		t := f.lexer.Scan()

		if t.kind == NL_EOF {
			if have_errors {
				return FAILED
			}
			return OK
		}

		// NOTE: on parse failures we generally skip ahead to the next
		//       known directive, allowing multiple syntax errors to be
		//       found and reported.

		Error_SetPos(t.pos)

		if t.kind == NL_ERROR {
			PostError("%s", t.str)
			f.lexer.SkipAhead()
			continue
		}

		head := t.children[0]

		if head.kind != NL_Name {
			PostError("expected a directive, got: %s", head.String())
			f.lexer.SkipAhead()
			continue
		}

		switch {
		case head.Match("alias"):
			if f.LoadAlias(t) != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("const"):
			if f.LoadConst(t) != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("type"):
			if f.LoadType(t, 0) != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("extern-type"):
			if f.LoadType(t, 'e') != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("var"):
			if f.LoadVar(t, 0) != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("extern-var"):
			if f.LoadVar(t, 'e') != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("rom-var"):
			if f.LoadVar(t, 'r') != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("zero-var"):
			if f.LoadVar(t, 'z') != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("fun"):
			if f.LoadFunction(t, 0) != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("extern-fun"):
			if f.LoadFunction(t, 'e') != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("inline-fun"):
			if f.LoadFunction(t, 'i') != OK {
				f.lexer.SkipAhead()
			}

		case head.Match("#public"):
			f.public = true

		case head.Match("#private"):
			f.public = false

		default:
			PostError("unknown directive: %s", head.str)
			f.lexer.SkipAhead()
		}
	}
}

func (f *InputFile) Load_DEBUG() error {
	for {
		t := f.lexer.Scan()

		Error_SetPos(t.pos)

		if t.kind == NL_ERROR {
			println("** ERROR IN LINE", t.pos.line, ":", t.str)
			return OK
		}

		if t.kind == NL_EOF {
			println("** EOF **")
			return OK
		}

		Dump(" ", t, 0)
	}
}

//----------------------------------------------------------------------

func (f *InputFile) LoadAlias(line *Node) error {
	children := line.children

	if len(children) < 4 {
		PostError("alias def is missing name or target")
		return FAILED
	}
	if ! children[2].Match("=") {
		PostError("alias def is missing '=' after name")
		return FAILED
	}
	if len(children) > 4 {
		PostError("extra rubbish at end of alias def")
		return FAILED
	}

	t_src  := children[1]
	t_dest := children[3]

	if ValidateName(t_src, "alias", 0) != OK {
		return FAILED
	}

	if t_dest.kind != NL_Name {
		PostError("bad alias: expected identifer, got: %s", t_dest.String())
		return FAILED
	}

	modname := t_dest.module

	if modname == "" {
		PostError("bad alias: target missing a module prefix")
		return FAILED
	}
	if modname == cur_mod.name {
		PostError("bad alias: target must refer a different module")
		return FAILED
	}

	if !ValidateModUsage(modname) {
		return FAILED
	}

	def := cur_mod.AddAlias(t_src.str)
	def.d_alias.target = t_dest.str
	def.d_alias.module = t_dest.module

	return OK
}

//----------------------------------------------------------------------

func (f *InputFile) LoadConst(line *Node) error {
	children := line.children

	if len(children) < 4 {
		PostError("const def is missing name or value")
		return FAILED
	}
	if !children[2].Match("=") {
		PostError("const def is missing '=' after name")
		return FAILED
	}

	t_name  := children[1]
	children = children[3:]

	if ValidateName(t_name, "const", 0) != OK {
		return FAILED
	}

	def := cur_mod.AddConst(t_name.str)

	con := def.d_const
	con.public = f.public

	t_value := children[0]

	if len(children) > 1 {
		t_value = NewNode(NG_Expr, "", t_value.pos)
		for _, child := range children {
			t_value.Add(child)
		}
	}

	// NL_Null and NL_FltSpec are deliberately excluded below...

	switch t_value.kind {
	case NL_Integer, NL_Float, NL_Char, NL_Bool, NL_String:
		con.expr  = nil
		con.value = t_value

	case NG_Expr, NL_Name:
		// this will be evaluated later (in CompileAllConstants)
		con.expr  = t_value
		con.value = nil

	default:
		PostError("bad const def: expected value, got: %s", t_value.String())
		return FAILED
	}

	return OK
}

//----------------------------------------------------------------------

func (f *InputFile) LoadType(line *Node, mode rune) error {
	children := line.children

	if len(children) < 2 || (mode != 'e' && len(children) < 4) {
		PostError("type def is missing name or definition")
		return FAILED
	}

	t_name := children[1]

	flags := 0
	if mode == 'e' {
		flags = ALLOW_MODULE
	}

	if ValidateName(t_name, "type", flags) != OK {
		return FAILED
	}

	def := cur_mod.AddType(t_name.str)

	tdef := def.d_type
	tdef.extern = (mode == 'e')
	tdef.public = f.public && !tdef.extern

	// Note: the actual Type is created later on...

	// something with a body?
	if mode != 'e' {
		if ! children[2].Match("=") {
			PostError("type def is missing '=' after name")
			return FAILED
		}
		t_part := children[3]

		if	t_part.Match("struct") || t_part.Match("raw-struct") ||
			t_part.Match("union")  || t_part.Match("raw-union") {

			if len(children) < 5 {
				PostError("type def is missing definition after %s", t_part.str)
				return FAILED
			}
			if len(children) > 5 {
				PostError("extra rubbish after body of type def")
				return FAILED
			}
			tdef.kind = t_part.str
			tdef.body = children[4]

			if tdef.body.kind != NG_Block {
				PostError("expected {} block after %s, got:%s", t_part.str,
					tdef.body.String())
				return FAILED
			}

		} else {
			tdef.body = NewNode(NG_Line, "", line.pos)
			for _, child := range children[3:] {
				tdef.body.Add(child)
			}
		}
	}

	return OK
}

//----------------------------------------------------------------------

func (f *InputFile) LoadVar(line *Node, mode rune) error {
	children := line.children

	if len(children) < 3 {
		PostError("variable is missing name or type")
		return FAILED
	}

	t_name := children[1]

	flags := 0
	if mode == 'e' {
		flags = ALLOW_MODULE
	}

	if ValidateName(t_name, "variable", flags) != OK {
		return FAILED
	}

	def := cur_mod.AddVar(t_name.str)
	v   := def.d_var

	v.extern = (mode == 'e')
	v.zeroed = (mode == 'z')
	v.rom    = (mode == 'r')
	v.public = f.public && !v.extern

	if v.extern {
		v.module = ""
	}

	// check if we have an inline value
	equal_pos := -1
	for i := 2; i < len(children); i++ {
		if children[i].Match("=") {
			equal_pos = i
			break
		}
	}

	if equal_pos == 2 {
		PostError("missing type for variable")
		return FAILED
	}

	// collect type tokens, they are parsed later
	raw_type := NewNode(NG_Line, "", line.pos)
	ty_end   := equal_pos
	if equal_pos < 0 { ty_end = len(children) }

	for k := 2; k < ty_end; k++ {
		raw_type.Add(children[k])
	}

	v.raw_type = raw_type

	// collect the value expression, if present
	if equal_pos > 0 {
		if v.extern || v.zeroed {
			PostError("unexpected value for variable")
			return FAILED
		}

		children = children[1+equal_pos:]

		if len(children) == 0 {
			PostError("missing value for variable")
			return FAILED

		} else if len(children) == 1 {
			v.expr = children[0]

		} else if children[0].kind == NG_Block {
			PostError("extra rubbish after data in {}")
			return FAILED

		} else {
			v.expr = NewNode(NG_Expr, "", line.pos)
			for _, child := range children {
				v.expr.Add(child)
			}
		}

		return OK
	}

	if !(v.extern || v.zeroed) {
		PostError("missing value for variable, expected '='")
		return FAILED
	}

	return OK
}

//----------------------------------------------------------------------

func (f *InputFile) LoadFunction(line *Node, mode rune) error {
	children := line.children

	if len(children) < 3 {
		PostError("function is missing name or parameters")
		return FAILED
	}

	t_name  := children[1]
	t_param := children[2]

	is_method := false

	if t_name.IsMethod() {
		is_method = true

	} else {
		flags := 0
		if mode == 'e' {
			flags = ALLOW_MODULE
		}
		if ValidateName(t_name, "function", flags) != OK {
			return FAILED
		}
	}

	fu := new(FuncDef)
	fu.name   = t_name.str
	fu.extern = (mode == 'e')
	fu.inline = (mode == 'i')
	fu.method = is_method
	fu.public = f.public && !fu.extern
	fu.raw_pars = t_param

	if is_method {
		cur_mod.AddMethod(fu)
	} else {
		cur_mod.AddFunc(fu.name, fu)
	}

	if fu.extern {
		fu.module = ""

		// I... ain't got no body
		if len(children) > 3 {
			PostError("extra rubbish after function parameters")
			return FAILED
		}
		return OK
	}

	// handle body of function....

	if len(children) < 4 {
		PostError("missing body for function")
		return FAILED
	}
	if len(children) > 4 {
		PostError("extra rubbish after function body")
		return FAILED
	}

	fu.body = children[3]

	if fu.body.kind != NG_Block {
		PostError("expected {} block for function body, got: %s", fu.body.String())
		return FAILED
	}

	return OK
}
