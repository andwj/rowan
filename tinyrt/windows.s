;; Copyright 2021 Andrew Apted.
;; Use of this code is governed by an MIT-style license.
;; See the top-level "LICENSE.md" file for the full text.

%define STD_INPUT_HANDLE   -10
%define STD_OUTPUT_HANDLE  -11

section .text

extern main

global start
global exit

start:
	; make the stack pointer 16-byte aligned
	xor	rbp,rbp
	push	rbp

	; reserve stack for the shadow area
	sub	rsp,4*8

	; get input and output handles
	mov	rcx,STD_INPUT_HANDLE
	call	[rel GetStdHandle]
	mov	[rel in_handle],rax

	mov	rcx,STD_OUTPUT_HANDLE
	call	[rel GetStdHandle]
	mov	[rel out_handle],rax

	call	main

	mov	ecx,0
exit:
	push	rbp
	mov	rbp,rsp
	sub	rsp,4*8

	; this will not return
	call	[rel ExitProcess]

	hlt


;;
;; panic functions
;;

global panic
global panic_null
global panic_range

panic_null:
	lea	r8,[rel .message]
	jmp	panic

.message:	db	"null pointer dereferenced", 0


panic_range:
	lea	r8,[rel .message]
	jmp	panic

.message:	db	"array index out of range", 0


panic:
	push	rbp
	mov	rbp,rsp
	sub	rsp,4*8

	; the first two parameters (file and line) are ignored

	mov	r12,r8
	lea	rcx,[rel .prefix]
	call	tinyrt_out_str

	mov	rcx,r12
	call	tinyrt_out_str

	mov	cl,13  ; '\r'
	call	tinyrt_out_byte

	mov	cl,10  ; '\n'
	call	tinyrt_out_byte

	mov	ecx,1
	jmp	exit

.prefix:	db	"panic: ", 0


;;
;; input/output functions
;;

section .text

global tinyrt_in_byte
global tinyrt_out_byte
global tinyrt_out_hex
global tinyrt_out_hex32
global tinyrt_out_hex16
global tinyrt_out_hex8
global tinyrt_out_str

tinyrt_in_byte:
	push	rbp
	mov	rbp,rsp
	sub	rsp,8*8

	xor	rax,rax

	mov	[rsp+4*8],rax  ; overlap info = NULL
	mov	[rsp+5*8],rax
	mov	[rbp-8],rax    ; storage for read count

	mov	rcx,[rel in_handle]
	lea	rdx,[rel buffer]
	mov	r8d,1       ; wanted size
	lea	r9,[rbp-8]  ; bytes read (out)

	call	[rel ReadFile]

	; check for error/EOF just by looking at read count
	mov	eax,[rbp-8]
	test	eax,eax
	jz	.error

	xor	rax,rax
	mov	al,[rel buffer]

	leave
	ret
.error:
	mov	rax,dword -1

	leave
	ret


tinyrt_out_byte:
	push	rbp
	mov	rbp,rsp
	sub	rsp,8*8

	xor	rax,rax

	mov	[rsp+4*8],rax  ; overlap info = NULL
	mov	[rsp+5*8],rax
	mov	[rbp-8],rax    ; storage for written count

	; store character in the buffer
	mov	[rel buffer+0],cl
	mov	[rel buffer+1],al

	mov	rcx,[rel out_handle]
	lea	rdx,[rel buffer]
	mov	r8d,1       ; length
	lea	r9,[rbp-8]  ; written bytes (out)

	call	[rel WriteFile]
.done:
	leave
	ret


tinyrt_out_str:
	push	rbp
	mov	rbp,rsp
	sub	rsp,6*8

	mov	[rbp-8],r15
	mov	r15,rcx
.loop:
	mov	cl,[r15]
	or	cl,cl
	jz	.done

	call	tinyrt_out_byte

	inc	r15
	jmp	.loop
.done:
	mov	r15,[rbp-8]

	leave
	ret


tinyrt_out_hex:
	push	rbp
	mov	rbp,rsp
	sub	rsp,6*8

	mov	[rbp-8],rcx
	shr	rcx,32
	call	tinyrt_out_hex32

	mov	rcx,[rbp-8]
	call	tinyrt_out_hex32

	leave
	ret


tinyrt_out_hex32:
	push	rbp
	mov	rbp,rsp
	sub	rsp,6*8

	mov	[rbp-8],rcx
	shr	rcx,16
	call	tinyrt_out_hex16

	mov	rcx,[rbp-8]
	call	tinyrt_out_hex16

	leave
	ret


tinyrt_out_hex16:
	push	rbp
	mov	rbp,rsp
	sub	rsp,6*8

	mov	[rbp-8],rcx
	shr	rcx,8
	call	tinyrt_out_hex8

	mov	rcx,[rbp-8]
	call	tinyrt_out_hex8

	leave
	ret


tinyrt_out_hex8:
	push	rbp
	mov	rbp,rsp
	sub	rsp,6*8

	mov	[rbp-8],rcx
	shr	rcx,4
	call	tinyrt_out_hex4

	mov	rcx,[rbp-8]
	call	tinyrt_out_hex4

	leave
	ret


tinyrt_out_hex4:
	push	rbp
	mov	rbp,rsp
	sub	rsp,4*8

	and	cl,15
	add	cl,48  ; '0'
	cmp	cl,58
	jb	.over

	add	cl,65-58  ; 'A'
.over:
	call	tinyrt_out_byte

	leave
	ret


tinyrt_out_dec:
	push	rbp
	mov	rbp,rsp
	sub	rsp,6*8

	test	rcx,rcx
	jns	.over

	mov	[rbp-8],rcx

	mov	cl,45  ; '-'
	call	tinyrt_out_byte

	mov	rcx,[rbp-8]
	neg	rcx
.over:
	call	tinyrt_out_dec_recursive

	leave
	ret


tinyrt_out_dec_recursive:
	push	rbp
	mov	rbp,rsp
	sub	rsp,6*8

	cmp	rcx,10
	jb	.small

	mov	rax,rcx
	xor	rdx,rdx

	mov	rcx,10
	div	rcx

	; save the remainder
	mov	[rbp-8],rdx

	; recurse to print the most significant digits
	mov	rcx,rax
	call	tinyrt_out_dec_recursive

	mov	rcx,[rbp-8]
.small:
	add	cl,48   ; '0'

	call	tinyrt_out_byte

	leave
	ret


;;
;; variables
;;

section .data

in_handle:
	dq	0
out_handle:
	dq	0
buffer:
	dq	0


;;
;; import data for KERNEL32.DLL
;;

section	.idata$2

	dd	0,0,0
	dd	kernel32_name    wrt ..imagebase
	dd	kernel32_imports wrt ..imagebase
	dd	0,0,0,0,0
	align	8

kernel32_imports:

ExitProcess:
	dd	ExitProcess_info  wrt ..imagebase, 0
GetStdHandle:
	dd	GetStdHandle_info wrt ..imagebase, 0
ReadFile:
	dd	ReadFile_info     wrt ..imagebase, 0
WriteFile:
	dd	WriteFile_info    wrt ..imagebase, 0

	dq	0  ; end of table
	align	8

kernel32_name:
	db	"KERNEL32.DLL",0
	align	8

ExitProcess_info:
	db	0,0,"ExitProcess",0
	align	8

GetStdHandle_info:
	db	0,0,"GetStdHandle",0
	align	8

ReadFile_info:
	db	0,0,"ReadFile",0
	align	8

WriteFile_info:
	db	0,0,"WriteFile",0
	align	8


;--- editor settings -------
; vi:ts=10:sw=10:noexpandtab
