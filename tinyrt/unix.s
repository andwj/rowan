;; Copyright 2021 Andrew Apted.
;; Use of this code is governed by an MIT-style license.
;; See the top-level "LICENSE.md" file for the full text.

%define sys_read	0
%define sys_write	1
%define sys_exit	60

section .text

extern main

global _start
global exit

_start:
	xor	rbp,rbp
	call	main
	mov	edi,0

exit:
	; this will not return
	mov	eax,sys_exit
	syscall
	hlt


;;
;; panic functions
;;

global panic
global panic_null
global panic_range

panic_null:
	lea	rdx,[rel .message]
	jmp	panic

.message:	db	"null pointer dereferenced", 0


panic_range:
	lea	rdx,[rel .message]
	jmp	panic

.message:	db	"array index out of range", 0


panic:
	push	rbp
	mov	rbp,rsp

	; the first two parameters (file and line) are ignored

	mov	r12,rdx
	lea	rdi,[rel .prefix]
	call	tinyrt_out_str

	mov	rdi,r12
	call	tinyrt_out_str

	mov	dil,10  ; '\n'
	call	tinyrt_out_byte

	mov	edi,99
	jmp	exit

.prefix:	db	"panic: ", 0


;;
;; input/output functions
;;

%define STDIN		0
%define STDOUT		1
%define STDERR		2

global tinyrt_in_byte
global tinyrt_out_byte
global tinyrt_out_hex
global tinyrt_out_hex32
global tinyrt_out_hex16
global tinyrt_out_hex8
global tinyrt_out_dec
global tinyrt_out_str

tinyrt_in_byte:
	push	rbp
	mov	rbp,rsp

	; use the stack as our buffer
	sub	rsp,16

	mov	edi,STDIN     ; fd
	lea	rsi,[rbp-4]   ; buf
	mov	edx,1         ; count

	mov	eax,sys_read
	syscall

	test	rax,rax
	jz	.eof
	js	.error

	xor	rax,rax
	mov	al,[rbp-4]

	leave
	ret
.eof:
.error:
	mov	rax,dword -1

	leave
	ret


tinyrt_out_byte:
	push	rbp
	mov	rbp,rsp

	; use the stack as our buffer
	sub	rsp,16
	mov	[rbp-4],dil

	mov	edi,STDOUT    ; fd
	lea	rsi,[rbp-4]   ; buf
	mov	edx,1         ; count

	mov	eax,sys_write
	syscall

	leave
	ret


tinyrt_out_str:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],r15
	mov	r15,rdi
.loop:
	mov	dil,[r15]
	or	dil,dil
	jz	.done

	call	tinyrt_out_byte

	inc	r15
	jmp	.loop
.done:
	mov	r15,[rbp-8]

	leave
	ret


tinyrt_out_hex:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],rdi
	shr	rdi,32
	call	tinyrt_out_hex32

	mov	rdi,[rbp-8]
	call	tinyrt_out_hex32

	leave
	ret


tinyrt_out_hex32:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],rdi
	shr	rdi,16
	call	tinyrt_out_hex16

	mov	rdi,[rbp-8]
	call	tinyrt_out_hex16

	leave
	ret


tinyrt_out_hex16:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],rdi
	shr	rdi,8
	call	tinyrt_out_hex8

	mov	rdi,[rbp-8]
	call	tinyrt_out_hex8

	leave
	ret


tinyrt_out_hex8:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],rdi
	shr	rdi,4
	call	tinyrt_out_hex4

	mov	rdi,[rbp-8]
	call	tinyrt_out_hex4

	leave
	ret


tinyrt_out_hex4:
	push	rbp
	mov	rbp,rsp

	and	dil,15
	add	dil,48  ; '0'
	cmp	dil,58
	jb	.over

	add	dil,65-58  ; 'A'
.over:
	call	tinyrt_out_byte

	leave
	ret


tinyrt_out_dec:
	push	rbp
	mov	rbp,rsp

	test	rdi,rdi
	jns	.over

	push	rdi
	push	rdi

	mov	dil,45  ; '-'
	call	tinyrt_out_byte

	pop	rdi
	pop	rdi

	neg	rdi
.over:
	call	tinyrt_out_dec_recursive

	leave
	ret


tinyrt_out_dec_recursive:
	push	rbp
	mov	rbp,rsp

	cmp	rdi,10
	jb	.small

	mov	rax,rdi
	xor	rdx,rdx

	mov	ecx,10
	div	rcx

	; save the remainder
	push	rdx
	push	rdx

	; recurse to print the most significant digits
	mov	rdi,rax
	call	tinyrt_out_dec_recursive

	pop	rdi
	pop	rdi
.small:
	add	dil,48   ; '0'

	call	tinyrt_out_byte

	leave
	ret

;--- editor settings -------
; vi:ts=10:sw=10:noexpandtab
