// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "sort"
import "strconv"
import "path/filepath"

var all_mods     map[string]*Module
var ordered_mods []*Module

// the main module does not prefix identifiers in the output assembly
// (all other modules get a prefix).
var main_mod *Module

// the current module being parsed/compiled
var cur_mod *Module

type Module struct {
	// this is the prefix (without '::') used to distinguish identifiers
	// in compiled code.  also used for default output filename.
	name  string

	// the order in the .list file.  modules of a higher rank cannot
	// use modules of a lower rank.
	rank  int

	// all the files belonging to the module.
	files []string

	// file being parsed for this module, either part of a .list file,
	// or a separate file.
	in_file *InputFile
	block   *Node

	// the output assembly file
	out_file   string
	out_fp     *os.File
	out_errors bool

	// all the definitions in this module
	defs     map[string]*Definition
	ord_defs []*Definition

	// current visibility in output: 0 unset, +1 public, -1 private
	public  int

	// all the definitions in other modules used by this one
	foreign_refs []*ForeignRef
	foreign_map  map[string]bool

	// all the strings used in a pointer context
	utf8_strings  map[string]int
	utf16_strings map[string]int
	utf32_strings map[string]int
}

type ForeignRef struct {
	mod *Module
	def *Definition
}

func InitModules() {
	all_mods = make(map[string]*Module)
	ordered_mods = make([]*Module, 0)
}

func NewModule(name string) *Module {
	mod := new(Module)

	mod.name  = name
	mod.files = make([]string, 0)
	mod.defs  = make(map[string]*Definition)
	mod.ord_defs = make([]*Definition, 0)
	mod.foreign_refs  = make([]*ForeignRef, 0)
	mod.foreign_map   = make(map[string]bool)
	mod.utf8_strings  = make(map[string]int)
	mod.utf16_strings = make(map[string]int)
	mod.utf32_strings = make(map[string]int)

	all_mods[name] = mod
	ordered_mods = append(ordered_mods, mod)

	return mod
}

func (mod *Module) AddAlias(name string) *Definition {
	alias := new(AliasDef)
	alias.name = name

	def := new(Definition)
	def.kind = DEF_Alias
	def.d_alias = alias

	mod.defs[name] = def
	return def
}

func (mod *Module) AddConst(name string) *Definition {
	con := new(ConstDef)
	con.name = name
	con.module = mod.name

	def := new(Definition)
	def.kind = DEF_Const
	def.d_const = con

	mod.defs[name] = def
	return def
}

func (mod *Module) AddConst_Int(name string, value int64) *Definition {
	def := mod.AddConst(name)
	con := def.d_const

	str := strconv.FormatInt(value, 10)
	con.value = NewNode(NL_Integer, str, Position{})

	return def
}

func (mod *Module) AddType(name string) *Definition {
	tdef := new(TypeDef)
	tdef.name = name
	tdef.module = mod.name

	def := new(Definition)
	def.kind = DEF_Type
	def.d_type = tdef

	mod.defs[name] = def
	return def
}

func (mod *Module) AddVar(name string) *Definition {
	v := new(VarDef)
	v.name = name
	v.module = mod.name

	def := new(Definition)
	def.kind = DEF_Var
	def.d_var = v

	mod.defs[name] = def
	return def
}

func (mod *Module) AddFunc(name string, fu *FuncDef) *Definition {
	fu.module = mod.name

	def := new(Definition)
	def.kind = DEF_Func
	def.d_func = fu

	mod.defs[name] = def
	return def
}

func (mod *Module) AddMethod(fu *FuncDef) * Definition {
	fu.module = mod.name

	def := new(Definition)
	def.kind = DEF_Method
	def.d_func = fu

	mod.ord_defs = append(mod.ord_defs, def)
	return def
}

func (mod *Module) AddForeignRef(other *Module, def *Definition) {
	switch def.kind {
	case DEF_Alias, DEF_Const, DEF_Type:
		// constants and types don't need anything
		return
	}

	// determine a string to check for uniqueness
	ref_str := other.name + "::" + fmt.Sprintf("%p", def)

	_, exist := mod.foreign_map[ref_str]
	if exist {
		return
	}
	mod.foreign_map[ref_str] = true

	ref := new(ForeignRef)
	ref.mod = other
	ref.def = def

	mod.foreign_refs = append(mod.foreign_refs, ref)
}

//----------------------------------------------------------------------

func BuildSingleModule(filenames []string) {
	// this is used when the command line contains a bunch of bare
	// code files (no ".list" file).

	mod := NewModule("main")

	main_mod = mod

	for _, fn := range filenames {
		mod.files = append(mod.files, fn)
	}

	// determine output filename
	if Options.out_dir != "" {
		mod.out_file = Options.out_dir
	} else {
		first := filenames[0]
		bare  := FileRemoveExtension(first)
		ext   := ".ll"
		mod.out_file = bare + ext
	}
}

func LoadModuleList(listfile string) {
	f_idx := AddFilename(listfile)

	read_fp, err := os.Open(listfile)
	if err != nil {
		Error_SetFile(0)

		if os.IsNotExist(err) {
			FatalError("no such file: %s", listfile)
		} else {
			FatalError("could not open file: %s", listfile)
		}
		return
	}

	f := NewInputFile(listfile, f_idx, read_fp)

	ReadModuleList(f)

	read_fp.Close()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	// first module in the ".list" file will be main
	main_mod = ordered_mods[0]

	SortModules()
}

func SortModules() {
	sort.Slice(ordered_mods, func(i, k int) bool {
		d1 := ordered_mods[i]
		d2 := ordered_mods[k]

		return d1.name < d2.name
	})
}

func ReadModuleList(f *InputFile) error {
	for {
		t := f.lexer.Scan()

		if t.kind == NL_EOF {
			break;
		}

		Error_SetPos(t.pos)

		if t.kind == NL_ERROR {
			PostError("bad .list file: %s", t.str)
			return FAILED
		}

		head := t.children[0]

		if head.kind != NL_Name {
			PostError("bad .list file: expected a directive, got: %s", head.String())
			return FAILED
		}
		if head.Match("module") {
			if ParseModuleDef(f, t) != OK {
				return FAILED
			}
		} else {
			PostError("bad .list file: unknown directive: %s", head.str)
			return FAILED
		}
	}

	if len(all_mods) == 0 {
		PostError("bad .list file: no modules!")
		return FAILED
	}
	return OK
}

func ParseModuleDef(f *InputFile, t *Node) error {
	if t.Len() < 2 {
		PostError("missing name in module directive")
		return FAILED
	}
	if t.Len() < 3 {
		PostError("missing info in module directive")
		return FAILED
	}
	if t.Len() > 3 {
		PostError("extra rubbish at end of module directive")
		return FAILED
	}

	t_name := t.children[1]
	if ValidateName(t_name, "module", ALLOW_SHADOW) != OK {
		return FAILED
	}

	_, exist := all_mods[t_name.str]
	if exist {
		PostError("bad module name: '%s' already defined", t_name.str)
		return FAILED
	}

	mod := NewModule(t_name.str)

	mod.rank = len(all_mods)

	// determine the default output filename
	dir := Options.out_dir
	if dir == "" {
		dir = "."
	} else {
		// create the directory
		os.MkdirAll(dir, 0777)
	}
	bare  := mod.name  // FIXME ensure this is an OK filename
	ext   := ".ll"
	mod.out_file = filepath.Join(dir, bare + ext)

	// process the definition....

	block := t.children[2]

	if block.kind != NG_Block {
		Error_SetPos(t.pos)
		PostError("expected a block, got: %s", block.String())
		return FAILED
	}

	// load definition in lines of the block

	mod.in_file = f
	mod.block = block

	if mod.Parse() != OK {
		return FAILED
	}

	if len(mod.files) == 0 {
		Error_SetPos(t.pos)
		PostError("no files in module definition of '%s'", mod.name)
		return FAILED
	}

	return OK
}

func (mod *Module) Parse() error {
	f := mod.in_file

	for {
		var t *Node

		if mod.block == nil {
			t = f.lexer.Scan()
			if t.kind == NL_EOF {
				return OK
			}
		} else {
			if mod.block.Len() == 0 {
				return OK
			}
			t = mod.block.PopHead()
		}

		Error_SetPos(t.pos)

		if t.kind == NL_ERROR {
			PostError("%s", t.str)
			return FAILED
		}

		if t.kind != NG_Line {
			PostError("bad module: expected a line, got: %s", t.String())
			return FAILED
		}

		head := t.children[0]

		if head.kind != NL_Name {
			PostError("bad module: expected a directive, got: %s", head.String())
			return FAILED
		}

		var err error

		if head.Match("file") {
			err = mod.Parse_File(t)
		} else if head.Match("output") {
			err = mod.Parse_Output(t)
		} else {
			PostError("bad module: unknown directive: %s", head.str)
			return FAILED
		}

		if err != OK {
			return FAILED
		}
	}

	return OK
}

func (mod *Module) Parse_File(t *Node) error {
	if t.Len() < 2 {
		PostError("missing name in file directive")
		return FAILED
	}
	if t.Len() > 2 {
		PostError("extra rubbish at end of file directive")
		return FAILED
	}

	t_file := t.children[1]

	if t_file.kind != NL_String || t_file.str == "" {
		PostError("filename must be a non-empty string")
		return FAILED
	}

	// check for a duplicate
	for _, existing := range mod.files {
		if existing == t_file.str {
			PostError("duplicate filename: %s", t_file.str)
			return FAILED
		}
	}

	mod.files = append(mod.files, t_file.str)

	return OK
}

func (mod *Module) Parse_Output(t *Node) error {
	if t.Len() < 2 {
		PostError("missing name in output directive")
		return FAILED
	}
	if t.Len() > 2 {
		PostError("extra rubbish at end of output directive")
		return FAILED
	}

	t_file := t.children[1]

	if t_file.kind != NL_String || t_file.str == "" {
		PostError("filename must be a non-empty string")
		return FAILED
	}

	mod.out_file = t_file.str
	return OK
}

//----------------------------------------------------------------------

func CreateOrderedDefs() {
	for _, mod := range ordered_mods {
		mod.OrderDefs()
	}
}

func (mod *Module) OrderDefs() {
	for _, def := range mod.defs {
		mod.ord_defs = append(mod.ord_defs, def)
	}

	sort.Slice(mod.ord_defs, func(i, k int) bool {
		d1 := mod.ord_defs[i]
		d2 := mod.ord_defs[k]

		// output private stuff before public stuff
		pub1 := d1.IsPublic()
		pub2 := d2.IsPublic()

		if !pub1 &&  pub2 { return true  }
		if  pub1 && !pub2 { return false }

		// tie breaker
		return d1.Name() < d2.Name()
	})
}

func LookupDef(name string, modref string) (*Definition, *Module) {
	mod := cur_mod
	if modref != "" {
		mod = all_mods[modref]
	}
	if mod == nil {
		return nil, nil
	}

	def := mod.defs[name]
	if def == nil {
		return nil, nil
	}

	// remember usages of definitions outside of this module
	if mod != cur_mod {
		cur_mod.AddForeignRef(mod, def)
	}
	return def, mod
}

func ValidateModUsage(modref string) bool {
	if modref == "" {
		return true
	}

	mod := all_mods[modref]

	if mod == nil {
		PostError("no such module: %s", modref)
		return false
	}

	if mod.rank < cur_mod.rank {
		PostError("module %s is out of scope to module %s", modref, cur_mod.name)
		return false
	}

	return true
}

//----------------------------------------------------------------------

func DeduceAllModules() {
	for _, mod := range ordered_mods {
		cur_mod = mod

		DeduceAllFunctions()
		DeduceAllVars()
	}
}

func ParseAllModules() {
	for _, mod := range ordered_mods {
		cur_mod = mod

		ParseAllFunctions()
		ParseAllVars()
	}
}

func CompileAllModules() {
	for _, mod := range ordered_mods {
		cur_mod = mod
		cur_mod.BeginOutput()

		CompileAllVars()
		CompileAllFunctions()
		CompileForeignRefs()
		CompileStrings()

		cur_mod.FinishOutput()
	}
}

func CompileForeignRefs() {
	for _, ref := range cur_mod.foreign_refs {
		full_name := ref.Encode()

		OutLine("extern %s", full_name)
		OutLine("")
	}
}

//----------------------------------------------------------------------

func (mod *Module) AddStringUTF8(s string) string {
	idx, exist := mod.utf8_strings[s]
	if !exist {
		idx = len(mod.utf8_strings)
		mod.utf8_strings[s] = idx
	}
	return "@utf8_string" + strconv.Itoa(idx)
}

func (mod *Module) AddStringUTF16(s string) string {
	idx, exist := mod.utf16_strings[s]
	if !exist {
		idx = len(mod.utf16_strings)
		mod.utf16_strings[s] = idx
	}
	return "@utf16_string" + strconv.Itoa(idx)
}

func (mod *Module) AddStringUTF32(s string) string {
	idx, exist := mod.utf32_strings[s]
	if !exist {
		idx = len(mod.utf32_strings)
		mod.utf32_strings[s] = idx
	}
	return "@utf32_string" + strconv.Itoa(idx)
}

//----------------------------------------------------------------------

func (mod *Module) BeginOutput() {
	var err error

	mod.out_fp, err = os.Create(mod.out_file)
	if err != nil {
		FatalError("cannot create output: %s", err.Error())
	}
}

func (mod *Module) FinishOutput() {
	if mod.out_errors {
		FatalError("failure writing to the output file: %s", mod.out_file)
	}

	mod.out_fp.Close()
	mod.out_fp = nil
}

func OutLine(format string, a ...interface{}) {
	if cur_mod == nil || cur_mod.out_fp == nil {
		panic("OutLine with no destination")
	}

	if cur_mod.out_errors {
		return
	}

	format += "\n"

	_, err := fmt.Fprintf(cur_mod.out_fp, format, a...)
	if err != nil {
		cur_mod.out_errors = true
	}
}

func OutCode(format string, a ...interface{}) {
	OutLine("\t" + format, a...)
}

func OutIns(ins, args string, a ...interface{}) {
	line := "\t" + ins

	if args != "" {
		line += "\t" + fmt.Sprintf(args, a...)
	}

	OutLine("%s", line)
}

func OutVisibility(public bool) {
	if cur_mod.public > 0 &&  public { return }
	if cur_mod.public < 0 && !public { return }

	if public {
		cur_mod.public = +1
		OutLine("public")
		OutLine("")
	} else {
		cur_mod.public = -1
		OutLine("private")
		OutLine("")
	}
}
