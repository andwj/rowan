
ROWAN GRAMMAR
=============

Introduction
------------

This document describes the grammar (syntax) of this language.
The grammar is specified using an extended BNF (Backus-Naur form),
where `|` represents a choice, things in `[]` square brackets are
optional, and things in `{}` curly braces may be repeated zero or
more times.  Uppercase names represent low-level tokens, possibly
with some language-specific meaning, e.g. `USER-TYPE` is the name
of a user-defined type.


Overall Format
--------------

The overall format of an input file is textual and line-based.
The encoding must be UTF-8 (the Unicode character set).
Each line is terminated by the `LF` (line feed) character.
The `CR` carriage return may appear before `LF`, and is ignored.
Other control characters act as whitespace.

Each input line is decomposed into a sequence of tokens.
The characters `()[]{},:^` always form individual tokens, all
other tokens are delimited by whitespace or one of those symbols.

If the final token on a line is `\` (a backslash), then the logical
line is extended with tokens from the next input line, and this can
repeated multiple times.  Conversely, the `,` (comma) symbol can be
used to split a single physical line into multiple logical lines.

The `;` (semicolon) character introduces a comment which extends to
the end of the line, and the whole sequence is equivalent to whitespace.
Multiline comments begin with a line beginning with the `#[` directive,
and extends until a line beginning with the `#]` directive.  These
comments do not nest.

The three opening braces `(`, `[` or `{` begin a logical group of
tokens.  The group must be terminated by the corresponding closing
brace `)`, `]` or `}`.  Groups in `{}` curly braces are called "blocks"
and keep the tokens in logical lines, whereas the other groups ignore
line boundaries.

Character literals are delimited by single quote characters.
Strings literals are delimited by double quote characters, and
must be contained on a single line.  These can contain escape
sequences to produce any valid Unicode character.


Input file
----------

```
file := { directive LF }

directive := def-alias
          |  def-type
          |  def-global
          |  def-function
          |  privacy-directive

def-alias := "alias" NAME "=" SCOPED-NAME

privacy-directive := "#public"
                  |  "#private"
```


Types
-----

```
def-type := "type" NAME "=" type-spec
         |  "extern-type" NAME

type-spec := simple-type
          |  USER-TYPE
          |  array-type
          |  struct-type
          |  union-type
          |  pointer-type
          |  function-type

simple-type := "s8"  | "s16" | "s32" | "s64" | "ssize"
            |  "u8"  | "u16" | "u32" | "u64" | "usize"
            |  "f32" | "f64"
            |  "bool"
            |  "void"
            |  "no-return"

array-type  := "[" const-expr "]" type-spec

struct-type := ("struct" | "raw-struct") "{" LF { field-def } LF "}"

union-type  := ("union" | "raw-union") "{" LF { field-def } LF "}"

field-def   := FIELD-NAME type-spec

pointer-type  := "^" type-spec
              |  "^" "raw-mem"

function-type := "fun" func-params
```


Global data
-----------

```
def-global  := var-kind NAME type-spec [ "=" var-data ]

var-kind    := "var" | "rom-var" | "zero-var" | "extern-var"

var-data    := data-block | const-expr

data-block  := "{" [LF] { data-term [LF] } [ "..." ] "}"

data-elem   := [ FIELD-NAME ] const-term

const-expr  := { un-op } const-term { bin-op { un-op } const-term }

const-term  := "(" const-expr ")"
            |  "min" basic-term { basic-term }
            |  "max" basic-term { basic-term }
            |  basic-term

bin-op  := "*"  | "/"  | "&"  | "<<" | ">>"
        |  "+"  | "-"  | "|"  | "^"
        |  "<"  | "<=" | ">"  | ">=" | "==" | "!="
        |  "and" | "or"

un-op   := "-"     | "~"
        |  "not"   | "abs"   | "sqrt"
        |  "round" | "trunc" | "floor" | "ceil"
        |  "zero?" | "some?" | "neg?"  | "pos?"
        |  "inf?"  | "nan?"  | "null?" | "ref?"
        |  "big-endian"
        |  "little-endian"
        |  "byte-swap"
```


Code structure
--------------

```
def-function := ("fun" | "inline-fun") NAME func-params block
             |  "extern-fun" NAME func-params

func-params := "(" { parameter } [ "->" type-spec ")"

parameter   := NAME type-spec

block := "{" LF { line LF } "}"

line  := def-local
      |  assignment
      |  statement
      |  block
      |  computation

def-local  := "let" NAME "=" computation

assignment := dest "=" computation
           |  "_"  "=" computation
           |  "swap" dest dest

dest := LOCAL
     |  GLOBAL
     |  "[" object { indexor } "]"

statement  := block
           |  "do" [ LABEL ] block
           |  "loop" [ loop-cond ] block
           |  "jump" LABEL [ jump-cond ]
           |  "break"    [ jump-cond ]
           |  "continue" [ jump-cond ]
           |  "return"   basic-term [ jump-cond ]
           |  "return"   [ computation ]
           |  "assert" computation
           |  "panic"  STRING
           |  if-statement
           |  match-statement
           |  tail-call

loop-cond := ("while" | "until") computation

jump-cond := ("if" | "unless") computation

if-statement := "if" computation block [ "else" (if-statement | block) ]

match-statement := "match" computation "{" { match-rule } "}"

match-rule := "case" case-values block

case-values := "_"
            |  basic-term { basic-term }

tail-call := "tail-call" basic-term { basic-term }
```


Computations
------------

```
computation := expression

expression := { un-op } conv-expr { bin-op { un-op } conv-expr }

conv-expr := { conv-prefix } method-expr

conv-prefix := "cast" type-spec
            |  "^"    type-spec
            |  simple-type
            |  USER-TYPE

method-expr := term { method-call }

term  := "matches?" basic-term { basic-term }
      |  "min"      basic-term { basic-term }
      |  "max"      basic-term { basic-term }
      |  "stack-var" type-spec
      |  explicit-call
      |  implicit-call
      |  basic-term

explicit-call := "call" basic-term { basic-term }

implicit-call := FUNC-NAME { basic-term }

method-call   := METHOD-NAME { basic-term }

basic-term := constant
           |  LOCAL
           |  GLOBAL
           |  LITERAL
           |  FIELD-NAME
           |  object-access
           |  "(" expression ")"

constant := "NULL"
         |  "FALSE" | "TRUE"
         |  "+INF"  | "-INF"
         |  "QNAN"  | "SNAN"
         |  "CPU_WORD_SIZE"
         |  "CPU_ENDIAN"

object-access := "[" [ "ref" ] object { indexor } "]"

object := basic-term

indexor := FIELD-NAME
        |  basic-term
```
