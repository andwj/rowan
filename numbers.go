// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "math"
import "strconv"

func CheckLiteral(t *Node, ty *Type) error {
	// checks that the literal can be given the type `ty`.
	// when okay, the `ty` field of the node is updated.
	// the literal may be modified, e.g. integer "255" for `s8`
	// will be changed to "-1".  node kind may be changed.

	if ty == nil {
		panic("CheckLiteral with no type")
	}
	if ! t.IsLiteral() {
		panic("CheckLiteral with non literal")
	}

	t.ty = ty

	if ty.kind == TYP_Int && (t.kind == NL_Integer || t.kind == NL_Char) {
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Float && t.kind == NL_Float {
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Float && (t.kind == NL_Integer || t.kind == NL_Char) {
		if IntegerToFloat(t) != OK {
			return FAILED
		}
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Float && t.kind == NL_FltSpec {
		return OK
	}
	if ty.kind == TYP_Bool && t.kind == NL_Bool {
		return OK
	}
	if ty.kind == TYP_Pointer && t.kind == NL_Null {
		return OK
	}
	if ty.kind == TYP_Pointer && t.kind == NL_String {
		if ty.sub.kind == TYP_Int && ty.sub.unsigned && ty.sub.bits < 64 {
			return OK
		}
		PostError("type mismatch with string literal")
		return FAILED
	}

	PostError("type mismatch with literal, wanted a %s", ty.SimpleName())
	return FAILED
}

func CheckNumericValue(t *Node, ty *Type) error {
	// see if the literal fits into the target type

	switch t.kind {
	case NL_Integer, NL_Char:
		// NOTE WELL: we allow some leeway here, the value needs to
		// fit into either a signed or unsigned version of the type.
		// for example, values between -128 to 255 are accepted for
		// both s8 and u8, but not anything outside of that range.

		v1, err1 := strconv.ParseInt (t.str, 0, ty.bits)
		v2, err2 := strconv.ParseUint(t.str, 0, ty.bits)

		if err1 != nil && err2 != nil {
			sign := 's'
			if ty.unsigned { sign = 'u' }
			PostError("value is too large for a %c%d", sign, ty.bits)
			return FAILED
		}

		// if value is outside the natural range (e.g. 255 for s8),
		// then adjust the literal to make it natural.
		// [ even though NASM does not care, other backends might ]

		// WISH: if literal was hexadecimal, make new version hex too

		if err1 != nil && !ty.unsigned {
			var new_val int64
			switch ty.bits {
			case 8:  new_val = int64(int8 (v2))
			case 16: new_val = int64(int16(v2))
			case 32: new_val = int64(int32(v2))
			case 64: new_val = int64(      v2 )
			}
			t.str = strconv.FormatInt(new_val, 10)

		} else if err2 != nil && ty.unsigned {
			var new_val uint64
			switch ty.bits {
			case 8:  new_val = uint64(uint8 (v1))
			case 16: new_val = uint64(uint16(v1))
			case 32: new_val = uint64(uint32(v1))
			case 64: new_val = uint64(       v1 )
			}
			t.str = strconv.FormatUint(new_val, 10)
		}

	case NL_Float:
		var err error
		if ty.bits == 32 {
			_, err = strconv.ParseFloat(t.str, 32)
		} else {
			_, err = strconv.ParseFloat(t.str, 64)
		}
		if err != nil {
			PostError("float value is too large for a f%d", ty.bits)
			return FAILED
		}
	}

	return OK
}

func IntegerForceSize(t *Node, bits int) error {
	// we need to handle full range of s64 *and* u64, so parse twice
	v1, err1 := strconv.ParseInt (t.str, 0, 64)
	v2, err2 := strconv.ParseUint(t.str, 0, 64)

	if err1 != nil && err2 != nil {
		PostError("integer literal is too large")
		return FAILED
	}

	// WISH: if literal was hexadecimal, make new version hex too

	var new_val int64

	switch bits {
	case 8:
		if err1 != nil {
			new_val = int64(uint8(v2))
		} else if v1 < -128 || v1 > 255 {
			new_val = int64(uint8(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 16:
		if err1 != nil {
			new_val = int64(uint16(v2))
		} else if v1 < -0x8000 || v1 > 0xFFFF {
			new_val = int64(uint16(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 32:
		if err1 != nil {
			new_val = int64(uint32(v2))
		} else if v1 < -0x80000000 || v1 > 0xFFFFFFFF {
			new_val = int64(uint32(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 64:
		// leave it as-is
	}

	return OK
}

func IntegerToBool(t *Node) {
	// negative or zero becomes FALSE.
	// everything else becomes TRUE.

	t.kind = NL_Bool
	t.ty   = bool_type

	if t.str[0] != '-' {
		v, err := strconv.ParseUint(t.str, 0, 64)
		if err != nil || v > 0 {
			t.str = "1"
			return
		}
	}

	t.str = "0"
}

func FloatToBool(t *Node) {
	// negative or zero becomes FALSE.
	// everything else becomes TRUE.

	t.kind = NL_Bool
	t.ty   = bool_type

	if t.str[0] != '-' {
		v, err := strconv.ParseFloat(t.str, 64)
		if err != nil || v > 0 {
			t.str = "1"
			return
		}
	}

	t.str = "0"
}

func IntegerToFloat(t *Node) error {
	// convert hex to decimal
	if ( len(t.str) >= 2 && t.str[1] == 'x') ||
		(len(t.str) >= 3 && t.str[2] == 'x') {

		if t.str[0] == '-' {
			v, err := strconv.ParseInt(t.str, 0, 64)
			if err != nil {
				PostError("integer literal is too large")
				return FAILED
			}
			t.str = strconv.FormatInt(v, 10)

		} else {
			v, err := strconv.ParseUint(t.str, 0, 64)
			if err != nil {
				PostError("integer literal is too large")
				return FAILED
			}
			t.str = strconv.FormatUint(v, 10)
		}
	}

	t.kind = NL_Float
	t.str  = t.str + ".0"

	return OK
}

func FloatToInteger(t *Node) error {
	val, err := strconv.ParseFloat(t.str, 64)
	if err != nil {
		PostError("float value is too large")
		return FAILED
	}

	// check the value is in range for a s64.  this may exclude a few
	// values at the limits, but that is better than allowing values
	// *past* the limit and getting a bogus result.
	const limit = 9.223372036854775e+18

	if val < -limit || val > limit {
		PostError("float value is too large")
		return FAILED
	}

	new_val := int64(val)

	t.kind = NL_Integer
	t.str  = strconv.FormatInt(new_val, 10)

	return OK
}

func IntegerScaleToShift(scale int) int {
	// returns -1 when scale is not a power of two.

	if scale <= 0 {
		panic("bad scale for IntegerScaleToShift")
	}

	res := 0

	for {
		if scale == 1 {
			return res
		}
		if (scale & 1) == 1 {
			return -1
		}
		scale = scale >> 1
		res   = res + 1
	}
}

func InfinityOrNan(n float64) bool {
	return math.IsInf(n, 0) || math.IsNaN(n)
}

func LiteralIsZero(t *Node) bool {
	switch t.kind {
	case NL_Integer, NL_Char:
		return t.str == "0" || t.str == "0x0"
	case NL_Float:
		return t.str == "0.0"
	}
	return false
}

func LiteralIsOne(t *Node) bool {
	switch t.kind {
	case NL_Integer, NL_Char:
		return t.str == "1" || t.str == "0x1"
	}
	return false
}

func LiteralIsFalse(t *Node) bool {
	return t.kind == NL_Bool && t.str == "0"
}

func LiteralIsTrue(t *Node) bool {
	return t.kind == NL_Bool && t.str == "1"
}
