// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

/* The code in this file is primarily concerned with syntax, it is the
   main place where special syntax is decoded into a simpler node tree
   (the other place is "expr.go" for expressions).
*/

//import "strconv"

// these extra node kinds are the result of parsing the raw tokens
// from the lexer into something the compiler can consume.
// [ literals like NL_Integer are also used ]
const (
	NX_INVALID NodeKind = 100 + iota

	/* expressions */

	NX_Const      // str is name of global const.  def is set
	NX_Func       // str is name of global func.   def is set
	NX_Global     // str is name of global var.    def is set
	NX_Local      // str is name.  lvar is a LocalVar struct

	NX_Unary      // value.          str is operator name
	NX_Binary     // lhs    | rhs.   str is operator name
	NX_Call       // func   | zero or more parameters
	NX_Method     // method | one or more parameters (first is self)

	NX_Deref      // pointer obj
	NX_ChkNull    // pointer obj
	NX_Field      // struct obj.  str is field name
	NX_Index      // array obj | index value
	NX_AddrOf     // access expr

	NX_Conv       // value.  ty is the target type.
	NX_RawCast    // value.  ty is the target type.
	NX_StackVar   // ty is the wanted type.

	NX_Matches    // main value | one or more compare values
	NX_Min        // children are the values
	NX_Max        // children are the values

	/* statements */

	NS_Return     // child is value (optional)
	NS_Implicit   // child is implicit return value
	NS_TailCall   // child is NX_Call or NX_Method

	NS_Jump       // condition (optional).  str is label name
	NS_Continue   // condition (optional).  str is label name
	NS_Break      // condition (optional).  str is label name

	NS_Let        // child is value.  `local` is var info
	NS_If         // cond | true-clause | false-clause (optional)
	NS_Loop       // body | condition (optional).
	NS_Assign     // dest | value
	NS_Swap       // dest | dest2

	NS_Match      // value | zero or more NS_Case
	NS_Case       // body  | zero or more literals

	NS_Ignore     // child is expression
	NS_Assert     // child is expression
	NS_Panic      // str is the message

	/* data nodes */

	ND_Array      // children are the array elements
	ND_Struct     // children are the field values
	ND_Union      // a union field value.  str is field name
	ND_Zeroes     // used for elided arrays and structs in data
)

type ParsingCtx struct {
	// this is nil if outside a function (e.g. global vars)
	fu *FuncDef

	// current block for new statements
	block *Node

	// the current scope for local vars
	scope *Scope

	// loop information -- a stack
	loops []*LoopInfo
}

type LoopInfo struct {
	start_label string
	break_label string
}

func (ctx *ParsingCtx) PushScope(kind string) *Scope {
	scope := NewScope(kind)
	scope.parent = ctx.scope
	ctx.scope = scope
	return scope
}

func (ctx *ParsingCtx) PopScope() {
	if ctx.scope == nil {
		panic("PopScope without push")
	}
	ctx.scope = ctx.scope.parent
}

func (ctx *ParsingCtx) PushLoop() *LoopInfo {
	if ctx.loops == nil {
		ctx.loops = make([]*LoopInfo, 0)
	}
	info := new(LoopInfo)
	ctx.loops = append(ctx.loops, info)
	return info
}

func (ctx *ParsingCtx) PopLoop() {
	if len(ctx.loops) == 0 {
		panic("PopLoop without push")
	}
	ctx.loops = ctx.loops[0:len(ctx.loops)-1]
}

//----------------------------------------------------------------------

func (fu *FuncDef) Parse() error {
	Error_SetWhat(fu.name)

	// setup some stuff
	fu.labels = make(map[string]*LabelInfo)
	fu.locals = make([]*LocalVar, 0)
	fu.params = make([]*LocalVar, 0)
	fu.jumps  = make([]*LabelInfo, 0)

	var ctx ParsingCtx
	ctx.fu = fu

	scope := ctx.PushScope("function")

	// create a local for each parameter
	for idx, par := range fu.ty.param {
		local := fu.NewLocal(par.name, par.ty)
		local.param_idx = idx
		fu.params = append(fu.params, local)

		scope.AddLocal(local)

		// self is immutable
		if idx == 0 && fu.method {
			local.self      = true
			local.immutable = true
		}
	}

	old_body := fu.body

	new_body := ctx.ParseBlock("body", old_body, true /* tail */)
	if new_body == P_FAIL {
		return FAILED
	}

	fu.body = new_body

	ctx.PopScope()

	if ctx.scope != nil {
		panic("PushScope without pop")
	}

//	Dumpty(fu.name, fu.body, 0)

	if fu.DetectMissingLabels() != OK {
		return FAILED
	}
	if fu.DetectMissingReturn() != OK {
		return FAILED
	}
	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseBlock(kind string, block *Node, tail bool) *Node {
	fu := ctx.fu

	new_block := NewNode(NG_Block, "", block.pos)

	// empty block?
	if block.Len() == 0 {
		return new_block
	}

	ctx.PushScope(kind)

	save_bk := ctx.block
	ctx.block = new_block

	for idx := 0; idx < block.Len(); idx++ {
		line    := block.children[idx]
		is_last := (idx == block.Len() - 1)

		if ctx.ParseLine(line, is_last && tail) != OK {
			// allow a limited number of errors per function
			fu.error_num += 1
			if fu.error_num > 5 && !Options.all_errors {
				break
			}
		}
	}

	ctx.block = save_bk
	ctx.PopScope()

	if fu.error_num > 0 {
		return P_FAIL
	}
	return new_block
}

func (ctx *ParsingCtx) ParseLine(line *Node, tail bool) error {
	// note that lines are never empty...

	fu := ctx.fu

	Error_SetPos(line.pos)

	if line.kind != NG_Line {
		panic("weird node for ParseLine")
	}

	// add a comment line (for final assembly)
	text := fu.LineAsText(line)
	comment := NewNode(NH_Comment, text, line.pos)
	ctx.block.Add(comment)

	head := line.children[0]

	if head.kind == NG_Block && line.Len() == 1 {
		block := ctx.ParseBlock("do", head, tail)
		if block == P_FAIL {
			return FAILED
		}
		ctx.block.Add(block)
		return OK
	}

	// handle special statements....
	// most of these are void and produce no value.
	switch {
	case head.Match("return"):
		return ctx.ParseReturn(line)

	case head.Match("do"):
		return ctx.ParseDo(line, tail)

	case head.Match("let"):
		return ctx.ParseLet(line)

	case head.Match("if"):
		return ctx.ParseIf(line, tail)

	case head.Match("match"):
		return ctx.ParseMatch(line, tail)

	case head.Match("panic"):
		return ctx.ParsePanic(line)

	case head.Match("assert"):
		return ctx.ParseAssert(line)

	case head.Match("loop"):
		return ctx.ParseLoop(line)

	case head.Match("break"):
		return ctx.ParseBreakContinue(line, NS_Break)

	case head.Match("continue"):
		return ctx.ParseBreakContinue(line, NS_Continue)

	case head.Match("jump"):
		return ctx.ParseJump(line)

	case head.Match("swap"):
		return ctx.ParseSwap(line)

	case head.Match("tail-call"):
		return ctx.ParseTailCall(line)
	}

	// check for an assignment
	if head.Match("=") {
		PostError("missing destination before '='")
		return FAILED
	}
	if line.Len() >= 2 {
		if line.children[1].Match("=") {
			return ctx.ParseAssignment(line)
		}
	}

	// handle a computation on a line by itself....

	comp := ctx.ParseComputation(line.children, "statement")
	if comp == P_FAIL {
		return FAILED
	}

	// values in tail position are handled like a `return` statement,
	// except when current function does not return anything.

	if tail {
		ret_type := fu.ty.sub
		if ! (ret_type.kind == TYP_Void || ret_type.kind == TYP_NoReturn) {
			return ctx.ImplicitReturn(comp)
		}
	}

	// function calls are allowed, even though return value is not used.
	// for all other computations, an unused value is not allowed.

	switch comp.kind {
	case NX_Call, NX_Method:
		ctx.block.Add(comp)
		return OK

	default:
		PostError("unused value/computation")
		return FAILED
	}
}

func (ctx *ParsingCtx) ParseArgument(t *Node) *Node {
	arg := ctx.ParseBasicTerm(t)
	if arg == P_FAIL {
		return P_FAIL
	}

	var ectx EvalContext
	ectx.fu = ctx.fu

	arg = ectx.EvalConstExprs(arg)
	return arg
}

func (ctx *ParsingCtx) ParseComputation(children []*Node, what string) *Node {
	if len(children) == 0 {
		PostError("missing value for %s", what)
		return P_FAIL
	}

	comp := ctx.ParseExpression(children)
	if comp == P_FAIL {
		return P_FAIL
	}

	var ectx EvalContext
	ectx.fu = ctx.fu

	comp = ectx.EvalConstExprs(comp)
	return comp
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseLet(line *Node) error {
	fu := ctx.fu

	if line.Len() < 2 {
		PostError("missing variable name after let")
		return FAILED
	}

	t_var := line.children[1]

	if ValidateName(t_var, "local", ALLOW_SHADOW) != OK {
		return FAILED
	}

	// allow using same name in an *outer* block, but not same block
	if ctx.scope.locals[t_var.str] != nil {
		PostError("local name already used: %s", t_var.str)
		return FAILED
	}

	// inhibit future error messages about the local if the rest
	// of this line fails to parse.
	ctx.scope.seen[t_var.str] = true

	if line.Len() < 3 || ! line.children[2].Match("=") {
		PostError("missing '=' in let statement")
		return FAILED
	}
	if line.Len() < 4 {
		PostError("missing computation in let statement")
		return FAILED
	}

	// the stuff after the `=` is the computation, often a function
	// call or intrinsic, but could just be a literal value.

	source := ctx.ParseComputation(line.children[3:], "let var")
	if source == P_FAIL {
		return FAILED
	}

	// create local -- must be AFTER parsing the value!
	local := fu.NewLocal(t_var.str, nil)

	ctx.scope.AddLocal(local)

	t_let := NewNode(NS_Let, "", line.pos)
	t_let.local = local
	t_let.Add(source)

	ctx.block.Add(t_let)
	return OK
}

func (ctx *ParsingCtx) ParseAssignment(line *Node) error {
	if line.Len() < 3 {
		PostError("missing computation in assignment")
		return FAILED
	}

	// parse the destination, which is either a local var, global var,
	// or an access expression in `[]` brackets.

	t_var := line.children[0]

	if t_var.Match("_") {
		return ctx.ParseIgnore(line.children)
	}
	if t_var.kind == NX_Const {
		PostError("cannot assign to global constant '%s'", t_var.str)
		return FAILED
	}

	dest := ctx.ParseArgument(t_var)

	if dest == P_FAIL {
		return FAILED
	}

	if dest.IsLiteral() {
		PostError("cannot assign to a literal value")
		return FAILED
	}

	if t_var.kind == NG_Data {
		// ok
	} else {
		switch dest.kind {
		case NX_Local, NX_Global, NX_Deref:
			// ok

		default:
			PostError("bad destination in assignment")
			return FAILED
		}
	}

	// the stuff after the `=` is the computation, often a function
	// call or intrinsic, but could just be a literal value.

	source := ctx.ParseComputation(line.children[2:], "assignment")
	if source == P_FAIL {
		return FAILED
	}

	t_assign := NewNode(NS_Assign, "", dest.pos)
	t_assign.Add(dest)
	t_assign.Add(source)

	ctx.block.Add(t_assign)
	return OK
}

func (ctx *ParsingCtx) ParseIgnore(children []*Node) error {
	// the stuff after the `=` is the computation, often a function
	// call or intrinsic, but could just be a literal value.

	comp := ctx.ParseComputation(children, "assignment")
	if comp == P_FAIL {
		return FAILED
	}
	if comp.IsLiteral() {
		// comp is a literal, so nothing needed
		return OK
	}

	t_ignore := NewNode(NS_Ignore, "", children[0].pos)
	t_ignore.Add(comp)

	ctx.block.Add(t_ignore)
	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseLoop(line *Node) error {
	if line.Len() < 2 {
		PostError("missing block for loop statement")
		return FAILED
	}

	t_block := line.PopTail()
	if t_block.kind != NG_Block {
		PostError("loop body must be a block, got: %s", t_block.String())
		return FAILED
	}

	info := ctx.PushLoop()
	info.start_label = ctx.fu.GenLabel("__loop")

	// support a condition after the `loop` keyword
	var t_cond *Node

	if line.Len() > 1 {
		t_cond = ctx.ParseLoopCondition(line.children[1:])
		if t_cond == P_FAIL {
			return FAILED
		}
		if info.break_label == "" {
			info.break_label = ctx.fu.GenLabel("__break")
		}
	}

	body := ctx.ParseBlock("loop", t_block, false)

	ctx.PopLoop()

	if body == P_FAIL {
		return FAILED
	}

	t_loop := NewNode(NS_Loop, "", line.pos)
	t_loop.loopinfo = info
	t_loop.Add(body)

	if t_cond != nil {
		t_loop.Add(t_cond)
	}

	ctx.block.Add(t_loop)
	return OK
}

func (ctx *ParsingCtx) ParseLoopCondition(children []*Node) *Node {
	negate := true

	t_word  := children[0]
	children = children[1:]

	if t_word.Match("while") {
		// ok
	} else if t_word.Match("until") {
		negate = false
	} else {
		PostError("expected while/until, got: %s", t_word.String())
		return P_FAIL
	}

	comp := ctx.ParseComputation(children, "loop")
	if comp == P_FAIL {
		return P_FAIL
	}

	if negate {
		t_neg := NewNode(NX_Unary, "not", comp.pos)
		t_neg.Add(comp)
		return t_neg
	}
	return comp
}

func (ctx *ParsingCtx) ParseBreakContinue(line *Node, kind NodeKind) error {
	// find the most recent loop block
	if len(ctx.loops) == 0 {
		PostError("%s used outside of a loop", line.children[0])
		return FAILED
	}

	info := ctx.loops[len(ctx.loops)-1]

	t_break := NewNode(kind, "", line.pos)

	if kind == NS_Continue {
		t_break.str = info.start_label
	} else {
		// create the exit label, if not already there
		if info.break_label == "" {
			info.break_label = ctx.fu.GenLabel("__break")
		}
		t_break.str = info.break_label
	}

	// support a condition after the `break` or `continue` keyword
	if line.Len() > 1 {
		t_cond := ctx.ParseJumpCondition(line.children[1:])
		if t_cond == P_FAIL {
			return FAILED
		}
		t_break.Add(t_cond)
	}

	ctx.block.Add(t_break)
	return nil
}

func (ctx *ParsingCtx) ParseJump(line *Node) error {
	if line.Len() < 2 {
		PostError("missing label for jump statement")
		return FAILED
	}

	t_label := line.children[1]

	if t_label.kind != NL_Name {
		PostError("expected label name in jump, got: %s", t_label.String())
		return FAILED
	}

	// remember this jump for checking later
	// TODO add a helper method for this  ctx.fu.RememberJump()
	jump_info := new(LabelInfo)
	jump_info.name  = t_label.str
	jump_info.pos   = t_label.pos
	jump_info.scope = ctx.scope

	ctx.fu.jumps = append(ctx.fu.jumps, jump_info)

	t_jump := NewNode(NS_Jump, t_label.str, line.pos)

	if line.Len() > 2 {
		t_cond := ctx.ParseJumpCondition(line.children[2:])
		if t_cond == P_FAIL {
			return FAILED
		}
		t_jump.Add(t_cond)
	}

	ctx.block.Add(t_jump)
	return OK
}

func (ctx *ParsingCtx) ParseJumpCondition(children []*Node) *Node {
	negate := false

	t_word  := children[0]
	children = children[1:]

	if t_word.Match("if") {
		// ok
	} else if t_word.Match("unless") {
		negate = true
	} else {
		PostError("expected if/unless, got: %s", t_word.String())
		return P_FAIL
	}

	comp := ctx.ParseComputation(children, "condition")
	if comp == P_FAIL {
		return P_FAIL
	}

	if negate {
		t_neg := NewNode(NX_Unary, "not", comp.pos)
		t_neg.Add(comp)
		return t_neg
	}
	return comp
}

func (ctx *ParsingCtx) ParseIf(line *Node, tail bool) error {
	t_if := ctx.ParseIf2(line, tail)
	if t_if == P_FAIL {
		return FAILED
	}
	ctx.block.Add(t_if)
	return OK
}

func (ctx *ParsingCtx) ParseIf2(line *Node, tail bool) *Node {
	t_if := NewNode(NS_If, "", line.pos)

	children := line.children[1:]

	// find the `else` clause (possibly an `else if` clause)
	else_p := -1

	for i, sub := range children {
		if sub.Match("else") {
			else_p = i
			break
		}
	}

	var t_else *Node

	if else_p >= 0 {
		if else_p+1 >= len(children) {
			PostError("missing block after else")
			return P_FAIL
		}
		t_next := children[else_p+1]

		if t_next.kind == NG_Block {
			if else_p+2 < len(children) {
				PostError("extra rubbish after else clause")
				return P_FAIL
			}
			t_else = t_next

		} else if t_next.Match("if") {
			t_else = NewNode(NG_Line, "", line.pos)

			for k := else_p+1; k < len(children); k++ {
				t_else.Add(children[k])
			}

		} else {
			PostError("expected block or 'if' after else, got: %s", t_next.String())
			return P_FAIL
		}

		children = children[0:else_p]
	}

	// handle block for `if` clause
	if len(children) < 1 {
		PostError("missing condition in if statement")
		return P_FAIL
	}
	if len(children) < 2 {
		PostError("missing block in if statement")
		return P_FAIL
	}

	t_block := children[  len(children)-1]
	children = children[0:len(children)-1]

	if t_block.kind != NG_Block {
		PostError("expected block in if statement, got: %s", t_block.String())
		return P_FAIL
	}

	// handle the condition
	comp := ctx.ParseComputation(children, "if")
	if comp == P_FAIL {
		return P_FAIL
	}
	t_if.Add(comp)

	// parse the `if` body
	main_body := ctx.ParseBlock("if", t_block, tail)
	if main_body == P_FAIL {
		return P_FAIL
	}
	t_if.Add(main_body)

	// handle the `else` part (recursively)
	if t_else != nil {
		var else_body *Node

		if t_else.kind == NG_Block {
			else_body = ctx.ParseBlock("else", t_else, tail)
		} else {
			else_body = ctx.ParseIf2(t_else, tail)
		}
		if else_body == P_FAIL {
			return P_FAIL
		}

		t_if.Add(else_body)
	}

	return t_if
}

func (ctx *ParsingCtx) ParseMatch(line *Node, tail bool) error {
	if line.Len() < 2 {
		PostError("missing value for match statement")
		return FAILED
	}
	if line.Len() < 3 {
		PostError("missing block for match statement")
		return FAILED
	}

	t_block := line.PopTail()

	if t_block.kind != NG_Block {
		PostError("match body must be a block, got: %s", t_block.String())
		return FAILED
	}

	// grab the value to match
	comp := ctx.ParseComputation(line.children[1:], "match")
	if comp == P_FAIL {
		return FAILED
	}

	t_match := NewNode(NS_Match, "", line.pos)
	t_match.Add(comp)

	// handle each match expression...
	catch_all := false

	for _, rule := range t_block.children {
		Error_SetPos(rule.pos)

		// each line is never empty
		t_block := rule.PopTail()

		if t_block.kind != NG_Block {
			PostError("expected block for match case, got: %s", t_block.String())
			return FAILED
		}

		rule_body := ctx.ParseBlock("match", t_block, tail)
		if rule_body == P_FAIL {
			return FAILED
		}

		if catch_all {
			PostError("extra cases beyond the else clause")
			return FAILED
		}

		head := rule.children[0]
		if !head.Match("case") {
			PostError("expected case in match body, got: %s", head.String())
			return FAILED
		}
		if rule.Len() < 2 {
			PostError("missing values for match case")
			return FAILED
		}

		t_case := NewNode(NS_Case, "", line.pos)
		t_case.Add(rule_body)

		if rule.children[1].Match("_") {
			if rule.Len() > 2 {
				PostError("extra rubbish after catch-all case")
				return FAILED
			}
			catch_all = true

		} else {
			// collect the values to compare with

			for i := 1; i < rule.Len(); i++ {
				t_value := rule.children[i]
				if t_value.Match("_") {
					PostError("cannot use '_' in that context")
					return FAILED
				}

				term := ctx.ParseArgument(t_value)
				if term == P_FAIL {
					return FAILED
				}
				if ! term.IsLiteral() {
					PostError("case values must be constants")
					return FAILED
				}
				t_case.Add(term)
			}
		}

		t_match.Add(t_case)
	}

	ctx.block.Add(t_match)
	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseReturn(line *Node) error {
	t_return := NewNode(NS_Return, "", line.pos)
	children := line.children[1:]

	// check for a conditional, directly after the `return` keyword
	// or after a single-token value.
	have_cond := false

	if len(children) > 0 {
		child := children[0]
		if child.Match("if") || child.Match("unless") {
			have_cond = true
		}
	}
	if len(children) > 1 {
		child := children[1]
		if child.Match("if") || child.Match("unless") {
			t_value := ctx.ParseArgument(children[0])
			if t_value == P_FAIL {
				return FAILED
			}
			t_return.Add(t_value)

			have_cond = true
			children = children[1:]
		}
	}

	if have_cond {
		t_cond := ctx.ParseJumpCondition(children)
		if t_cond == P_FAIL {
			return FAILED
		}
		t_if := NewNode(NS_If, "", line.pos)
		t_if.Add(t_cond)
		t_if.Add(t_return)

		ctx.block.Add(t_if)
		return OK
	}

	if len(children) == 0 {
		// plain `return` with no value
	} else {
		comp := ctx.ParseComputation(children, "return")
		if comp == P_FAIL {
			return FAILED
		}
		t_return.Add(comp)
	}

	ctx.block.Add(t_return)
	return OK
}

func (ctx *ParsingCtx) ImplicitReturn(comp *Node) error {
	t_return := NewNode(NS_Implicit, "", comp.pos)
	t_return.Add(comp)

	ctx.block.Add(t_return)
	return OK
}

func (ctx *ParsingCtx) ParsePanic(line *Node) error {
	if line.Len() < 2 {
		PostError("missing message for panic statement")
		return FAILED
	}
	if line.Len() > 2 {
		PostError("extra rubbish at end of panic statement")
		return FAILED
	}

	t_msg := line.children[1]

	if t_msg.kind != NL_String {
		PostError("expected string after panic, got: %s", t_msg.String())
		return FAILED
	}

	t_panic := NewNode(NS_Panic, t_msg.str, line.pos)

	ctx.block.Add(t_panic)
	return OK
}

func (ctx *ParsingCtx) ParseAssert(line *Node) error {
	if line.Len() < 2 {
		PostError("missing condition for assert statement")
		return FAILED
	}

	comp := ctx.ParseComputation(line.children[1:], "assert")
	if comp == P_FAIL {
		return FAILED
	}

	t_assert := NewNode(NS_Assert, "", line.pos)
	t_assert.Add(comp)

	ctx.block.Add(t_assert)
	return OK
}

func (ctx *ParsingCtx) ParseDo(line *Node, tail bool) error {
	if line.Len() < 2 {
		PostError("missing block after do")
		return FAILED
	}
	if line.Len() > 3 {
		PostError("too many elements for do block")
		return FAILED
	}

	if line.Len() >= 3 {
		t_label := line.children[1]

		if ValidateName(t_label, "label", ALLOW_SHADOW) != OK {
			return FAILED
		}

		label_name := t_label.str

		// check that labels are unique
		if ctx.scope.HasLabel(label_name) {
			PostError("duplicate label: %s", label_name)
			return FAILED
		}

		// add the label  [ TODO move into a helper func ]
		info := new(LabelInfo)
		info.name  = label_name
		info.pos   = t_label.pos
		info.scope = ctx.scope

		ctx.fu.labels[label_name] = info
		ctx.scope.labels[label_name] = true

		// create a label node
		t_label = NewNode(NH_Label, label_name, line.pos)

		ctx.block.Add(t_label)
	}

	t_block := line.children[line.Len()-1]

	if t_block.kind != NG_Block {
		PostError("expected block after do, got: %s", t_block.String())
		return FAILED
	}

	t_do := ctx.ParseBlock("do", t_block, tail)
	if t_do == P_FAIL {
		return FAILED
	}

	ctx.block.Add(t_do)
	return OK
}

func (ctx *ParsingCtx) ParseSwap(line *Node) error {
	if line.Len() < 3 {
		PostError("missing targets for swap statement")
		return FAILED
	}
	if line.Len() > 3 {
		PostError("extra rubbish at end of swap statement")
		return FAILED
	}

	left  := ctx.ParseArgument(line.children[1])
	right := ctx.ParseArgument(line.children[2])

	if left == P_FAIL || right == P_FAIL {
		return FAILED
	}

	left_ok  := false
	right_ok := false

	switch left.kind {
	case NX_Local, NX_Global, NX_Deref, NX_Field, NX_Index:
		left_ok = true
	}
	switch right.kind {
	case NX_Local, NX_Global, NX_Deref, NX_Field, NX_Index:
		right_ok = true
	}

	if !left_ok || !right_ok {
		PostError("targets in a swap statement must be assignable")
		return FAILED
	}

	if left.kind == NX_Local && right.kind == NX_Local {
		if left.local == right.local {
			PostError("cannot swap a local with itself")
			return FAILED
		}
	}
	if left.kind == NX_Global && right.kind == NX_Global {
		if left.def == right.def {
			PostError("cannot swap a global var with itself")
			return FAILED
		}
	}

	t_swap := NewNode(NS_Swap, "", line.pos)
	t_swap.Add(left)
	t_swap.Add(right)

	ctx.block.Add(t_swap)
	return OK
}

func (ctx *ParsingCtx) ParseTailCall(line *Node) error {
	if line.Len() < 2 {
		PostError("missing function for tail-call statement")
		return FAILED
	}

	t_call := ctx.Term_ExplicitCall(line.children[1:])
	if t_call == P_FAIL {
		return FAILED
	}

	t_tailcall := NewNode(NS_TailCall, "", line.pos)
	t_tailcall.Add(t_call)

	ctx.block.Add(t_tailcall)
	return OK
}
