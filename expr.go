// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

func (ctx *ParsingCtx) ParseExpression(children []*Node) *Node {
	// handle "terms" which consist of multiple tokens.
	// terms are separated by *binary* operators (and will never
	// begin with a binary operator).  a function name which is just
	// an identifier (no parameters) must be considered a term too.

	// this will become: TERM { bin-op TERM }.
	expr := make([]*Node, 0)
	num_ops := 0

	for len(children) > 0 {
		// find the next binary operator.
		// [ first element should never be one ]
		pos := 1

		for pos < len(children) {
			if IsBinaryOperator(children[pos]) {
				break
			}
			pos += 1
		}

		term := ctx.MultiTokenTerm(children[0:pos])
		children = children[pos:]

		if term == P_FAIL || term == P_UNKNOWN {
			return term
		}

		expr = append(expr, term)

		// do we have an operator?
		if len(children) > 0 {
			expr = append(expr, children[0])
			children = children[1:]
			num_ops += 1
		}
	}

	if num_ops == 0 {
		return expr[0]
	}

	node := ctx.ShuntingYard(expr)
	return node
}

func (ctx *ParsingCtx) ShuntingYard(children []*Node) *Node {
	/* this is Dijkstra's shunting-yard algorithm */

	op_stack   := make([]*Operator, 0)
	term_stack := make([]*Node, 0)

	shunt := func() error {
		// the op_stack is never empty when this is called
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0 : len(op_stack)-1]

		// this should never happen
		if len(term_stack) < 2 {
			panic("FAILURE AT THE SHUNTING YARD")
		}

		L := term_stack[len(term_stack)-2]
		R := term_stack[len(term_stack)-1]

		term := NewNode(NX_Binary, op.name, L.pos)
		term.Add(L)
		term.Add(R)

		term_stack = term_stack[0 : len(term_stack)-2]
		term_stack = append(term_stack, term)
		return OK
	}

	seen_term := false

	for _, t := range children {
		op := operators[t.str]

		if t.kind == NL_Name && op != nil {
			if len(op.binary) == 0 {
				panic("OOPSIE IN MATH PARSE")
			}

			// shunt existing operators if they have greater precedence
			for len(op_stack) > 0 {
				top := op_stack[len(op_stack)-1]

				right_assoc := (op.name == "**")

				if top.prec > op.prec ||
					(top.prec == op.prec && !right_assoc) {

					if shunt() != OK {
						return P_FAIL
					}
					continue
				}
				break
			}

			op_stack  = append(op_stack, op)
			seen_term = false

		} else {
			// if not an operator, it must be a term
			term_stack = append(term_stack, t)
			seen_term  = true
		}
	}

	if !seen_term {
		PostError("bad expression, missing term after last operator")
		return P_FAIL
	}

	// handle the remaining operators on stack
	for len(op_stack) > 0 {
		if shunt() != OK {
			return P_FAIL
		}
	}

	if len(term_stack) != 1 {
		panic("PARSE MATH FAIlURE")
	}

	// replace input token with root of the expression tree
	return term_stack[0]
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) MultiTokenTerm(children []*Node) *Node {
	// this handles a "conv-expr" of the formal grammar (apart from
	// the unary operator check below).

	head := children[0]

	if IsOperator(head) {
		return ctx.Term_UnaryOp(children)
	}

	// a pointer type name?
	if head.Match("^") {
		ty, count := ParseTypeSpec(children, "a literal value")
		if ty == nil {
			return P_FAIL
		}
		children = children[count:]
		return ctx.Term_Conv(ty, children)
	}

	if head.kind != NL_Name {
		return ctx.ParseMethodExpr(children)
	}

	if head.Match("cast") {
		return ctx.Term_RawCast(children[1:])
	}

	// a local can never be a type name
	if ctx.fu != nil && head.module == "" {
		if ctx.scope.LookupLocal(head.str) != nil {
			return ctx.ParseMethodExpr(children)
		}
	}

	if !ValidateModUsage(head.module) {
		return P_FAIL
	}

	// module usage by aliases is checked at load time
	ReplaceAlias(head)

	// a type name?
	def, mod := LookupDef(head.str, head.module)

	if def != nil && mod != cur_mod && !def.IsPublic() {
		PostError("%s is private to module %s", head.str, mod.name)
		return P_FAIL
	}

	var ty *Type

	if def != nil && def.kind == DEF_Type {
		ty = def.d_type.ty
	} else {
		ty = LookupBuiltinType(head.str)
	}

	if ty != nil {
		children = children[1:]
		return ctx.Term_Conv(ty, children)
	}

	return ctx.ParseMethodExpr(children)
}

func (ctx *ParsingCtx) ParseMethodExpr(children []*Node) *Node {
	// find the *last* token which is a method name.
	// everything before it will be the receiver (which may contain
	// additional method calls).
	method_pos := -1

	for i, child := range children {
		if child.IsMethod() {
			method_pos = i
		}
	}

	if method_pos == 0 {
		PostError("missing receiver for %s method", children[method_pos].str)
		return P_FAIL
	}

	if method_pos > 0 {
		recv := ctx.ParseMethodExpr(children[0:method_pos])
		if recv == P_FAIL {
			return P_FAIL
		}
		return ctx.Term_MethodCall(recv, children[method_pos:])
	}

	return ctx.ParseTerm(children)
}

func (ctx *ParsingCtx) ParseTerm(children []*Node) *Node {
	head := children[0]

	if head.kind != NL_Name {
		if len(children) > 1 {
			PostError("bad syntax in expression")
			return P_FAIL
		}
		return ctx.ParseBasicTerm(head)
	}

	// locals always shadow a global
	if ctx.fu != nil && head.module == "" {
		if ctx.scope.LookupLocal(head.str) != nil {
			if len(children) > 1 {
				PostError("extra rubbish after local: %s", head.str)
			}
			return ctx.Term_Name(head)
		}
	}

	if !ValidateModUsage(head.module) {
		return P_FAIL
	}
	ReplaceAlias(head)

	// a function name?
	def, mod := LookupDef(head.str, head.module)

	if def != nil && mod != cur_mod && !def.IsPublic() {
		PostError("%s is private to module %s", head.str, mod.name)
		return P_FAIL
	}

	if def != nil && def.kind == DEF_Func {
		head = ctx.Term_GlobalFunc(head, def)
		return ctx.Term_FunCall(head, children[1:])
	}

	// these forms are allowed in constant exprs
	if head.Match("max") {
		return ctx.Term_MinMax(children[1:], NX_Max)
	}
	if head.Match("min") {
		return ctx.Term_MinMax(children[1:], NX_Min)
	}

	// certain keywords are only usable inside a function
	if ctx.fu != nil {
		args := children[1:]

		switch {
		case head.Match("call"):
			return ctx.Term_ExplicitCall(args)

		case head.Match("matches?"):
			return ctx.Term_Matches(args)

		case head.Match("stack-var"):
			return ctx.Term_StackVar(args)
		}
	}

	// nothing else allows arguments
	if len(children) > 1 {
		PostError("unknown function name: %s", head.str)
		return P_FAIL
	}

	return ctx.Term_Name(head)
}

func (ctx *ParsingCtx) ParseBasicTerm(t *Node) *Node {
	Error_SetPos(t.pos)

	if t.IsLiteral() {
		if t.kind == NL_Null && ctx.fu == nil {
			PostError("cannot use NULL in const exprs")
			return P_FAIL
		}
		return t
	}

	switch t.kind {
	case NL_Name:
		return ctx.Term_Name(t)

	case NG_Expr:
		if len(t.children) == 0 {
			PostError("missing computation in ()")
			return P_FAIL
		}
		expr := ctx.ParseExpression(t.children)
		return expr

	case NG_Block:
		PostError("block used in value context")
		return P_FAIL

	case NG_Data:
		return ctx.Term_Access(t)
	}

	if ctx.fu == nil {
		PostError("bad value in const expression, got: %s", t.String())
		return P_FAIL
	}

	PostError("expected a term, got: %s", t.String())
	return P_FAIL
}

func (ctx *ParsingCtx) Term_Name(t *Node) *Node {
	if IsOperator(t) {
		PostError("cannot use operator '%s' in that context", t.str)
		return P_FAIL
	}

	if ctx.fu != nil && t.module == "" {
		// access to a class member?
		if t.IsField() {
			return ctx.Term_AccessMember(t)
		}

		local := ctx.scope.LookupLocal(t.str)

		if local != nil {
			op := NewNode(NX_Local, t.str, t.pos)
			op.local = local
			op.ty = local.ty
			return op
		}
	}

	if !ValidateModUsage(t.module) {
		return P_FAIL
	}
	ReplaceAlias(t)

	def, mod := LookupDef(t.str, t.module)

	if def != nil && mod != cur_mod {
		if !def.IsPublic() {
			PostError("%s is private to module %s", t.str, mod.name)
			return P_FAIL
		}
	}

	if def != nil {
		switch def.kind {
		case DEF_Const:
			return ctx.Term_GlobalConst(t, def)

		case DEF_Var:
			return ctx.Term_GlobalVar(t, def)

		case DEF_Func:
			return ctx.Term_GlobalFunc(t, def)

		case DEF_Type:
			PostError("cannot use type '%s' in that context", t.str)
			return P_FAIL
		}
	}

	if ctx.fu == nil {
		PostError("no such constant: %s", t.str)
		return P_FAIL
	}

	if ctx.fu.error_num > 0 && ctx.scope.SeenLocal(t.str) {
		// don't show errors for a local that was known to be defined
	} else {
		PostError("unknown identifier: %s", t.str)

		// inhibit future errors about this identifier
		ctx.scope.seen[t.str] = true
	}

	return P_FAIL
}

func (ctx *ParsingCtx) Term_GlobalConst(t *Node, def *Definition) *Node {
	cdef := def.d_const

	if cdef.value == nil {
		if ctx.fu != nil {
			panic("uneval constant in function context")
		}
		return P_UNKNOWN
	}

	t_const := NewNode(NX_Const, cdef.name, t.pos)
	t_const.def = def
	return t_const
}

func (ctx *ParsingCtx) Term_GlobalVar(t *Node, def *Definition) *Node {
	vdef := def.d_var

	t_var := NewNode(NX_Global, vdef.name, t.pos)
	t_var.def = def
	return t_var
}

func (ctx *ParsingCtx) Term_GlobalFunc(t *Node, def *Definition) *Node {
	fdef := def.d_func

	t_func := NewNode(NX_Func, fdef.name, t.pos)
	t_func.def = def
	return t_func
}

func (ctx *ParsingCtx) Term_UnaryOp(children []*Node) *Node {
	head := children[0]
	op   := operators[head.str]

	if len(op.unary) == 0 {
		PostError("bad expression: unexpected %s operator", head.str)
		return P_FAIL
	}
	if len(children) < 2 {
		PostError("bad expression: missing term after %s operator", head.str)
		return P_FAIL
	}

	term := ctx.MultiTokenTerm(children[1:])
	if term == P_FAIL || term == P_UNKNOWN {
		return term
	}

	expr := NewNode(NX_Unary, op.name, head.pos)
	expr.Add(term)
	return expr
}

func (ctx *ParsingCtx) Term_Access(t *Node) *Node {
	if ctx.fu == nil {
		PostError("cannot use [] in const exprs")
		return P_FAIL
	}

	children := t.children
	addr_of  := false

	if len(children) > 0 && children[0].Match("ref") {
		addr_of = true
		children = children[1:]
	}

	if len(children) == 0 {
		PostError("missing elements in []")
		return P_FAIL
	}

	base := ctx.ParseBasicTerm(children[0])
	children = children[1:]

	if base == P_FAIL {
		return P_FAIL
	}
	if base.IsLiteral() {
		PostError("cannot access a literal value")
		return P_FAIL
	}

	// handle `[ref VARNAME]` and `[ref FUNCNAME]` syntax
	if addr_of && len(children) == 0 {
		switch base.kind {
		case NX_Global, NX_Func:
			t_addr := NewNode(NX_AddrOf, "", t.pos)
			t_addr.Add(base)
			return t_addr
		}
	}

	t_access := ctx.Term_Access2(base, children)
	if t_access == P_FAIL {
		return P_FAIL
	}

	if t_access.kind == NX_Deref {
		if addr_of {
			PostError("invalid use of ref in []")
			return P_FAIL
		}
		return t_access
	}

	if addr_of {
		t_addr := NewNode(NX_AddrOf, "", t.pos)
		t_addr.Add(t_access)
		return t_addr
	}

	// the NX_Field and NX_Index nodes represent pointer arithmetic,
	// but we still need to actually read/write the memory.
	t_deref := NewNode(NX_Deref, "", t.pos)
	t_deref.Add(t_access)
	return t_deref
}

func (ctx *ParsingCtx) Term_Access2(base *Node, children []*Node) *Node {
	// add a null pointer check
	t_check := NewNode(NX_ChkNull, "", base.pos)
	t_check.Add(base)

	// handle the case of no indexors -- deref a pointer
	if len(children) == 0 {
		t_deref := NewNode(NX_Deref, "", base.pos)
		t_deref.Add(t_check)
		return t_deref
	}

	base = t_check

	for _, t_index := range children {
		var t_mem *Node

		if t_index.IsField() {
			// struct or union access
			t_mem = NewNode(NX_Field, t_index.str, t_index.pos)
			t_mem.Add(base)

		} else {
			// array access
			indexor := ctx.ParseBasicTerm(t_index)
			if indexor == P_FAIL {
				return P_FAIL
			}

			t_mem = NewNode(NX_Index, "", t_index.pos)
			t_mem.Add(base)
			t_mem.Add(indexor)
		}

		base = t_mem
	}

	return base
}

func (ctx *ParsingCtx) Term_AccessMember(t *Node) *Node {
	if ctx.fu == nil {
		PostError("cannot access field in a const expression")
		return P_FAIL
	}
	if !ctx.fu.method {
		PostError("cannot access field '%s' in non-method", t.str)
		return P_FAIL
	}

	self := ctx.fu.params[0]

	t_local := NewNode(NX_Local, self.name, t.pos)
	t_local.local = self

	t_field := NewNode(NX_Field, t.str, t.pos)
	t_field.Add(t_local)

	t_deref := NewNode(NX_Deref, "", t.pos)
	t_deref.Add(t_field)

	return t_deref
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) Term_ExplicitCall(children []*Node) *Node {
	if len(children) < 1 {
		PostError("missing function for call statement")
		return P_FAIL
	}

	t_func := ctx.ParseBasicTerm(children[0])
	if t_func == P_FAIL {
		return P_FAIL
	}

	return ctx.Term_FunCall(t_func, children[1:])
}

func (ctx *ParsingCtx) Term_FunCall(t_func *Node, params []*Node) *Node {
	if ctx.fu == nil {
		PostError("cannot call functions in const exprs")
		return P_FAIL
	}

	// add a null pointer check for function
	t_check := NewNode(NX_ChkNull, "", t_func.pos)
	t_check.Add(t_func)

	t_call := NewNode(NX_Call, "", t_func.pos)
	t_call.Add(t_check)

	if ctx.GrabParameters(t_call, params) != OK {
		return P_FAIL
	}
	return t_call
}

func (ctx *ParsingCtx) Term_MethodCall(recv *Node, children []*Node) *Node {
	if ctx.fu == nil {
		PostError("cannot call methods in const exprs")
		return P_FAIL
	}

	t_method := children[0]
	params   := children[1:]

	// add a null pointer check on receiver
	t_check := NewNode(NX_ChkNull, "", t_method.pos)
	t_check.Add(recv)

	t_call := NewNode(NX_Method, "", t_method.pos)
	t_call.Add(t_method)
	t_call.Add(t_check)

	if ctx.GrabParameters(t_call, params) != OK {
		return P_FAIL
	}

	return t_call
}

func (ctx *ParsingCtx) GrabParameters(t_call *Node, params []*Node) error {
	for _, t := range params {
		t_par := ctx.ParseBasicTerm(t)
		if t_par == P_FAIL {
			return FAILED
		}
		t_call.Add(t_par)
	}

	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) Term_Conv(dest_type *Type, children []*Node) *Node {
	if ctx.fu == nil {
		PostError("cannot use types in const exprs")
		return P_FAIL
	}
	if len(children) == 0 {
		PostError("bad conversion: missing term after type")
		return P_FAIL
	}

	L := ctx.MultiTokenTerm(children)
	if L == P_FAIL {
		return P_FAIL
	}

	t_conv := NewNode(NX_Conv, "", L.pos)
	t_conv.ty = dest_type
	t_conv.Add(L)

	return t_conv
}

func (ctx *ParsingCtx) Term_RawCast(children []*Node) *Node {
	// source and target type must be one of: int, float, bool, ptr.
	// source and target must be different types.
	// source and target must be same size, no exceptions.

	if len(children) == 0 {
		PostError("missing type in cast statement")
		return P_FAIL
	}

	dest_type, count := ParseTypeSpec(children, "a cast")
	if dest_type == nil {
		return P_FAIL
	}
	children = children[count:]

	if len(children) == 0 {
		PostError("missing value in cast statement")
		return P_FAIL
	}

	L := ctx.MultiTokenTerm(children)
	if L == P_FAIL {
		return P_FAIL
	}

	// check target type makes sense
	switch dest_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok
	default:
		PostError("target type of cast cannot be a %s", dest_type.SimpleName())
		return P_FAIL
	}

	t_cast := NewNode(NX_RawCast, "", L.pos)
	t_cast.ty = dest_type
	t_cast.Add(L)

	return t_cast
}

func (ctx *ParsingCtx) Term_Matches(children []*Node) *Node {
	if len(children) < 2 {
		PostError("missing values in matches? statement")
		return P_FAIL
	}

	t_matches := NewNode(NX_Matches, "", children[0].pos)

	for _, t := range children {
		t_val := ctx.ParseBasicTerm(t)
		if t_val == P_FAIL {
			return P_FAIL
		}
		t_matches.Add(t_val)
	}

	return t_matches
}

func (ctx *ParsingCtx) Term_MinMax(children []*Node, kind NodeKind) *Node {
	if len(children) < 1 {
		PostError("missing values in min/max statement")
		return P_FAIL
	}

	t_minmax := NewNode(kind, "", children[0].pos)

	for _, t := range children {
		t_val := ctx.ParseBasicTerm(t)
		if t_val == P_FAIL {
			return P_FAIL
		}
		t_minmax.Add(t_val)
	}

	return t_minmax
}

func (ctx *ParsingCtx) Term_StackVar(children []*Node) *Node {
	if len(children) < 1 {
		PostError("missing type in stack-var statement")
		return P_FAIL
	}

	ty, count := ParseTypeSpec(children, "a stack var")
	if ty == nil {
		return P_FAIL
	}
	if ty.size == 0 {
		PostError("stack-var cannot be zero-sized")
		return P_FAIL
	}

	if len(children) > count {
		PostError("extra rubbish at end of stack-var statement")
		return P_FAIL
	}

	t_stackvar := NewNode(NX_StackVar, "", children[0].pos)
	t_stackvar.ty = ty

	return t_stackvar
}
