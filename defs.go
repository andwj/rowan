// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strings"

type Definition struct {
	kind    DefKind

	d_alias *AliasDef
	d_const *ConstDef
	d_type  *TypeDef
	d_var   *VarDef
	d_func  *FuncDef
}

type DefKind int
const (
	DEF_Alias DefKind = iota
	DEF_Const
	DEF_Type
	DEF_Var
	DEF_Func
	DEF_Method
)

type AliasDef struct {
	name   string
	target string
	module string
}

type ConstDef struct {
	name   string
	module string
	public bool
	expr   *Node
	value  *Node
}

type TypeDef struct {
	name   string
	module string
	extern bool
	public bool
	alias  bool
	kind   string
	body   *Node
	ty     *Type
}

type VarDef struct {
	name     string
	module   string
	extern   bool
	zeroed   bool
	rom      bool
	public   bool

	expr     *Node
	raw_type *Node
	ty       *Type
}

type FuncDef struct {
	name     string
	module   string
	extern   bool
	inline   bool
	method   bool
	public   bool

	raw_pars *Node
	body     *Node
	ty       *Type

	// parsing info....
	labels      map[string]*LabelInfo
	jumps       []*LabelInfo
	gen_labels  int
	error_num   int

	locals      []*LocalVar
	params      []*LocalVar
	num_temps   int
	num_inlines int

	// optimisation info....
	missing_ret bool
}

type LocalVar struct {
	name string
	ty   *Type

	// this is >= 0 for parameters, -1 for locals
	param_idx int

	// this local is read-only
	immutable bool

	// this local is the receiver of a method
	self bool
}

type LabelInfo struct {
	name   string
	scope *Scope
	pos    Position
}

type Scope struct {
	kind      string
	parent    *Scope

	labels    map[string]bool
	locals    map[string]*LocalVar
	seen      map[string]bool
}

func NewScope(kind string) *Scope {
	sc := new(Scope)
	sc.kind = kind
	sc.labels = make(map[string]bool)
	sc.locals = make(map[string]*LocalVar)
	sc.seen   = make(map[string]bool)
	return sc
}

func (def *Definition) Name() string {
	switch def.kind {
	case DEF_Alias:  return def.d_alias.name
	case DEF_Const:  return def.d_const.name
	case DEF_Type:   return def.d_type.name
	case DEF_Func:   return def.d_func.name
	case DEF_Method: return def.d_func.name
	case DEF_Var:    return def.d_var.name
	default:         return ""
	}
}

func (def *Definition) IsPublic() bool {
	switch def.kind {
	case DEF_Alias:  return false
	case DEF_Const:  return def.d_const.public
	case DEF_Type:   return def.d_type.public
	case DEF_Func:   return def.d_func.public
	case DEF_Method: return def.d_func.public
	case DEF_Var:    return def.d_var.public
	default:         return false
	}
}

func ReplaceAlias (t *Node) {
	if t.kind == NL_Name {
		if cur_mod != nil {
			def := cur_mod.defs[t.str]
			if def != nil && def.kind == DEF_Alias {
				t.str    = def.d_alias.target
				t.module = def.d_alias.module
			}
		}
	}
}

func (sc *Scope) AddLocal(local *LocalVar) {
	sc.locals[local.name] = local
}

func (sc *Scope) LookupLocal(name string) *LocalVar {
	for sc != nil {
		lvar := sc.locals[name]
		if lvar != nil {
			return lvar
		}
		sc = sc.parent
	}
	return nil
}

func (sc *Scope) SeenLocal(name string) bool {
	for sc != nil {
		if sc.seen[name] {
			return true
		}
		sc = sc.parent
	}
	return false
}

func (sc *Scope) HasLabel(name string) bool {
	for sc != nil {
		if sc.labels[name] {
			return true
		}
		sc = sc.parent
	}
	return false
}

//----------------------------------------------------------------------

const (
	// allow the name to shadow a global definition
	ALLOW_SHADOW = (1 << iota)
	ALLOW_UNDERSCORE
	ALLOW_MODULE
)

func ValidateName(t_name *Node, what string, flags int) error {
	if t_name.kind != NL_Name {
		PostError("bad %s name: not an identifier", what)
		return FAILED
	}

	if t_name.module != "" {
		if (flags & ALLOW_MODULE) == 0 {
			PostError("bad %s name: cannot contain '::'", what)
			return FAILED
		}
	}

	name := t_name.str

	if name == "_" && (flags & ALLOW_UNDERSCORE) != 0 {
		return OK
	}

	// disallow field names
	if len(name) >= 1 && name[0] == '.' {
		PostError("bad %s name: cannot begin with a dot", what)
		return FAILED
	}

	// disallow attributes
	if len(name) >= 1 && name[0] == '@' {
		PostError("bad %s name: cannot begin with a '@'", what)
		return FAILED
	}

	// disallow special keywords
	if len(name) >= 1 && name[0] == '#' {
		PostError("bad %s name: cannot begin with a '#'", what)
		return FAILED
	}

	// already defined?
	if (flags & ALLOW_SHADOW) == 0 {
		if cur_mod != nil {
			if cur_mod.defs[name] != nil {
				PostError("bad %s name: '%s' already defined", what, name)
				return FAILED
			}
		}
	}

	// disallow language keywords
	if IsLanguageKeyword(name) {
		PostError("bad %s name: '%s' is a reserved keyword", what, name)
		return FAILED
	}

	// disallow operators
	if IsOperatorName(name) {
		PostError("bad %s name: '%s' is a math operator", what, name)
		return FAILED
	}

	// type names cannot contain a dot
	if what == "type" {
		for _, ch := range name {
			if ch == '.' {
				PostError("bad %s name: cannot contain a dot", what)
				return FAILED
			}
		}
	}

	// disallow names with certain suffixes
	if len(name) >= 6 && strings.HasSuffix(name, ".size") {
		PostError("bad %s name: '.size' is a reserved suffix", what)
		return FAILED
	}

	return OK
}

func IsLanguageKeyword(name string) bool {
	switch name {
	case
		// directives
		"#public", "#private",
		"alias", "const", "type", "extern-type",
		"var", "extern-var", "zero-var", "rom-var",
		"fun", "extern-fun", "inline-fun",

		// statements (etc)
		"if", "else", "unless", "do", "let",
		"loop", "while", "until", "break", "continue",
		"jump", "return", "call", "tail-call",
		"swap", "match", "case", "matches?",
		"panic", "assert", "min", "max",
		"cast", "ref", "stack-var",

		// types
		"u8", "u16", "u32", "u64",
		"s8", "s16", "s32", "s64",
		"^", "bool", "void", "no-return", "raw-mem",
		"array", "struct", "union", "raw-struct", "raw-union",

		// constants
		"NULL", "FALSE", "TRUE",
		"+INF", "-INF", "SNAN", "QNAN",
		"CPU_WORD_SIZE", "CPU_ENDIAN",

		// symbols
		"_", "\\", "->", "=>", ".", "..", "...":

		return true
	}

	return false
}

//----------------------------------------------------------------------

func CompileAllConstants() {
	// visit all constant definitions which need to be evaluated.
	// we process them repeatedly until the count shrinks to zero.
	// if it fails to shrink, it means there is a cyclic dependency.

	last_count := -1  // first time through

	for {
		count     := 0
		got_error := false
		example   := ""

		for _, mod := range ordered_mods {
			cur_mod = mod

			for _, def := range mod.ord_defs {
				if def.kind == DEF_Const {
					con := def.d_const

					if con.value == nil {
						count += 1

						if con.TryEvaluate() != OK {
							got_error = true
						}
						if con.value == nil {
							example = con.name
						}
					}
				}
			}
		}

		if count == 0 || got_error {
			return
		}
		if count == last_count {
			PostError("cyclic dependencies with constants (such as %s)", example)
			return
		}

		last_count = count
	}
}

func (con *ConstDef) TryEvaluate() error {
	Error_SetPos(con.expr.pos)

	var pctx ParsingCtx
	expr := pctx.ParseBasicTerm(con.expr)

	if expr == P_FAIL {
		return FAILED
	}
	if expr == P_UNKNOWN {
		// try again next round
		return OK
	}

	var ectx EvalContext
	t_value := ectx.EvalConstExprs(expr)

	if t_value == P_FAIL {
		return FAILED
	}
	if t_value.kind == NL_FltSpec {
		PostError("bad const def: expected value, got: %s", t_value.String())
		return FAILED
	}

	con.value = t_value
	return OK
}

func (con *ConstDef) MakeLiteral(pos Position) *Node {
	t_lit := NewNode(con.value.kind, con.value.str, pos)

	if con.value.kind == NL_Bool {
		t_lit.ty = bool_type
	}
	return t_lit
}
