// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/*
   this code contains optimisations which do not depend on the
   target architecture, plus some helper stuff for the back-end
   (architecture-specific) code.
*/

package main

// import "fmt"
import "strconv"

// loop multipliers for LocalVar.reads and .writes
var outer_factor = 10
var inner_factor = 40

func (fu *FuncDef) MissingLabels() error {
	problems := 0

	// check that jumps go into their own block, or a parent of it
	for _, jump_info := range fu.jumps {
		scope := jump_info.scope
		name  := jump_info.name

		if ! scope.HasLabel(name) {
			Error_SetPos(jump_info.pos)

			if fu.labels[name] == nil {
				PostError("missing label: %s", name)
			} else {
				PostError("jump into foreign block")
			}
			problems += 1
		}
	}

	if problems > 0 {
		return FAILED
	}
	return OK
}

func (fu *FuncDef) UnusedLabels() {
	// remove any labels which are unused
	used_labels := make(map[string]bool)

	for _, t := range fu.body.children {
		if t.kind == NH_Jump {
			used_labels[t.str] = true
		}
	}

	old_body := fu.body
	fu.body = NewNode(NG_Block, "", old_body.pos)

	for _, t := range old_body.children {
		if t.kind == NH_Label && ! used_labels[t.str] {
			delete(fu.labels, t.str)
			continue
		}
		fu.body.Add(t)
	}
}

//----------------------------------------------------------------------

type InlineAnalysis struct {
	input   []*FuncDef
	output  []*FuncDef
	seen    map[*FuncDef]bool
	example *FuncDef
}

type InlineResult int
const (
	INRES_OK       InlineResult = 0  // finished, all okay
	INRES_Progress InlineResult = 1  // progress was made
	INRES_Failed   InlineResult = 2  // cycle detected
)

func InlineAllFunctions() {
	lan := new(InlineAnalysis)

	lan.input  = make([]*FuncDef, 0)
	lan.output = make([]*FuncDef, 0)
	lan.seen   = make(map[*FuncDef]bool)

	// collect all function defs
	for _, mod := range ordered_mods {
		for _, def := range mod.ord_defs {
			if def.kind == DEF_Func || def.kind == DEF_Method {
				lan.input = append(lan.input, def.d_func)
			}
		}
	}

	// determine an ordering so that any inlined function is handled
	// before the function which inlines it.  also detect situations
	// which are impossible to satisfy, for example: A inlines B and
	// B inlines A.

	for {
		res := lan.DoPass()
		if res == INRES_OK {
			break
		} else if res == INRES_Failed {
			Error_SetPos(lan.example.body.pos)
			PostError("cyclic inlining of functions (such as %s)", lan.example.name)
			return
		}
	}

	/* we now have a usable ordering */

	for _, fu := range lan.output {
		fu.PerformInlining()
	}
	for _, fu := range lan.input {
		if !fu.inline && !fu.extern {
			fu.PerformInlining()
		}
	}
}

func (lan *InlineAnalysis) DoPass() InlineResult {
	count    := 0
	progress := false

	for _, fu := range lan.input {
		if fu.inline && !lan.seen[fu] {
			count += 1
			lan.example = fu

			if lan.Visit(fu) {
				lan.output = append(lan.output, fu)
				lan.seen[fu] = true
				progress = true
			}
		}
	}

	if count == 0 {
		return INRES_OK
	} else if progress {
		return INRES_Progress
	} else {
		return INRES_Failed
	}
}

func (lan *InlineAnalysis) Visit(fu *FuncDef) bool {
	for _, t := range fu.body.children {
		comp := GetComputation(t)

		if comp != nil && comp.kind == NC_Call {
			called_func := GetCalledFunc(comp)

			if called_func == nil {
				continue
			}
			if !called_func.inline {
				continue
			}
			if lan.seen[called_func] {
				continue
			}

			// this inline func is not yet "leafy"
			return false
		}
	}

	// this inline func is "leafy"
	return true
}

func (fu *FuncDef) PerformInlining() {
	old_body := fu.body
	fu.body = NewNode(NG_Block, "", old_body.pos)

	for _, t := range old_body.children {
		comp := GetComputation(t)

		if comp != nil && comp.kind == NC_Call {
			called_func := GetCalledFunc(comp)

			if called_func != nil && called_func.inline {
				t_new := fu.PerformInlineCall(t, called_func)
				if t_new != nil {
					fu.body.Add(t_new)
				}
				fu.num_inlines += 1
				continue
			}
		}

		fu.body.Add(t)
	}
}

func (fu *FuncDef) PerformInlineCall(t_call *Node, other *FuncDef) *Node {
	comp := GetComputation(t_call)

	suffix := ":" + strconv.Itoa(fu.num_inlines)

	// copy the locals, including parameters
	local_map := make(map[*LocalVar]*LocalVar)

	for _, local := range other.locals {
		local_map[local] = fu.CopyInlineLocal(local, suffix)
	}

	// NOTE: no need to update fu.labels

	// create assignments for the parameter locals
	for i, par_info := range other.params {
		call_par := comp.children[1 + i]

		new_local := local_map[par_info]
		if new_local == nil {
			panic("unmapped local")
		}

		var_name := par_info.name + suffix
		var_dest := NewNode(NP_Local, var_name, t_call.pos)
		var_dest.local = new_local
		var_dest.ty = par_info.ty

		t_move := NewNode(NH_Move, "", t_call.pos)
		t_move.Add(var_dest)
		t_move.Add(call_par)

		fu.body.Add(t_move)
	}

	// return values may need a local to store it
	var ret_local *LocalVar
	var ret_local_node *Node
	var ret_local_exist bool = false

	switch other.ty.sub.kind {
	case TYP_Void, TYP_NoReturn:
		// no local needed
	default:
		if t_call.kind == NH_Drop {
			// no local needed
		} else if t_call.kind == NH_Move && t_call.children[0].kind == NP_Local {
			// use the existing local
			ret_local_node = t_call.children[0]
			ret_local = ret_local_node.local
			ret_local_exist = true
		} else {
			ret_local = fu.NewLocal("__inline" + suffix, other.ty.sub, nil)
			ret_local_node = NewNode(NP_Local, "", t_call.pos)
			ret_local_node.local = ret_local
			ret_local_node.ty = ret_local.ty
		}
	}

	need_end := false

	// copy the statements
	for _, t2 := range other.body.children {
		if t2.kind == NH_Return {
			need_end = true
		}

		fu.CopyInlineStatement(t2, suffix, ret_local, t_call.pos, local_map)
	}

	if need_end {
		lab_end := NewNode(NH_Label, "__end" + suffix, t_call.pos)
		fu.body.Add(lab_end)
	}

	// copy the original node (NH_Move, NH_Drop, etc...)

	switch other.ty.sub.kind {
	case TYP_Void, TYP_NoReturn:
		// nothing needed
		return nil
	}

	switch t_call.kind {
	case NH_Drop:
		// nothing needed
		return nil

	case NH_Move:
		if ret_local_exist {
			// nothing needed, inlined code writes result to an existing local
			return nil
		}

		t_move := NewNode(NH_Move, "", t_call.pos)
		t_move.Add(t_call.children[0])
		t_move.Add(ret_local_node)
		return t_move

	case NH_Jump:
		t_jump := NewNode(NH_Jump, t_call.str, t_call.pos)
		t_jump.negate = t_call.negate
		t_jump.Add(ret_local_node)
		return t_jump

	case NH_Return:
		t_ret := NewNode(NH_Return, "", t_call.pos)
		t_ret.Add(ret_local_node)
		return t_ret

	default:
		panic("strange node kind in PerformInlineCall")
	}
}

func (fu *FuncDef) CopyInlineLocal(local *LocalVar, suffix string) *LocalVar {
	new_loc := fu.NewLocal(local.name + suffix, local.ty, nil)
	return new_loc
}

func (fu *FuncDef) CopyInlineStatement(t *Node, suffix string, ret_local *LocalVar, pos Position,
	local_map map[*LocalVar]*LocalVar) {

	if t.kind == NH_Return {
		// special logic for return statements: transfer a needed value
		// to the return local, then jump to the end label.

		if ret_local != nil {
			comp := fu.CopyInlineNode(t.children[0], suffix, pos, local_map)

			dest := NewNode(NP_Local, "", pos)
			dest.local = ret_local
			dest.ty = ret_local.ty

			t_move := NewNode(NH_Move, "", pos)
			t_move.Add(dest)
			t_move.Add(comp)

			fu.body.Add(t_move)
		}

		t_jump := NewNode(NH_Jump, "__end" + suffix, pos)
		fu.body.Add(t_jump)
		return
	}

	new_node := fu.CopyInlineNode(t, suffix, pos, local_map)
	fu.body.Add(new_node)
}

func (fu *FuncDef) CopyInlineNode(t1 *Node, suffix string, pos Position,
	local_map map[*LocalVar]*LocalVar) *Node {

	t2 := NewNode(t1.kind, t1.str, pos)

	t2.ty     = t1.ty
	t2.module = t1.module
	t2.offset = t1.offset
	t2.negate = t1.negate
	t2.def    = t1.def

	// adjust labels and locals with a suffix
	switch t1.kind {
	case NH_Label, NH_Jump:
		t2.str += suffix

	case NP_Local:
		t2.local = local_map[t1.local]
		if t2.local == nil {
			panic("unmapped local")
		}
	}

	for _, child := range t1.children {
		t2.Add(fu.CopyInlineNode(child, suffix, pos, local_map))
	}
	return t2
}

//----------------------------------------------------------------------

// CodePath represents a path of code statements in a function.
// The path may be empty, it represents a function which has not yet
// started execution.
type CodePath struct {
	// the next node to be visited, -1 if finished
	next int

	// nodes in the path (indexes into body)
	visited map[int]bool

	// the locals which have been created along this path.
	locals map[*LocalVar]bool

	// this path produced an error
	failed bool
}

func NewCodePath() *CodePath {
	pt := new(CodePath)
	pt.next    = 0
	pt.visited = make(map[int]bool, 0)
	pt.locals  = make(map[*LocalVar]bool)
	return pt
}

func (pt *CodePath) Copy() *CodePath {
	pt2 := new(CodePath)
	pt2.next = pt.next

	pt2.visited = make(map[int]bool)
	for idx,_ := range pt.visited {
		pt2.visited[idx] = true
	}
	pt2.locals = make(map[*LocalVar]bool)
	for loc,_ := range pt.locals {
		pt2.locals[loc] = true
	}
	return pt2
}

func (pt1 *CodePath) CanMerge(pt2 *CodePath) bool {
	return pt1.next == pt2.next
}

func (pt *CodePath) Merge(old *CodePath) {
	for idx,_ := range old.visited {
		pt.visited[idx] = true
	}

	// only keep locals that are common to both paths
	new_locals := make(map[*LocalVar]bool)
	for loc, _ := range old.locals {
		if pt.locals[loc] {
			new_locals[loc] = true
		}
	}

	pt.locals = new_locals
	pt.failed = pt.failed || old.failed

	// mark old path as dead
	old.next = -1
}

func (fu *FuncDef) FlowAnalysis() error {
	for _, t := range fu.body.children {
		t.dead = true
	}

	// create initial path
	pt := NewCodePath()

	fu.paths = make([]*CodePath, 0)
	fu.paths = append(fu.paths, pt)

	error_count := 0

	/* process active paths until done */

	for len(fu.paths) > 0 {
		// remove dead paths from the end
		last := len(fu.paths) - 1
		pt   := fu.paths[last]

		if pt.next < 0 {
			if pt.failed {
				error_count += 1
			}
			fu.paths = fu.paths[0:last]
			continue
		}

		// choose the path at earliest node
		for i := 0; i < last; i++ {
			other := fu.paths[i]
			if other.next >= 0 && other.next < pt.next {
				pt = other
			}
		}

		// attempt to merge other paths into this one
		for i := 0; i <= last; i++ {
			other := fu.paths[i]
			if other != pt {
				if pt.CanMerge(other) {
					pt.Merge(other)
				}
			}
		}

		fu.FlowStep(pt)
	}

	if error_count > 0 {
		return FAILED
	}
	return OK
}

func (fu *FuncDef) FlowStep(pt *CodePath) {
	// reached end?
	if pt.next >= fu.body.Len() {
		ret_type := fu.ty.sub

		if ret_type.kind != TYP_Void && !fu.missing_ret {
			fu.missing_ret = true
			last := fu.body.Last()
			Error_SetPos(last.pos)
			PostError("control reaches end of non-void function")
		}
		pt.next = -1
		return
	}

	// already visited? (i.e. looping back)
	if pt.visited[pt.next] {
		pt.next = -1
		return
	}

	pt.visited[pt.next] = true

	t := fu.body.children[pt.next]
	t.dead = false

	// check for use of uninitialized locals
	err := fu.FlowCheckLocals(t, pt)
	if err != nil {
		pt.failed = true
	}

	// check for creation of locals
	if t.kind == NH_Move {
		if t.children[0].kind == NP_Local {
			local := t.children[0].local
			pt.locals[local] = true
		}
	}

	// handle return statements, or equivalent
	switch t.kind {
	case NH_Return:  // NH_Abort
		pt.next = -1
		return

	case NH_Drop:
		// calling a `no-return` function?
		comp := t.children[0]

		if comp.kind == NC_Call {
			if comp.ty.kind == TYP_NoReturn {
				pt.next = -1
				return
			}
		}
		/*
		if comp.kind == NC_TailCall {
			pt.next = -1
			return
		} */
	}

	// handle jumps.
	// conditional jumps split the path into two.

	if t.kind == NH_Jump {
		dest_idx := fu.FindLabel(t.str)

		// WISH: detect when a jump is not really conditional, especially in
		//       loops where a local begins at zero and stops at a constant.

		if t.Len() > 0 {
			branch := pt.Copy()
			branch.next = pt.next + 1
			fu.paths = append(fu.paths, branch)
		}

		pt.next = dest_idx
		return
	}

	pt.next += 1
}

func (fu *FuncDef) FlowCheckLocals(t *Node, pt *CodePath) error {
	if t.kind == NP_Local {
		local := t.local

		// has been created in this path?
		if pt.locals[local] {
			return OK
		}
		// parameters don't need a previous write
		if local.param_idx >= 0 {
			return OK
		}
		// prevent multiple errors
		if !local.errored {
			local.errored = true

			Error_SetPos(t.pos)
			PostError("local '%s' may be used uninitialized", t.local.name)
		}

		pt.failed = true
		return FAILED
	}

	start := 0
	if t.kind == NH_Move && t.children[0].kind == NP_Local {
		start = 1
	}
	for pos := start; pos < t.Len(); pos++ {
		child := t.children[pos]
		if fu.FlowCheckLocals(child, pt) != OK {
			return FAILED
		}
	}

	return OK
}

func (fu *FuncDef) DeadCodeRemoval() {
	if Options.no_optim {
		return
	}

	// WISH: detect a jump statement followed by its label
	//       (with nothing but comments in between), remove it.

	for _, t := range fu.body.children {
		if t.dead {
			if t.kind != NH_Comment && t.kind != NH_Label {
				t.kind = NH_Comment
				t.str  = "[ DEAD CODE ]"
			}
		}
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) StrengthReduce() {
	// this optimizes certain nodes to ones which are more
	// efficient but produce the same result.
	// for example: `imul x 8` --> `ishl x 3`

	if Options.no_optim {
		return
	}

	// WISH: evaluate operations when all args are a literal

	for _, t := range fu.body.children {
		comp := GetComputation(t)

		if comp != nil {
			new_comp := fu.StrengthReduceNode(comp)

			if new_comp != comp {
				ReplaceComputation(t, new_comp)
			}
		}
	}
}

func (fu *FuncDef) StrengthReduceNode(t *Node) *Node {
	if t.kind != NC_OpSeq {
		return t
	}

	// handle "force" cases, e.g. a multiply by zero.
	// when found, all previous operations can be discarded.
	for i := t.Len()-1; i >= 1; i-- {
		op := t.children[i]

		if fu.SR_ForcesResult(op) {
			for k := i; k > 0; k-- {
				t.Remove(k)
			}
			t.children[0] = op.children[0]
			break
		}
	}

	// remove operations which do nothing.
	for i := t.Len()-1; i >= 1; i-- {
		op := t.children[i]

		if fu.SR_HasNoEffect(op) {
			t.Remove(i)
		}
	}

	// if there would only be a single term left, the whole NC_OpSeq
	// will be replaced by the remaining term.
	if t.Len() == 1 {
		return t.children[0]
	}

	for _, op := range t.children {
		switch op.str {
		case "imul":
			fu.StrengthReduce_imul(op)
		case "udivt":
			fu.StrengthReduce_udivt(op)
		case "uremt":
			fu.StrengthReduce_uremt(op)
		default:
			fu.StrengthReduce_compare(op)
		}
	}

	// handle `not TRUE` and `not FALSE`
	if t.Len() == 2 {
		term := t.children[0]
		op := t.children[1]

		if op.str == "bnot" {
			if LiteralIsFalse(term) {
				term.str = "1"
				return term
			}
			if LiteralIsTrue(term) {
				term.str = "0"
				return term
			}
		}
	}

	return t
}

func (fu *FuncDef) SR_ForcesResult(op *Node) bool {
	switch op.str {
	case "iand", "imul", "idivt", "udivt":
		return LiteralIsZero(op.children[0])

	case "band":
		return LiteralIsFalse(op.children[0])

	case "bor":
		return LiteralIsTrue(op.children[0])
	}

	return false
}

func (fu *FuncDef) SR_HasNoEffect(op *Node) bool {
	switch op.str {
	case "iadd", "isub", "padd", "psub", "ior", "ixor",
	     "ishl", "ishr", "ushl", "ushr":
		return LiteralIsZero(op.children[0])

	case "imul", "idivt", "udivt":
		return LiteralIsOne(op.children[0])

	case "band":
		return LiteralIsTrue(op.children[0])

	case "bor", "bxor":
		return LiteralIsFalse(op.children[0])

	case "beq?", "blt?":
		return LiteralIsTrue(op.children[0])

	case "bne?", "bgt?":
		return LiteralIsFalse(op.children[0])
	}

	return false
}

func (fu *FuncDef) StrengthReduce_imul(op *Node) {
	t_mul := op.children[0]
	if t_mul.kind != NL_Integer {
		return
	}

	val, err := strconv.ParseUint(t_mul.str, 0, op.ty.bits)
	if err != nil {
		return
	}

	// see whether the multiplier is a power of two
	for p := 0; p < op.ty.bits; p++ {
		if val == (uint64(1) << uint64(p)) {
			op.str = "ishl"
			t_mul.str = strconv.FormatUint(uint64(p), 10)
			return
		}
	}
}

func (fu *FuncDef) StrengthReduce_udivt(op *Node) {
	// only unsigned types can be optimized to a shift, since `idivt` is
	// a truncating division but a right shift is a floored division.

	t_div := op.children[0]
	if t_div.kind != NL_Integer {
		return
	}

	val, err := strconv.ParseUint(t_div.str, 0, op.ty.bits)
	if err != nil {
		return
	}

	// see whether the divisor is a power of two
	for p := 0; p < op.ty.bits; p++ {
		if val == (uint64(1) << uint64(p)) {
			op.str = "ushr"
			t_div.str = strconv.FormatUint(uint64(p), 10)
			return
		}
	}
}

func (fu *FuncDef) StrengthReduce_uremt(op *Node) {
	// this replaces a `uremt` with a masking `iand` operation.
	// it only works for unsigned types, since signed types always
	// return a value with same sign as the dividend.

	t_div := op.children[0]
	if t_div.kind != NL_Integer {
		return
	}

	val, err := strconv.ParseUint(t_div.str, 0, op.ty.bits)
	if err != nil {
		return
	}

	// see whether the divisor is a power of two
	for p := 0; p < op.ty.bits; p++ {
		if val == (uint64(1) << uint64(p)) {
			op.str = "iand"
			t_div.str = strconv.FormatUint(val - 1, 10)
			return
		}
	}
}

func (fu *FuncDef) StrengthReduce_compare(op *Node) {
	if op.Len() != 1 {
		return
	}

	t_val := op.children[0]

	switch op.str {
	case "igt?":
		if LiteralIsZero(t_val) {
			op.str = "ipos?"
			op.Remove(0)
		}

	case "ilt?":
		if LiteralIsZero(t_val) {
			op.str = "ineg?"
			op.Remove(0)
		}

	case "ieq?", "ule?":
		if LiteralIsZero(t_val) {
			op.str = "izero?"
			op.Remove(0)
		}

	case "ine?", "ugt?":
		if LiteralIsZero(t_val) {
			op.str = "isome?"
			op.Remove(0)
		}

	case "beq?":
		if LiteralIsFalse(t_val) {
			op.str = "bnot"
			op.Remove(0)
		}

	case "bne?":
		if LiteralIsTrue(t_val) {
			op.str = "bnot"
			op.Remove(0)
		}
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) UnusedLocals() {
	if Options.no_optim {
		return
	}

	// keep trying until no more changes
	for fu.UnusedLocalPass() {
	}
}

func (fu *FuncDef) UnusedLocalPass() bool {
	changed := false

	// determine # of reads and writes of each local
	fu.LiveRanges(true)

	// we remove nodes writing to an unused local
	old_body := fu.body
	fu.body = NewNode(NG_Block, "", old_body.pos)

	removals := make(map[*LocalVar]bool)

	for node_idx, t := range old_body.children {
		if t.kind != NH_Move {
			fu.body.Add(t)
			continue
		}

		dest := t.children[0]
		comp := t.children[1]

		kill_node := false

		if dest.kind == NP_Local {
			local := dest.local

			if local.param_idx >= 0 {
				// ignore parameters here

			} else if local.reads == 0 {
				kill_node = true

			} else if local.writes == 1 && t.children[1].IsLiteral() {
				// replace this never-modified local with the constant
				lit := t.children[1]
				for k := local.live_start; k <= local.live_end; k++ {
					if k != node_idx {
						fu.PropagateConstantInNode(local, old_body.children[k], lit)
					}
				}
				local.reads = 0
				kill_node = true
			}

			// leave a tombstone
			if kill_node {
				if !removals[dest.local] {
					removals[dest.local] = true

					msg   := "[ " + dest.local.name + " OPTIMISED AWAY ]"
					cmt   := NewNode(NH_Comment, msg, t.pos)
					blank := NewNode(NH_Comment, "",  t.pos)

					fu.body.Add(cmt)
					fu.body.Add(blank)
				}
			}
		}

		// certain nodes MUST be kept, these become NH_Drop
		if kill_node {
			changed = true

			switch comp.kind {
			case NC_Call:
				t2 := NewNode(NH_Drop, "", t.pos)
				t2.Add(comp)
				fu.body.Add(t2)

			default:
				// remove the node (by doing nothing)
			}
		} else {
			fu.body.Add(t)
		}
	}

	// remove the LocalVar data (rebuild the list)
	old_locals := fu.locals
	fu.locals = make([]*LocalVar, 0, len(old_locals))

	for _, local := range old_locals {
		if local.reads == 0 && local.param_idx < 0 {
			changed = true
		} else {
			fu.locals = append(fu.locals, local)
		}
	}

	return changed
}

func (fu *FuncDef) LiveRanges(ignore_updates bool) {
	for _, local := range fu.locals {
		local.reads  = 0
		local.writes = 0
		local.live_start = -1
		local.live_end   = -1
	}

	for use_pos, t := range fu.body.children {
		loop_factor := 1
		if t.loop == 1 {
			loop_factor = outer_factor
		} else if t.loop >= 2 {
			loop_factor = inner_factor
		}

		start := 0
		var skip_local *LocalVar

		if t.kind == NH_Move {
			dest := t.children[0]

			if dest.kind == NP_Local {
				local := dest.local
				local.writes += loop_factor

				fu.UpdateLiveRange(local, use_pos)

				// skip this destination when marking reads
				start = 1

				// when determining unused locals, a statement like `x = iadd x 1`
				// should not be considered as reading from ("using") the local x.
				// however function calls (etc) must not be removed.
				if ignore_updates {
					comp := t.children[1]
					if comp.kind != NC_Call {
						skip_local = local
					}
				}
			}
		}

		if t.kind == NH_Swap {
			for _, child := range t.children {
				if child.kind == NP_Local {
					local := child.local
					local.writes += loop_factor

					fu.UpdateLiveRange(local, use_pos)
				}
			}
		}

		fu.LiveRangesInNode(t, start, use_pos, loop_factor, skip_local)
	}

	// mark all parameters to be live at first node
	for _, par := range fu.params {
		par.live_start = 0
		if par.live_end < 0 {
			par.live_end = 0
		}
	}
}

func (fu *FuncDef) LiveRangesInNode(t *Node, start, use_pos, loop_factor int, skip_local *LocalVar) {
	if t.kind == NP_Local {
		local := t.local
		if local != skip_local {
			local.reads += loop_factor
		}
		fu.UpdateLiveRange(local, use_pos)
	}

	for pos := start; pos < t.Len(); pos++ {
		child := t.children[pos]
		fu.LiveRangesInNode(child, 0, use_pos, loop_factor, skip_local)
	}
}

func (fu *FuncDef) UpdateLiveRange(local *LocalVar, use_pos int) {
	if local.live_start < 0 {
		local.live_start = use_pos
		local.live_end   = use_pos

	} else if use_pos < local.live_start {
		local.live_start = use_pos

	} else if use_pos > local.live_end {
		local.live_end = use_pos
	}
}

func (fu *FuncDef) ExtendLiveRanges() {
	// this detects whether a local's last usage is inside a loop,
	// which means the live range may need to extend to the end of
	// that loop unless the local only exists solely in that loop.
	//
	// similarly, if a local is created inside a loop but used by code
	// beyond the end of the loop, its live range needs to be extended
	// to the start of that loop.

	for _, local := range fu.locals {
		if local.live_end >= 0 {
			new_end   := fu.TryExtendForward(local)
			new_start := fu.TryExtendBackward(local)

			local.live_start = new_start
			local.live_end   = new_end
		}
	}
}

func (fu *FuncDef) TryExtendForward(local *LocalVar) int {
	loop := fu.body.children[local.live_end].loop

	// when `loop` is two or more, the local is technically in multiple
	// loops at the same time, and each loop may need to extend it.

	for ; loop > 0; loop-- {
		// find range of the loop
		start := fu.FindLoopStart(local.live_end, loop)
		end   := fu.FindLoopEnd  (local.live_end, loop)

		// is the local wholly contained in this loop?
		if local.param_idx < 0 && local.live_start >= start {
			break
		}

		// WISH: if we are 100% sure that this local is written to near start
		//       of loop (without reading it), with no jumps/labels in between,
		//       then no need to extend the local for this loop.

		// extend the local
		local.live_end = end
	}

	return local.live_end
}

func (fu *FuncDef) TryExtendBackward(local *LocalVar) int {
	if local.param_idx >= 0 {
		return 0
	}

	loop := fu.body.children[local.live_start].loop

	for ; loop > 0; loop-- {
		// find range of the loop
		start := fu.FindLoopStart(local.live_start, loop)
		end   := fu.FindLoopEnd  (local.live_start, loop)

		// is the local wholly contained in this loop?
		if local.live_end <= end {
			break
		}

		// extend the local
		local.live_start = start
	}

	return local.live_start
}

func (fu *FuncDef) FindLoopStart(pos int, level int) int {
	for pos > 0 {
		t := fu.body.children[pos-1]

		// any node with .loop >= level is considered part of the loop
		if t.loop < level {
			break
		}
		pos -= 1
	}
	return pos
}

func (fu *FuncDef) FindLoopEnd(pos int, level int) int {
	for pos+1 < fu.body.Len() {
		t := fu.body.children[pos+1]

		// any node with .loop >= level is considered part of the loop
		if t.loop < level {
			break
		}
		pos += 1
	}
	return pos
}

func (fu *FuncDef) NodeReadsLocal(t *Node, local *LocalVar) bool {
	if t.kind == NP_Local {
		return t.local == local
	}

	start := 0
	if t.kind == NH_Move && t.children[0].kind == NP_Local {
		start = 1
	}
	for pos := start; pos < t.Len(); pos++ {
		child := t.children[pos]
		if fu.NodeReadsLocal(child, local) {
			return true
		}
	}

	return false
}

func (fu *FuncDef) NodeWritesLocal(t *Node, local *LocalVar) bool {
	if t.kind == NH_Move {
		dest := t.children[0]
		if dest.kind == NP_Local && dest.local == local {
			return true
		}
	}
	return false
}

func (fu *FuncDef) PropagateConstantInNode(local *LocalVar, t, lit *Node) {
	for i, child := range t.children {
		fu.PropagateConstantInNode(local, child, lit)

		if child.kind == NP_Local && child.local == local {
			// copy the literal
			lit2 := NewNode(lit.kind, lit.str, lit.pos)
			lit2.ty = lit.ty

			t.children[i] = lit2
		}
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) DetectLoops() {
	level := 0

	for {
		found := false

		for idx, line := range fu.body.children {
			if line.kind == NH_Jump {
				if fu.MarkLoop(idx, line.str, level) {
					found = true
				}
			}
		}

		if !found {
			return
		}

		level += 1
	}
}

func (fu *FuncDef) MarkLoop(idx int, label string, level int) bool {
	start := fu.FindLabel(label)

	if start >= idx {
		return false
	}

	children := fu.body.children

	// check that the loop is truly "inside" another one.
	next := idx
	if level > 0 {
		next += 1
	}

	if next < len(children) &&
		children[start+1].loop >= level &&
		children[next].loop >= level {

		// do not mark the label itself, to prevent two loops in
		// a row looking like one big loop.
		for k := start+1; k <= idx; k++ {
			fu.body.children[k].loop = level+1
		}
		return true
	}

	return false
}

func (fu *FuncDef) FindLabel(label string) int {
	for idx, line := range fu.body.children {
		if line.kind == NH_Label && line.str == label {
			return idx
		}
	}

	panic("no such label: " + label)
}

//----------------------------------------------------------------------

func GetComputation(t *Node) *Node {
	switch t.kind {
	case NH_Drop:
		return t.children[0]

	case NH_Move:
		return t.children[1]

	case NH_Jump, NH_Return:
		if t.Len() > 0 {
			return t.children[0]
		}
	}

	return nil
}

func ReplaceComputation(t *Node, comp *Node) {
	switch t.kind {
	case NH_Drop, NH_Jump, NH_Return:
		t.children[0] = comp

	case NH_Move:
		t.children[1] = comp

	default:
		panic("ReplaceComputation in odd node")
	}
}

func GetCalledFunc(t *Node) *FuncDef {
	// input node must be an NC_Call or NC_TailCall, or a high level
	// node which has one of those as its computation.  result is NIL
	// when the function is not a global definition (or a method).

	comp := GetComputation(t)
	if comp != nil {
		t = comp
	}

	switch t.kind {
	case NC_Call:  // NC_TailCall
		// ok
	default:
		return nil
	}

	t_func := t.children[0]

	switch t_func.kind {
	case NP_GlobPtr:
		return t_func.def.d_func

	case NP_Method:
		belong_type := t_func.ty.param[0].ty.sub
		return belong_type.FindMethod(t_func.str)
	}

	return nil
}

//----------------------------------------------------------------------

func (fu *FuncDef) CostOfLocal(local *LocalVar, c_create, c_write, c_read int) int64 {
	// compute the cost of keeping this local on the stack (as compared
	// to keeping it in a register).  the result is an estimation based
	// on the number of times the local is read and written, taking loops
	// into account.
	//
	// the `c_xxx` parameters are cost values, which should be >= 0.
	// -  c_create is the cost of creation
	// -  c_write  is the cost of all writes (including the first)
	// -  c_read   is the cost of all reads.

	var cost int64

	if local.param_idx >= 0 {
		if local.abi_reg != REG_ON_STACK {
			// this parameter begins in a reg, but gets written to the stack.
			cost += int64(c_create)
		}
	} else {
		if local.writes > 0 {
			cost += int64(c_create)
		}
	}

	cost += int64(c_write) * int64(local.writes)
	cost += int64(c_read)  * int64(local.reads)

	// slight bonus to temporary vars
	if len(local.name) > 2 && local.name[0] == '_' && local.name[1] == '_' {
		cost += 1
	}

	return cost
}
