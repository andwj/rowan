// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"
import "fmt"
import "path/filepath"

// this indicates that a parsing function which normally returns a
// Node pointer has failed, and posted an error via PostError.
var P_FAIL *Node

// this indicates that the expression parser encountered a constant
// which has not been evaluated yet.
var P_UNKNOWN *Node

// node kinds for the compiling phase
const (
	NH_INVALID NodeKind = 200 + iota

	NH_Label    // a label for jumps
	NH_Comment  // a comment for the generated assembly

	/* operation sequence */

	NC_OpSeq    // term | zero or more NC_Operator...
	NC_Op       // term (optional).  str is name.  ty is set

	/* output nodes */

	NZ_Local    // a local variable
	NZ_Target   // a target for assign or swap
	NZ_Term     // a term (typed literal, glob-ptr, mem access)
	NZ_Comp     // a computation (expression, call, matches)
)

//----------------------------------------------------------------------

func DeduceAllFunctions() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Func || def.kind == DEF_Method {
			def.d_func.Deduce()
		}
	}
	Error_SetWhat("")
}

func ParseAllFunctions() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Func || def.kind == DEF_Method {
			if !def.d_func.extern {
				def.d_func.Parse()
			}
		}
	}
	Error_SetWhat("")
}

func CompileAllFunctions() {
	cur_mod.public = 0

	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Func || def.kind == DEF_Method {
			def.d_func.Compile()
		}
	}
	Error_SetWhat("")
}

//----------------------------------------------------------------------

// Deduce determines the overall type of the function.
// It does not examine the body of the function.
func (fu *FuncDef) Deduce() error {
	Error_SetWhat(fu.name)
	Error_SetPos(fu.raw_pars.pos)

	fu.ty = ParseParameters(fu.raw_pars)
	if fu.ty == nil {
		return FAILED
	}
	if ValidateType(fu.ty) != OK {
		return FAILED
	}

	// store methods into the Type which they belong, and check
	// for duplicate names.
	if fu.method {
		if len(fu.ty.param) == 0 {
			PostError("method definition with no receiver")
			return FAILED
		}
		self_type := fu.ty.param[0].ty
		if self_type.kind != TYP_Pointer {
			PostError("method receiver must be a pointer, got a %s",
				self_type.SimpleName())
			return FAILED
		}
		belong_type := self_type.sub
		if belong_type.def == nil {
			PostError("method receiver must be a custom type")
			return FAILED
		}
		if belong_type.FindMethod(fu.name) != nil {
			PostError("method '%s' already defined in type %s",
				fu.name, belong_type.def.name)
			return FAILED
		}
		belong_type.AddMethod(fu)
	}

	return OK
}

//----------------------------------------------------------------------

// Compile analyses the code in the function and produces the low-level
// syntax for the back-end code generator.
func (fu *FuncDef) Compile() error {
	Error_SetWhat(fu.name)

	out_name := ""

	if fu.method {
		belong_type := fu.ty.param[0].ty.sub
		out_name = EncodeMethodName(fu.name, belong_type)
	} else {
		out_name = "@" + EncodeName(fu.name, fu.module)
	}

	if fu.extern {
		OutLine("extern %s", out_name)
		OutLine("")
		return OK
	}

	OutVisibility(fu.public)

	OutLine("fun %s %s", out_name, fu.EncodeParameters())

	if fu.body.pos.file > 0 {
		filename  := all_filenames[fu.body.pos.file - 1]
		filename   = filepath.Base(filename)
		glob_name := cur_mod.AddStringUTF8(filename)
		OutCode("file %s", glob_name)
	}

	if fu.CompileBlock(fu.body) != OK {
		return FAILED
	}

	OutLine("end")
	OutLine("")

	return OK
}

func (fu *FuncDef) EncodeParameters() string {
	fun_type := fu.ty

	s := "("

	for i := 0 ; i < len(fun_type.param) ; i++ {
		par_name := fun_type.param[i].name
		par_type := fun_type.param[i].ty

		s = s + "%" + EncodeName(par_name, "")
		s = s + " " + par_type.Encode() + " "
	}

	ret_str := "-> " + fun_type.sub.Encode()

	return s + ret_str + ")"
}

func (fu *FuncDef) CompileBlock(block *Node) error {
	// empty block?
	if block.Len() == 0 {
		return OK
	}

	for idx := 0; idx < block.Len(); idx++ {
		statement := block.children[idx]

		if fu.CompileStatement(statement) != OK {
			// allow a limited number of errors per function
			fu.error_num += 1
			if fu.error_num > 5 && !Options.all_errors {
				break
			}
		}
	}

	if fu.error_num > 0 {
		return FAILED
	}
	return OK
}

func (fu *FuncDef) CompileStatement(t *Node) error {
	Error_SetPos(t.pos)

	switch t.kind {
	case NH_Comment:
		OutCode("line %d %s", t.pos.line, EncodeStringLiteral(t.str))
		return OK

	case NH_Label:
		OutLine("%s:", EncodeLabel(t.str))
		return OK

	case NG_Block:
		return fu.CompileBlock(t)

	case NX_Call:
		t_call := fu.Comp_FunCall(t, false, false)
		if t_call == P_FAIL {
			return FAILED
		}
		OutCode("%s", t_call.str)
		return OK

	case NX_Method:
		t_call := fu.Comp_MethodCall(t, false, false)
		if t_call == P_FAIL {
			return FAILED
		}
		OutCode("%s", t_call.str)
		return OK

	case NS_Let:
		return fu.CompileLet(t)

	case NS_Assign:
		return fu.CompileAssign(t)

	case NS_Ignore:
		return fu.CompileIgnore(t)

	case NS_Return:
		return fu.CompileReturn(t)

	case NS_Implicit:
		return fu.CompileImplicitReturn(t)

	case NS_If:
		return fu.CompileIf(t)

	case NS_Match:
		return fu.CompileMatch(t)

	case NS_Panic:
		return fu.CompilePanic(t)

	case NS_Assert:
		return fu.CompileAssert(t)

	case NS_Loop:
		return fu.CompileLoop(t)

	case NS_Jump, NS_Continue, NS_Break:
		return fu.CompileJump(t, false)

	case NS_Swap:
		return fu.CompileSwap(t)

	case NS_TailCall:
		return fu.CompileTailCall(t)

	default:
		panic("weird node")
	}
}

func (fu *FuncDef) CompileLet(t *Node) error {
	local := t.local

	comp := fu.CompileComputation(t.children[0], nil)
	if comp == P_FAIL {
		return FAILED
	}

//TODO be nice to keep this error (but check happens deep in CompileComputation)
//
//--	// check the type
//--	if comp.ty == nil {
//--		PostError("local '%s' needs explicit type (literals are untyped)", local.name)
//--		return FAILED
//--	}

	switch comp.ty.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok
	default:
		PostError("local '%s' inferred as %s", local.name, comp.ty.SimpleName())
		return FAILED
	}

	local.ty = comp.ty

	OutCode("%s = %s", local.Encode(), comp.str)
	return OK
}

func (fu *FuncDef) CompileAssign(t *Node) error {
	dest := fu.CompileTarget(t.children[0])
	if dest == P_FAIL {
		return FAILED
	}

	// this will perform type checking
	comp := fu.CompileComputation(t.children[1], dest.ty)

	OutCode("%s = %s", dest.str, comp.str)
	return OK
}

func (fu *FuncDef) CompileIgnore(t *Node) error {
	comp := fu.CompileComputation(t.children[0], nil)
	if comp == P_FAIL {
		return FAILED
	}

	fu.DropComputation(comp.str)
	return OK
}

func (fu *FuncDef) CompileReturn(t *Node) error {
	ret_type := fu.ty.sub

	if ret_type.kind == TYP_NoReturn {
		PostError("return used in a no-return function")
		return FAILED
	}
	if ret_type.kind == TYP_Void && t.Len() > 0 {
		PostError("return with value in void function")
		return FAILED
	}
	if ret_type.kind != TYP_Void && t.Len() == 0 {
		PostError("missing value after return")
		return FAILED
	}

	if t.Len() == 0 {
		OutCode("ret")
		return OK
	}

	child := t.children[0]

	comp := fu.CompileComputation(child, ret_type)
	if comp == P_FAIL {
		return FAILED
	}

	OutCode("ret %s", comp.str)
	return OK
}

func (fu *FuncDef) CompileImplicitReturn(t *Node) error {
	// note that parser does not create a NS_Implicit node when
	// current function has a void or no-return type.
	ret_type := fu.ty.sub

	// we need to handle literals, but also accept function/method
	// calls which don't return anything.  hence we only pass a type
	// to CompileComputation() when the node is a literal.
	var dest_type *Type = nil

	child := t.children[0]

	if child.IsLiteral() {
		dest_type = ret_type
	}

	comp := fu.CompileComputation(child, dest_type)
	if comp == P_FAIL {
		return FAILED
	}
	if comp.ty.kind == TYP_Void {
		PostError("control reaches end of non-void function")
		return FAILED
	}
	if comp.ty.kind == TYP_NoReturn {
		fu.DropComputation(comp.str)
		return OK
	}

	if dest_type == nil {
		// WISH: get a NZ_Comp here (omit the temporary)
		comp = fu.CheckUpgradeType(comp, ret_type)
		if comp == P_FAIL {
			return FAILED
		}
	}

	OutCode("ret %s", comp.str)
	return OK
}

func (fu *FuncDef) CompileIf(t *Node) error {
	t_cond := t.children[0]
	t_true := t.children[1]

	var t_false *Node

	suffix     := fu.GenLabel("")
	done_label := "__endif" + suffix
	else_label := ""

	t_jump := NewNode(NS_Jump, done_label, t.pos)

	if t.Len() >= 3 {
		t_false = t.children[2]
		else_label = "__else" + suffix
		t_jump.str = else_label
	}

	t_jump.Add(t_cond)

	if fu.CompileJump(t_jump, true) != OK {
		return FAILED
	}

	if fu.CompileStatement(t_true) != OK {
		return FAILED
	}

	if t.Len() >= 3 {
		OutCode("jump %s", EncodeLabel(done_label))

		fu.EmitLabel(else_label, t.pos)

		if fu.CompileStatement(t_false) != OK {
			return FAILED
		}
	}

	fu.EmitLabel(done_label, t.pos)
	return OK
}

func (fu *FuncDef) CompilePanic(t *Node) error {
	glob_name := cur_mod.AddStringUTF8(t.str)
	OutCode("panic %s", glob_name)
	return OK
}

func (fu *FuncDef) CompileAssert(t *Node) error {
	t_cond   := t.children[0]
	ok_label := fu.GenLabel("__assert")

	t_jump := NewNode(NS_Jump, ok_label, t.pos)
	t_jump.Add(t_cond)

	if fu.CompileJump(t_jump, false) != OK {
		return FAILED
	}

	// create the message
	msg := "assertion failed"
	glob_name := cur_mod.AddStringUTF8(msg)

	OutCode("panic %s", glob_name)

	fu.EmitLabel(ok_label, t.pos)
	return OK
}

func (fu *FuncDef) CompileMatch(t *Node) error {
	exit_label := fu.GenLabel("__endmatch")

	// grab the value to match.
	// use CompileTerm to ensure a computation is shifted.
	comp := fu.CompileTerm(t.children[0], nil)
	if comp == P_FAIL {
		return FAILED
	}

	// check the type
	match_type := comp.ty

	switch match_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool, TYP_Pointer:
		// ok
	default:
		PostError("cannot match a %s", match_type.SimpleName())
		return FAILED
	}

	// handle each match expression...
	for _, rule := range t.children[1:] {
		Error_SetPos(rule.pos)

		rule_body := rule.children[0]
		catch_all := rule.Len() < 2

		match_str := ""

		if ! catch_all {
			match_str = "matches? " + comp.str

			// collect the values to compare with
			for i := 1; i < rule.Len(); i++ {
				t_value := rule.children[i]

				term := fu.CompileTerm(t_value, match_type)
				if term == P_FAIL {
					return FAILED
				}

				match_str = match_str + " " + term.str
			}
		}

		// label for next check
		next_label := fu.GenLabel("__nextmatch")

		if ! catch_all {
			OutCode("jump %s if not %s", EncodeLabel(next_label), match_str)
		}

		if fu.CompileStatement(rule_body) != OK {
			return FAILED
		}

		OutCode("jump %s", EncodeLabel(exit_label))

		if !catch_all {
			fu.EmitLabel(next_label, rule.pos)
		}
	}

	fu.EmitLabel(exit_label, t.pos)
	return OK
}

func (fu *FuncDef) CompileLoop(t *Node) error {
	info := t.loopinfo

	fu.EmitLabel(info.start_label, t.pos)

	var t_cond *Node
	if t.Len() > 1 {
		t_cond = t.children[1]
	}

	if t_cond != nil {
		t_break := NewNode(NS_Jump, info.break_label, t.pos)
		t_break.Add(t_cond)

		if fu.CompileJump(t_break, false) != OK {
			return FAILED
		}
	}

	t_block := t.children[0]

	if fu.CompileStatement(t_block) != OK {
		return FAILED
	}

	OutCode("jump %s", EncodeLabel(info.start_label))

	if info.break_label != "" {
		fu.EmitLabel(info.break_label, t.pos)
	}
	return OK
}

func (fu *FuncDef) CompileJump(t *Node, negate bool) error {
	// Note: this also handles NS_Continue and NS_Break

	line := "jump " + EncodeLabel(t.str)

	if t.Len() > 0 {
		t_cond := t.children[0]

		if t_cond.kind == NX_Unary && t_cond.str == "not" {
			negate = ! negate
			t_cond = t_cond.children[0]
		}

		line = line + " if "
		if negate {
			line = line + "not "
		}

		comp := fu.CompileComputation(t_cond, bool_type)
		if comp == P_FAIL {
			return FAILED
		}

		line = line + comp.str
	}

	OutCode("%s", line)
	return OK
}

func (fu *FuncDef) CompileSwap(t *Node) error {
	left  := fu.CompileTarget(t.children[0])
	right := fu.CompileTarget(t.children[1])

	if left == P_FAIL || right == P_FAIL {
		return FAILED
	}

	if ! TypeCompare(left.ty, right.ty) {
		PostError("type mismatch in swap: got %s and %s",
			left.ty.String(), right.ty.String())
		return FAILED
	}

	OutCode("swap %s %s", left.str, right.str)
	return OK
}

//----------------------------------------------------------------------

func (fu *FuncDef) CompileTailCall(t *Node) error {
	t = t.children[0]

	// this limitation stems from the Win64 ABI, since tail-calls can
	// only pass parameters via registers (NEVER via the stack).
	if t.Len() - 1 > 4 {
		PostError("too many parameters for a tail call (limit is 4)")
		return FAILED
	}

	var t_call *Node = nil

	switch t.kind {
		case NX_Call:
			t_call = fu.Comp_FunCall(t, false, true)

		case NX_Method:
			t_call = fu.Comp_MethodCall(t, false, true)
	}

	if t_call == P_FAIL {
		return FAILED
	}
	OutCode("%s", t_call.str)
	return OK
}

//----------------------------------------------------------------------

func (fu *FuncDef) MakeOpSeq(t *Node) *Node {
	if t.kind == NC_OpSeq {
		return t
	}
	seq := NewNode(NC_OpSeq, "", t.pos)
	seq.Add(t)
	seq.ty = t.ty
	return seq
}

func (fu *FuncDef) OpSeqApply(op *Node, name string, ty *Type, term *Node) {
	apply := NewNode(NC_Op, name, op.pos)
	apply.ty = ty

	if term != nil {
		apply.Add(term)
	}

	op.Add(apply)
	op.ty = ty
}

func (fu *FuncDef) EncodeOpSeq(t *Node) *Node {
	// output is a NZ_Comp node.

	if t.Len() == 1 {
		base := t.children[0]
		base.ty = t.ty
		return base
	}

	line := "("
	line  = line + fu.EncodeTerm(t.children[0])

	for _, op := range t.children[1:] {
		op_str := fu.EncodeOperation(op)
		line = line + " " + op_str
	}

	line = line + ")"

	comp := NewNode(NZ_Comp, line, t.pos)
	comp.ty = t.ty
	return comp
}

func (fu *FuncDef) EncodeOperation(op *Node) string {
	op_str := op.str

	// a conversion op?
	switch op.str {
	case "itof", "utof", "ftoi", "ftou", "btoi",
		"iext", "uext", "fext", "raw-cast":
		type_str := op.ty.Encode()
		return op_str + " " + type_str
	}

	// binary op?
	if op.Len() > 0 {
		arg_str := fu.EncodeTerm(op.children[0])
		return op_str + " " + arg_str
	}

	// unary op
	return op_str
}

func (fu *FuncDef) EncodeTerm(t *Node) string {
	if t.kind < NZ_Local {
		panic("bad term in NC_OpSeq")
	}
	return t.str
}

//----------------------------------------------------------------------

func (fu *FuncDef) CheckUpgradeType(t *Node, dest_type *Type) *Node {
	// (a) handle auto conversion of subtypes
	// (b) for everything else, just check type compatibility
	// input should be a NZ_XXX node, output is same node or a NZ_Local.

	if t.ty == nil {
		panic("node with no type")
	}

	if TypeIsSubtype(t.ty, dest_type) {
		if t.IsLiteral() {
			t.ty = dest_type
			return t
		}

		op_seq := fu.Math_DoConvert(t, dest_type)
		comp   := fu.EncodeOpSeq(op_seq)
		return    fu.ShiftToTemporary(comp, "__upgrade")
	}

	if ! TypeCompare(t.ty, dest_type) {
		PostError("type mismatch: wanted %s, got %s", dest_type.String(), t.ty.String())
		return P_FAIL
	}
	return t
}

func (fu *FuncDef) DropComputation(comp string) {
	temp := "%" + fu.NewTemporary("__drop")
	OutCode("%s = %s", temp, comp)
}

func (fu *FuncDef) ShiftComputation(comp *Node) *Node {
	// if input is a NZ_Comp, it is shifted to a temporary var.
	// otherwise returns the input unchanged.

	if comp.kind == NZ_Comp {
		return fu.ShiftToTemporary(comp, "__comp")
	}

	return comp
}

func (fu *FuncDef) ShiftToTemporary(comp *Node, prefix string) *Node {
	// input node must be a NZ_XXX, output is a *new* NZ_Local.

	if comp.ty == nil {
		panic("node with no type")
	}

	// create the temporary local
	temp  := fu.NewTemporary(prefix)
	local := fu.NewLocal(temp, comp.ty)

	// insert new assignment before the current line
	OutCode("%s = %s", local.Encode(), comp.str)

	t2 := NewNode(NZ_Local, local.Encode(), comp.pos)
	t2.ty = local.ty
	return t2
}

func (fu *FuncDef) LineAsText(line *Node) string {
	s := ""

	for i, t := range line.children {
		token := t.str

		switch t.kind {
		case NL_String: token = "\"...\""
		case NL_Null:   token = "NULL"

		case NL_Bool:
			if LiteralIsFalse(t) {
				token = "FALSE"
			} else if LiteralIsTrue(t) {
				token = "TRUE"
			} else {
				token = t.str
			}

		case NL_Char:
			ch, _ := strconv.Atoi(t.str)
			if 32 <= ch && ch <= 126 {
				token = fmt.Sprintf("'%c'", rune(ch))
			} else if ch <= 0xFFFF {
				token = fmt.Sprintf("'\\u%04X'", ch)
			} else {
				token = fmt.Sprintf("'\\U%08X'", ch)
			}

		case NG_Expr:   token = "(" + fu.LineAsText(t) + ")"
		case NG_Data:   token = "[" + fu.LineAsText(t) + "]"
		case NG_Block:  token = "{...}"
		}

		if token == "" {
			token = "????"
		}
		if i > 0 {
			s += " "
		}
		s += token
	}

	return s
}

func (fu *FuncDef) NewTemporary(prefix string) string {
	fu.num_temps += 1
	return prefix + strconv.Itoa(fu.num_temps)
}

func (fu *FuncDef) NewLocal(name string, ty *Type) *LocalVar {
	local := new(LocalVar)
	local.name = name
	local.ty   = ty
	local.param_idx = -1

	fu.locals = append(fu.locals, local)

	return local
}

//----------------------------------------------------------------------

func (fu *FuncDef) DetectMissingLabels() error {
	problems := 0

	// check that jumps go into their own block, or a parent of it
	for _, jump_info := range fu.jumps {
		scope := jump_info.scope
		name  := jump_info.name

		if ! scope.HasLabel(name) {
			Error_SetPos(jump_info.pos)

			if fu.labels[name] == nil {
				PostError("missing label: %s", name)
			} else {
				PostError("jump into foreign block")
			}
			problems += 1
		}
	}

	if problems > 0 {
		return FAILED
	}

	return OK
}
func (fu *FuncDef) GenLabel(prefix string) string {
	fu.gen_labels += 1
	num_str := strconv.Itoa(fu.gen_labels)
	return prefix + num_str
}

func (fu *FuncDef) EmitLabel(name string, pos Position) {
	info := new(LabelInfo)
	info.name = name
	info.pos  = pos

	fu.labels[name] = info

	OutLine("%s:", EncodeLabel(name))
}

//----------------------------------------------------------------------

func (fu *FuncDef) DetectMissingReturn() error {
	// Note: this logic is simplistic and can produce false positives
	// (giving an error when the code cannot actually reach the end
	// the function), but should never produce false negatives.

	ret_type := fu.ty.sub

	if ret_type.kind == TYP_Void {
		return OK
	}

	no_return := (ret_type.kind == TYP_NoReturn)

	if ! fu.BlockHasReturn(fu.body) {
		if no_return {
			PostError("control may reach end of no-return function")
		} else {
			PostError("missing return at end of non-void function")
		}
		return FAILED
	}

	return OK
}

func (fu *FuncDef) BlockHasReturn(t *Node) bool {
	Error_SetPos(t.pos)

	switch t.kind {
	case NG_Block:
		if t.Len() == 0 {
			return false
		}
		return fu.BlockHasReturn(t.Last())

	case NS_Return, NS_Implicit:
		return true

	case NS_TailCall, NS_Panic:
		return true

	case NS_Jump, NS_Continue, NS_Break:
		// this is last node, jump must go backwards or outside block
		return true

	case NS_If:
		// when no else clause, can fall through
		if t.Len() < 3 {
			return false
		}
		if ! fu.BlockHasReturn(t.children[1]) { return false }
		if ! fu.BlockHasReturn(t.children[2]) { return false }
		return true

	case NS_Match:
		if t.Len() == 0 {
			return false
		}
		// when no match-all case, can fall through
		last_rule := t.Last()
		if last_rule.Len() > 0 {
			return false
		}
		// check all bodies
		for _, rule := range t.children {
			if ! fu.BlockHasReturn(rule.children[0]) {
				return false
			}
		}
		return true

	case NS_Loop:
		return fu.LoopNeverEnds(t)

	case NX_Call:
		return fu.CallNeverReturns(t)

	default:
		return false
	}
}

func (fu *FuncDef) LoopNeverEnds(t *Node) bool {
	info := t.loopinfo

	if info.break_label != "" {
		return false
	}

	// WISH: detect a NS_Jump somewhere in the body

	return true
}

func (fu *FuncDef) CallNeverReturns(t *Node) bool {
	t_func := t.children[0]

	if t_func.kind == NX_ChkNull {
		t_func = t_func.children[0]
	}
	if t_func.kind != NX_Func {
		return false
	}

	fun_type := t_func.def.d_func.ty

	return fun_type.sub.kind == TYP_NoReturn
}
