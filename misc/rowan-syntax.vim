" Vim syntax file
" Language:	Rowan
" Maintainer:	Andrew Apted  <ajapted@users.sf.net>
" Last Change:	2022 Mar 15
" Home Page:	https://gitlab.com/andwj/rowan

" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

" syntax is case sensitive
syn case match

" identifier characters
setlocal iskeyword=33,35-39,42-43,45-46,48-57,60-90,95,97-122,126,_

" identifiers
syn match rowan_Identifier	/\k\+/

" module prefixes
syn match rowan_Module		/\k\+::/

" special directives
syn match  rowan_Directive	/#\k\+/
syn region rowan_Directive	start=/^\s*#/ end=/$/ keepend

" comments
syn region  rowan_Comment	start=/;/ end=/$/ contains=rowan_CommentTodo keepend
syn region  rowan_Comment	start=/^\s*#\[/   end=/^\s*#]/  contains=rowan_CommentTodo
syn keyword rowan_CommentTodo 	contained TODO FIXME XXX

" strings and characters
syn region rowan_String		start=/"/ skip=/\\\\\|\\"/ end=/"/
syn region rowan_String		start=/'/ skip=/\\\\\|\\'/ end=/'/

" integers
syn match rowan_Number		/[+-]\?\d\+/
syn match rowan_Number		/[+-]\?0b[01]\+/
syn match rowan_Number		/[+-]\?0x\x\+/

" floating point
syn match rowan_Number		/[+-]\?\d\+[.]\d\+/
syn match rowan_Number		/[+-]\?\d\+[.]\d*[eE][+-]\?\d\+/
syn match rowan_Number		/[+-]\?\d\+[eE][+-]\?\d\+/
syn match rowan_Number		/[+-]\?0x\x\+[.]\x*[pP][+-]\?\d\+/
syn match rowan_Number		/[+-]\?0x\x\+[pP][+-]\?\d\+/

" types
syn keyword rowan_Type		void no-return raw-mem
syn keyword rowan_Type		s8 s16 s32 s64 ssize
syn keyword rowan_Type		u8 u16 u32 u64 usize bool
syn keyword rowan_Type		f32 f64
syn keyword rowan_Type		array struct union raw-struct raw-union

syn match rowan_Type		/\^/

" language keywords
syn keyword rowan_Keyword	alias const type extern-type
syn keyword rowan_Keyword	var extern-var zero-var rom-var let
syn keyword rowan_Keyword	fun extern-fun inline-fun
syn keyword rowan_Keyword	if else loop while until unless
syn keyword rowan_Keyword	do return jump break continue
syn keyword rowan_Keyword	swap mem-read mem-write stack-var
syn keyword rowan_Keyword	call tail-call cast match case
syn keyword rowan_Keyword	and or not ref assert panic

" constants
syn keyword rowan_Constant	NULL FALSE TRUE
syn keyword rowan_Constant	+INF -INF QNAN SNAN
syn keyword rowan_Constant	CPU_WORD_SIZE CPU_ENDIAN

" operators and builtins
syn keyword rowan_Function	zero? some? pos? neg?
syn keyword rowan_Function	null? ref? inf? nan?
syn keyword rowan_Function	round trunc floor ceil
syn keyword rowan_Function	abs sqrt
syn keyword rowan_Function	little-endian big-endian byte-swap
syn keyword rowan_Function	min max matches?

" Define the default highlighting.
" Only when an item doesn't have highlighting yet

hi def link rowan_Comment	Comment
hi def link rowan_CommentTodo	Todo
hi def link rowan_Constant	Constant
hi def link rowan_Directive	Special
hi def link rowan_Function	Function
hi def link rowan_Identifier	Normal
hi def link rowan_Keyword	Keyword
hi def link rowan_Module	Special
hi def link rowan_Number	Number
hi def link rowan_NumberError	Error
hi def link rowan_NumberFloat	Float
hi def link rowan_String	String
hi def link rowan_StringError	Error
hi def link rowan_StringEscape	SpecialChar
hi def link rowan_Type		Type

let b:current_syntax = "rowan"

" vim:ts=8:sw=8:noexpandtab:nowrap
