// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "math"
import "strconv"
import "strings"

type EvalContext struct {
	fu *FuncDef
}

func (ctx *EvalContext) EvalConstExprs(t *Node) *Node {
	// literals are already constants
	if t.IsLiteral() {
		return t
	}

	for i, c1 := range t.children {
		c2 := ctx.EvalConstExprs(c1)
		if c2 == P_FAIL {
			return P_FAIL
		}
		t.children[i] = c2
	}

	Error_SetPos(t.pos)

	switch t.kind {
	case NX_Const:
		return ctx.Eval_Const(t)

	case NX_Unary:
		return ctx.Eval_Unary(t)

	case NX_Binary:
		return ctx.Eval_Binary(t)

	case NX_Min, NX_Max:
		return ctx.Eval_MinMax(t)
	}

	if ctx.fu == nil {
		PostError("strange term in constant expression")
		return P_FAIL
	}

	if t.kind == NX_Conv {
		child := t.children[0]
		if child.IsLiteral() {
			return ctx.Eval_TypedLiteral(t)
		}
	}

	return t
}

func (ctx *EvalContext) Eval_Const(t *Node) *Node {
	cdef := t.def.d_const
	return cdef.MakeLiteral(t.pos)
}

func (ctx *EvalContext) Eval_Unary(t *Node) *Node {
	op := operators[t.str]
	L  := t.children[0]

	switch L.kind {
	case NL_Integer, NL_Char:
		// if literal has a type, cannot use const expr evaluator
		if L.ty == nil {
			return EV_UnaryInteger(op.name, L)
		}

	case NL_Float:
		if L.ty == nil {
			return EV_UnaryFloat(op.name, L)
		}

	case NL_FltSpec:
		if ctx.fu == nil {
			PostError("detected an infinity or NaN in const expression")
			return P_FAIL
		}

	case NL_Bool:
		return EV_UnaryBool(op.name, L)
	}

	if ctx.fu == nil {
		PostError("cannot use operator '%s' in that context", op.name)
		return P_FAIL
	}

	return t
}

func (ctx *EvalContext) Eval_Binary(t *Node) *Node {
	op := operators[t.str]
	L  := t.children[0]
	R  := t.children[1]

	if L.kind == NL_FltSpec || R.kind == NL_FltSpec {
		if ctx.fu == nil {
			PostError("detected an infinity or NaN in const expression")
			return P_FAIL
		}
		return t
	}

	if L.IsLiteral() && R.IsLiteral() {
		// check types
		L_kind := L.kind
		R_kind := R.kind

		// allow mixing integer are character literals
		if L_kind == NL_Char { L_kind = NL_Integer }
		if R_kind == NL_Char { R_kind = NL_Integer }

		// when mixing integer and float, upgrade the integer to a float
		if L.ty == nil && L_kind == NL_Integer && R_kind == NL_Float {
			if IntegerToFloat(L) != OK {
				return P_FAIL
			}
			L_kind = NL_Float
		}
		if R.ty == nil && R_kind == NL_Integer && L_kind == NL_Float {
			if IntegerToFloat(R) != OK {
				return P_FAIL
			}
			R_kind = NL_Float
		}

		if L_kind != R_kind {
			PostError("type mismatch with operator '%s'", op.name)
			return P_FAIL
		}

		switch L_kind {
		case NL_Integer:
			// if either literal has a type, cannot use const expr evaluator
			if L.ty == nil && R.ty == nil {
				// if one of the arguments is negative, use signed ints.
				// otherwise use unsigned ints to compute the result.
				if L.str[0] == '-' || R.str[0] == '-' {
					return EV_BinarySigned(op.name, L, R)
				} else {
					return EV_BinaryUnsigned(op.name, L, R)
				}
			}

		case NL_Float:
			if L.ty == nil && R.ty == nil {
				return EV_BinaryFloat(op.name, L, R)
			}

		case NL_Bool:
			return EV_BinaryBool(op.name, L, R)

		case NL_String:
			return EV_BinaryString(op.name, L, R)
		}
	}

	if ctx.fu == nil {
		PostError("cannot use operator '%s' in that context", op.name)
		return P_FAIL
	}
	return t
}

func (ctx *EvalContext) Eval_MinMax(t *Node) *Node {
	// check nodes, and determine final type (int or float)
	is_float := false

	for _, child := range t.children {
		switch child.kind {
		case NL_Integer, NL_Char:
			// ok

		case NL_Float:
			is_float = true
			// ok

		case NL_FltSpec:
			if ctx.fu == nil {
				PostError("detected an infinity or NaN in const expression")
				return P_FAIL
			}
			return t

		default:
			if ctx.fu == nil {
				PostError("strange term in min/max expression")
				return P_FAIL
			}
			return t
		}
	}

	if t.Len() == 1 {
		return t.children[0]
	}

	// actually compute the minimum or maximum
	is_max := (t.kind == NX_Max)

	res := t.children[0]

	for i, child := range t.children {
		if i > 0 {
			if is_float {
				res = EV_FloatMinMax(is_max, res, child)
			} else {
				res = EV_IntegerMinMax(is_max, res, child)
			}

			if res.kind == NL_ERROR {
				break
			}
		}
	}

	return res
}

func (ctx *EvalContext) Eval_TypedLiteral(t_conv *Node) *Node {
	lit := t_conv.children[0]
	dest_type := t_conv.ty

	// check destination type makes sense
	switch dest_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool, TYP_Pointer:
		// ok
	default:
		PostError("a literal value cannot be a %s", dest_type.SimpleName())
		return P_FAIL
	}

	switch lit.kind {
	case NL_Integer, NL_Char:
		switch dest_type.kind {
		case TYP_Int:
			if IntegerForceSize(lit, dest_type.bits) != OK {
				return P_FAIL
			}
			lit.ty = dest_type
			return lit

		case TYP_Float:
			if IntegerToFloat(lit) != OK {
				return P_FAIL
			}
			lit.ty = dest_type
			return lit

		case TYP_Bool:
			IntegerToBool(lit)
			return lit
		}

	case NL_Bool:
		switch dest_type.kind {
		case TYP_Bool:
			return lit

		case TYP_Int:
			lit.kind = NL_Integer
			lit.ty   = dest_type
			return lit

		case TYP_Float:
			lit.kind = NL_Float
			lit.str  = lit.str + ".0"
			lit.ty   = dest_type
			return lit
		}

	case NL_Float:
		switch dest_type.kind {
		case TYP_Float:
			if CheckLiteral(lit, dest_type) != OK {
				return P_FAIL
			}
			return lit

		case TYP_Int:
			if dest_type.unsigned {
				PostError("cannot convert between float and unsigned int")
				return P_FAIL
			}
			if FloatToInteger(lit) != OK {
				return P_FAIL
			}
			if IntegerForceSize(lit, dest_type.bits) != OK {
				return P_FAIL
			}
			lit.ty = dest_type
			return lit

		case TYP_Bool:
			FloatToBool(lit)
			return lit
		}

	default:
		if CheckLiteral(lit, dest_type) != OK {
			return P_FAIL
		}
		return lit
	}

	PostError("incompatible type for literal: %s", dest_type.String())
	return P_FAIL
}

//----------------------------------------------------------------------

func EV_UnaryBool(op_name string, L *Node) *Node {
	switch op_name {
	case "not":
		V := LiteralIsTrue(L)
		return EV_MakeBool(! V, L.pos)
	}

	PostError("cannot use operator '%s' in a boolean const-expr", op_name)
	return P_FAIL
}

func EV_BinaryBool(op_name string, L, R *Node) *Node {
	LV := LiteralIsTrue(L)
	RV := LiteralIsTrue(R)

	var result bool

	switch op_name {
	case "and": result = LV && RV
	case "or":  result = LV || RV
	case "==":  result = LV == RV
	case "!=":  result = LV != RV
	default:
		PostError("cannot use operator '%s' in a boolean const-expr", op_name)
		return P_FAIL
	}

	return EV_MakeBool(result, L.pos)
}

func EV_UnaryFloat(op_name string, L *Node) *Node {
	V, V_err := strconv.ParseFloat(L.str, 64)
	if V_err != nil {
		PostError("bad float value in const expression: %s", L.str)
		return P_FAIL
	}

	switch op_name {
	case "-":
		return EV_MakeFloat(0.0 - V, L.pos)

	case "abs":
		return EV_MakeFloat(math.Abs(V), L.pos)

	case "sqrt":
		if V < 0 {
			PostError("square root of a negative value")
			return P_FAIL
		}
		return EV_MakeFloat(math.Sqrt(V), L.pos)

	case "round": V = math.Round(V)
	case "trunc": V = math.Trunc(V)
	case "floor": V = math.Floor(V)
	case "ceil":  V = math.Ceil (V)

	default:
		PostError("cannot use operator '%s' in a float const-expr", op_name)
		return P_FAIL
	}

	term := EV_MakeFloat(V, L.pos)

	if FloatToInteger(term) != OK {
		return P_FAIL
	}
	return term
}

func EV_BinaryFloat(op_name string, L, R *Node) *Node {
	// WISH: format result as hexadecimal if LHS is hex.

	LV, L_err := strconv.ParseFloat(L.str, 64)
	RV, R_err := strconv.ParseFloat(R.str, 64)

	if L_err != nil {
		PostError("bad float value in const expression: %s", L.str)
		return P_FAIL
	}
	if R_err != nil {
		PostError("bad float value in const expression: %s", R.str)
		return P_FAIL
	}

	// check for division by zero
	if op_name == "/" || op_name == "%" {
		if RV == 0.0 {
			PostError("division by zero in const expression")
			return P_FAIL
		}
	}

	var result float64

	switch op_name {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV - math.Trunc(LV / RV) * RV

	case "==": return EV_MakeBool(LV == RV, L.pos)
	case "!=": return EV_MakeBool(LV != RV, L.pos)
	case "<":  return EV_MakeBool(LV <  RV, L.pos)
	case "<=": return EV_MakeBool(LV <= RV, L.pos)
	case ">":  return EV_MakeBool(LV >  RV, L.pos)
	case ">=": return EV_MakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' in a float const-expr", op_name)
		return P_FAIL
	}

	if InfinityOrNan(result) {
		PostError("detected an infinity or NaN in const expression")
		return P_FAIL
	}

	return EV_MakeFloat(result, L.pos)
}

func EV_UnaryInteger(op_name string, L *Node) *Node {
	V, V_err := strconv.ParseInt(L.str, 0, 64)
	if V_err != nil {
		PostError("bad integer in const expression: %s", L.str)
		return P_FAIL
	}

	switch op_name {
	case "-", "~":
		if op_name == "-" {
			V = 0 - V
		} else {
			V = 0 - (V + 1)
		}
		int_str := strconv.FormatInt(V, 10)
		return NewNode(NL_Integer, int_str, L.pos)

	case "abs":
		if V < 0 {
			V = 0 - V
		}
		int_str := strconv.FormatInt(V, 10)
		return NewNode(NL_Integer, int_str, L.pos)
	}

	PostError("cannot use operator '%s' in an integer const-expr")
	return P_FAIL
}

func EV_BinarySigned(op_name string, L, R *Node) *Node {
	LV, L_err := strconv.ParseInt(L.str, 0, 64)
	RV, R_err := strconv.ParseInt(R.str, 0, 64)

	if L_err != nil {
		PostError("bad integer in const expression: %s", L.str)
		return P_FAIL
	}
	if R_err != nil {
		PostError("bad integer in const expression: %s", R.str)
		return P_FAIL
	}

	// check for bad shift count
	if op_name == "<<" || op_name == ">>" {
		if RV < 0 {
			PostError("bad shift count in const expression")
			return P_FAIL
		}
	}

	// check for division by zero
	if op_name == "/" || op_name == "%" {
		if RV == 0 {
			PostError("division by zero in const expression")
			return P_FAIL
		}
	}

	var result int64

	switch op_name {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV % RV

	case "&":  result = LV & RV
	case "|":  result = LV | RV
	case "~":  result = LV ^ RV
	case "<<": result = LV << uint64(RV)
	case ">>": result = LV >> uint64(RV)

	case "==": return EV_MakeBool(LV == RV, L.pos)
	case "!=": return EV_MakeBool(LV != RV, L.pos)
	case "<":  return EV_MakeBool(LV <  RV, L.pos)
	case "<=": return EV_MakeBool(LV <= RV, L.pos)
	case ">":  return EV_MakeBool(LV >  RV, L.pos)
	case ">=": return EV_MakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' in an integer const-expr", op_name)
		return P_FAIL
	}

	// WISH: format result as hexadecimal if LHS is hex.

	int_str := strconv.FormatInt(result, 10)
	return NewNode(NL_Integer, int_str, L.pos)
}

func EV_BinaryUnsigned(op_name string, L, R *Node) *Node {
	LV, L_err := strconv.ParseUint(L.str, 0, 64)
	RV, R_err := strconv.ParseUint(R.str, 0, 64)

	if L_err != nil {
		PostError("bad integer in const expression: %s", L.str)
		return P_FAIL
	}
	if R_err != nil {
		PostError("bad integer in const expression: %s", R.str)
		return P_FAIL
	}

	// check for subtraction producing a negative value
	if op_name == "-" && RV > LV {
		result  := int64(LV) - int64(RV)
		int_str := strconv.FormatInt(result, 10)
		return NewNode(NL_Integer, int_str, L.pos)
	}

	// check for division by zero
	if op_name == "/" || op_name == "%" {
		if RV == 0 {
			PostError("division by zero in const expression")
			return P_FAIL
		}
	}

	var result uint64

	switch op_name {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV % RV

	case "&":  result = LV & RV
	case "|":  result = LV | RV
	case "~":  result = LV ^ RV
	case "<<": result = LV << RV
	case ">>": result = LV >> RV

	case "==": return EV_MakeBool(LV == RV, L.pos)
	case "!=": return EV_MakeBool(LV != RV, L.pos)
	case "<":  return EV_MakeBool(LV <  RV, L.pos)
	case "<=": return EV_MakeBool(LV <= RV, L.pos)
	case ">":  return EV_MakeBool(LV >  RV, L.pos)
	case ">=": return EV_MakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' in an integer const-expr", op_name)
		return P_FAIL
	}

	// WISH: format result as hexadecimal if LHS is hex.

	int_str := strconv.FormatUint(result, 10)
	return NewNode(NL_Integer, int_str, L.pos)
}

func EV_BinaryString(op_name string, L, R *Node) *Node {
	if op_name != "+" {
		PostError("cannot use operator '%s' on strings", op_name)
		return P_FAIL
	}

	return NewNode(NL_String, L.str + R.str, L.pos)
}

func EV_IntegerMinMax(is_max bool, L, R *Node) *Node {
	// check sign to allow full range of 64-bit unsigned ints
	L_neg := (L.str[0] == '-')
	R_neg := (R.str[0] == '-')

	if (L_neg != R_neg) {
		if (L_neg == is_max) {
			return R;
		} else {
			return L;
		}
	}

	if (L_neg) {
		LV, L_err := strconv.ParseInt(L.str, 0, 64)
		RV, R_err := strconv.ParseInt(R.str, 0, 64)

		if L_err != nil || R_err != nil {
			PostError("bad integer in min/max expression")
			return P_FAIL
		}

		if ((LV < RV) == is_max) {
			return R;
		}
	} else {
		LV, L_err := strconv.ParseUint(L.str, 0, 64)
		RV, R_err := strconv.ParseUint(R.str, 0, 64)

		if L_err != nil || R_err != nil {
			PostError("bad integer in min/max expression")
			return P_FAIL
		}

		if ((LV < RV) == is_max) {
			return R;
		}
	}

	return L;
}

func EV_FloatMinMax(is_max bool, L, R *Node) *Node {
	LV, L_err := strconv.ParseFloat(L.str, 64)
	RV, R_err := strconv.ParseFloat(R.str, 64)

	if L_err != nil || R_err != nil {
		PostError("bad float value in min/max expression")
		return P_FAIL
	}

	// use MakeFloat to ensure a mixture of integer and floating
	// point will become a floating point value.
	if ((LV < RV) == is_max) {
		return EV_MakeFloat(RV, R.pos)
	} else {
		return EV_MakeFloat(LV, L.pos)
	}
}

func EV_MakeFloat(V float64, pos Position) *Node {
	term := NewNode(NL_Float, "", pos)
	term.str = strconv.FormatFloat(V, 'g', -1, 64)

	// ensure representation has a dot
	if strings.ContainsRune(term.str, '.') {
		// ok
	} else if strings.ContainsRune(term.str, 'e') {
		// ok
	} else {
		term.str = term.str + ".0"
	}
	return term
}

func EV_MakeBool(b bool, pos Position) *Node {
	term := NewNode(NL_Bool, "0", pos)
	term.ty = bool_type

	if b {
		term.str = "1"
	}
	return term
}
